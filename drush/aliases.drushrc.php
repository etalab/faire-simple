<?php

$aliases['dev'] = array(
    'root' => '/var/www/www.sgmap.dev.scoua.de/current',
    'uri' => 'http://www.sgmap.dev.scoua.de',
    'remote-host' => 'www.sgmap.dev.scoua.de',
    'remote-user' => 'capistrano',
    'path-aliases' => array(
        '%drush-script' => '/home/capistrano/.composer/vendor/bin/drush',
    ),
);

$aliases['preprod'] = array(
    'root' => '/var/exports/Datas/Pprod/var_www/gouv.preprod.scoua.de/faire-simple/current',
    'uri' => 'http://faire-simple.gouv.preprod.scoua.de',
    'remote-host' => 'faire-simple.gouv.preprod.scoua.de',
    'remote-user' => 'fsimple',
    'path-aliases' => array(
        '%drush-script' => '/var/exports/Datas/Pprod/var_www/gouv.preprod.scoua.de/faire-simple/.composer/vendor/bin/drush',
    ),
);

$aliases['prod'] = array(
    'root' => '/var/exports/Datas/Prod/var_www/gouv.fr/faire-simple/current',
    'uri' => 'http://faire-simple.gouv.fr',
    'remote-host' => '89.31.147.78',
    'remote-user' => 'fairesim',
    'path-aliases' => array(
        '%drush-script' => '/var/exports/Datas/Prod/var_www/gouv.fr/faire-simple/.composer/vendor/bin/drush',
    ),
);

