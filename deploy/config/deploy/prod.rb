### This file contains stage-specific settings ###
 
# Set the deployment directory on the target hosts.
set :deploy_to, "/var/exports/Datas/Prod/var_www/gouv.fr/faire-simple"
 
# The hostnames to deploy to.
set :domain, "goupil.nexen.net"
set :repository, "git@github.com:LaNetscouade/sgmap.git"

set :use_composer,          true
set :copy_vendors,          true

# The username on the target system, if different from your local username
ssh_options[:user] = 'fairesim'
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_rsa")]
ssh_options[:forward_agent] = true
