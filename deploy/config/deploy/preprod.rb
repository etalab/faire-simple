### This file contains stage-specific settings ###
 
# Set the deployment directory on the target hosts.
set :deploy_to, "/var/exports/Datas/Pprod/var_www/gouv.preprod.scoua.de/faire-simple"
 
# The hostnames to deploy to.
set :domain, "89.31.147.89"

set :use_composer,          true
set :copy_vendors,          true

# The username on the target system, if different from your local username
ssh_options[:user] = 'fsimple'
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_rsa")]
ssh_options[:forward_agent] = true 
