### This file contains stage-specific settings ###

# Set the deployment directory on the target hosts.
set :deploy_to, "/var/www/www.#{application}.dev.scoua.de"

# The hostnames to deploy to.
set :domain, "web.dev.scoua.de"

# The username on the target system, if different from your local username
ssh_options[:user] = 'capistrano'
