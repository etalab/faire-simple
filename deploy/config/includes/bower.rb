namespace :bower do
  desc "run bower install on localhost and upload files to server"
  task :install, :except => { :no_release => true } do
    run_locally "cd .. && bower install";
    run_locally "cd .. && tar -czf libraries.tar.gz libraries"
    top.upload "../libraries.tar.gz", release_path + '/', :via => :scp
    run "cd #{release_path} && tar -xzf libraries.tar.gz && rm -rf libraries.tar.gz"
    run_locally "cd .. && rm -rf libraries.tar.gz"
  end
end

after "deploy:update_code" do
  bower.install
end
