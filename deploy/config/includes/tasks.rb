# after trigger
after 'deploy', 'drupal:cleanup_files', 'deploy:cleanup', 'drush:cc' #, "drush:db"
after 'deploy:cold', 'drupal:cleanup_files', 'deploy:cleanup'
after 'deploy:setup', 'drupal:setup'

after 'deploy:create_symlink', 'drupal:symlink'

after 'deploy:build', 'deploy:setup', 'deploy:cold', 'drush:db:ul', 'drush:files:ul'

# composer
["composer:install", "composer:update"].each do |action|
  before action do
    if copy_vendors
      composer.composer.copy_vendors
    end
  end
end

after "deploy:finalize_update" do
  if use_composer
    if update_vendors
      composer.update
    else
      composer.install
    end
  end
end

namespace :deploy do

  desc "Create files dir for each domain"
  task :setup, :except => { :no_release => true } do
    dirs = [deploy_to, releases_path, shared_path]
    domains.each do |domain|
      dirs += [shared_path + "/#{domain}/files"]
    end
    dirs += %w(system).map { |d| File.join(shared_path, d) }
    run "umask 02 && mkdir -p #{dirs.join(' ')}" 
  end

  desc "Complete Site setup (setup + deploy:cold + files upload + database upload"
    task :build do
  end



  desc "Group writable permission"
  task :finalize_update, :except => { :no_release => true } do
    "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)
  end

  # Each of the following tasks are Rails specific. They're removed.
  task :migrate do
  end

  task :migrations do
  end

  task :start do
  end

  task :stop do
  end

  task :restart do
  end

end