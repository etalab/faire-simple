# v1.0.1
namespace :drush do
  task :default do
  end

  desc "Drush cache clear"
  task :cc, :except => { :no_release => true } do
    domains.each do |domain|
      run "cd #{current_path}; #{remote_drush} --uri=#{domain} cc all"
    end
  end

  namespace :web do
    desc "Set Drupal maintainance mode to online."
    task :enable, :except => { :no_release => true } do
      set(:domain) do
        Capistrano::CLI.ui.ask("Domain? (all or #{domains}) ") { |q| q.validate = /\A\w+\Z/ }
      end unless exists?(:q)

      php = 'variable_set("site_offline", FALSE)'

      if domain == 'all'
        domains.each do |domain|
          run "cd #{current_path}; #{remote_drush} --uri=#{domain} php-eval '#{php}'"
        end
      else
        run "cd #{current_path}; #{remote_drush} --uri=#{domain} php-eval '#{php}'"
      end
    end

    desc "Set Drupal maintainance mode to off-line."
    task :disable, :except => { :no_release => true } do
      set(:domain) do
        Capistrano::CLI.ui.ask("Domain? (all or #{domains}) ") { |q| q.validate = /\A\w+\Z/ }
      end unless exists?(:q)

      php = 'variable_set("site_offline", TRUE)'
      if domain == 'all'
        domains.each do |domain|
          run "cd #{current_path}; #{remote_drush} --uri=#{domain} php-eval '#{php}'"
        end
      else
        run "cd #{current_path}; #{remote_drush} --uri=#{domain} php-eval '#{php}'"
      end
    end

    desc "Drush custom command"
    task :custom, :except => { :no_release => true } do
      set(:command) do
        Capistrano::CLI.ui.ask("Command to execute:")
      end unless exists?(:command)
      set(:domain) do
        Capistrano::CLI.ui.ask("Domain? (all or #{domains}) ") { |q| q.validate = /\A\w+\Z/ }
      end unless exists?(:q)
      if domain == 'all'
        domains.each do |domain|
          run "cd #{current_path}; #{remote_drush} --uri=#{domain} #{command}"
        end
      else
        run "cd #{current_path}; #{remote_drush} --uri=#{domain} #{command}"
      end
    end
  end
  
  namespace :files do 
     desc "Create files backup"
     task :default do
       domains.each do |domain|
         dump_path = "#{shared_path}/backups/files"
         filename = "#{application}-#{stage}-#{domain}_files_#{Time.now.strftime("%Y%m%d%H%M%S")}.tar.gz"
         run "mkdir -p #{dump_path}"
         run "cd #{shared_path}/#{domain}/files; tar czf #{dump_path}/#{filename} *"
       end
     end

     desc "Backup and download files directories"
     task :dl, :except => { :no_release => true } do
       drush::files::default
       domains.each do |domain|
          dump_path = "#{shared_path}/backups/files"         
          dumps = capture("ls -xt #{dump_path}").split.reverse
          run_locally "mkdir -p backups/files"
          get("#{dump_path}/#{dumps.last}", "./backups/files/#{dumps.last}")
       end
     end

     desc "Upload and restore files directories"
     task :ul, :except => { :no_release => true } do
       domains.each do |domain|
          dumps = `ls -xt ./backups/files/`.split.reverse
          logger.info("Specify the files_backup file for '#{domain}' domain (must be in local ./backups/files/ folder)")
          prompt_with_default(:filename, dumps.last)
          run "mkdir -p #{shared_path}/backups/files"
          upload("./backups/files/#{filename}", "#{shared_path}/backups/files/#{filename}")
          run "cd #{shared_path}/#{domain}/files; tar xvf #{shared_path}/backups/files/#{filename}"
       end
     end

   end

   namespace :db do
     desc "Database backup"
     task :default, :except => { :no_release => true } do
       run "mkdir -p #{shared_path}/backups/database/"
       domains.each do |domain|
         filename = "#{application}-#{stage}-#{domain}_database_#{Time.now.strftime("%Y%m%d%H%M%S")}.sql"
         dump_path = "#{shared_path}/backups/database"
         run "cd #{current_path}; #{remote_drush} --uri=#{domain} sql-dump --structure-tables-key=common > #{dump_path}/#{filename}"
         run "cd #{dump_path}; gzip #{filename}"
       end
     end

   desc "Database backup && Download"
    task :dl, :except => { :no_release => true } do
      run "mkdir -p #{shared_path}/backups/database/"
      domains.each do |domain|
         filename = "#{application}-#{stage}-#{domain}_database_#{Time.now.strftime("%Y%m%d%H%M%S")}.sql"
         dump_path = "#{shared_path}/backups/database"
         run "cd #{current_path}; #{remote_drush} --uri=#{domain} sql-dump --structure-tables-key=common > #{dump_path}/#{filename}"         
         dumps = capture("ls -xt #{dump_path}").split.reverse
         run_locally "mkdir -p ./backups/database"
         get("#{dump_path}/#{dumps.last}", "./backups/database/#{dumps.last}")
      end
    end
  
    desc "Backup Upload and Restore database backup"
    task :ul, :except => { :no_release => true } do
      domains.each do |domain|
        dumps = `ls -xt ./backups/database/`.split.reverse
        logger.info("Specify the database backup (.sql) file for '#{domain}' domain (must be in local ./backups/database/ folder)")
        prompt_with_default(:filename, dumps.last)
        run "mkdir -p #{shared_path}/backups/database"
        upload("./backups/database/#{filename}", "#{shared_path}/backups/database/#{filename}")
        drush::db::default
        if Capistrano::CLI.ui.ask("Are you sure you want to restore #{filename} for #{application} - #{stage} - #{domain} ? (y/n): ") == 'y' 
          
          run "cd #{current_path}; `#{remote_drush} --uri=#{domain} sql-connect` < #{shared_path}/backups/database/#{filename} " do |channel, stream, data|
            raise Capistrano::Error, "unexpected output from mysql: " + data
          end
          logger.info "Restored successfully."
         else
          logger.info "Operation aborted."
        end
      end
    end
  
  end
end
