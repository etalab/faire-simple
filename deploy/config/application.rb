### This file contains project-specific settings ###
# The project name.
set :application, "sgmap"

# Multistage support - see config/deploy/[STAGE].rb for specific configs
set :default_stage, "dev"
set :stages, %w(dev preprod prod)

####################################################
# Set the repository type and location to deploy from.

# GITHUB
set :scm, :git
set :repository, "git@github-sgmap:LaNetscouade/sgmap.git"
set(:branch_prompt) { Capistrano::CLI.ui.ask("Branche ou tag à déployer [master]: ") }
set(:branch) { (branch_prompt == "") ? "master" : "#{branch_prompt}" }
set :repository_cache, "git_cache"
set :ssh_options, { :forward_agent => true }
set :deploy_via, :remote_cache
####################################################

# List the Drupal multi-site folders.  Use "default" if no multi-sites are installed.
set :domains, ["default"]

# Servers uris
set(:domain) { "#{domain}" }
role(:web) { domain }
role(:app) { domain }
role(:db, :primary => true) { domain }

# Generally don't need sudo for this deploy setup
set :use_sudo, false
# Keep the last 3 releases
set :keep_releases, 3

default_run_options[:pty] = true

# Drush configuration (could be overrided on stages.rb)
set :local_drush, "drush"

# Drush remote path
set :remote_drush, "/usr/bin/drush"
