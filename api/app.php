<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;

function boostrap_drupal() {
    $drupal_path = __DIR__ . '/../';
    define('DRUPAL_ROOT', $drupal_path);
    require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
}

boostrap_drupal();

function _get_users_account(){
    $query = db_select('sgmap_api_account', 'a');
    $query->fields('a', array('account','psswd'));
    $accounts = $query->execute()->fetchAll();
    $users = array();
    foreach ($accounts as $account) {
        $users[$account->account] = array('ROLE_ADMIN', $account->psswd);
    }
    return $users;
}

$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^/users',
            'http' => true,
            'users' => _get_users_account()
        ),
    )
));

$app->before(function (Request $request) {
    $parameter = $request->get('q');
    if(preg_match('/store\/annotations/', $parameter)){
        return true;
    }
    $key = $request->query->get('key');
    if(!isset($key) || empty($key)) {
        return new Response('Api key is required', 403);
    }
    $keyValid = db_query("SELECT count(cles) FROM {sgmap_api_API_key} WHERE cles=:key", array(':key' => $key))->fetchField();
    if(!$keyValid) {
        return new Response('Invalid api key', 403);
    }
    return true;
});

$app->post('/users/{uid}/statut', function ($uid, Request $request) use ($app) {
    $data = json_decode($request->getContent(), true);

    if(!isset($data['action']) || !in_array($data['action'], array(0,1))) {
        return new Response('Bad Request', 400);
    }

    $user = user_load($uid);
    if($user) {
        $fieldPartenaire = $user_fields->field_sgmap_est_partenaire;
        $fieldPartenaire['und'][0]['value'] = $data['action'];
        user_save($user, array('field_sgmap_est_partenaire' => $fieldPartenaire));
    }
    return true;
});

$app->post('/users/{uid}/authentication', function ($uid, Request $request) use ($app) {
    $data = json_decode($request->getContent(), true);
    if(!isset($data['token'])) {
        return new Response('Bad Request', 400);
    }
    $query = db_select('sgmap_panel_user_key', 'k')
        ->fields('k', array('user_key', 'updated'))
        ->condition('k.uid', $uid)
        ->condition('k.user_key',$data['token'])
        ;
    $result = $query->execute()->fetchField();
    if(!$result) {
        return new Response('Unauthorized', 401);
    } else {
        $num = db_delete('sgmap_panel_user_key')
            ->condition('uid', $uid)
            ->execute();
        return new Response('OK', 200);
    }
});

$app->get('/users/{uid}', function ($uid) use ($app) {
    $user = user_load($uid);
    unset($user->pass);
    return $app->json($user);
});

$app->get('/users', function (Request $request) use ($app) {
    $associe = $request->query->get('associe', 0);

    $usersQuery = new EntityFieldQuery();
    $usersQuery
       ->entityCondition('entity_type', 'user')
        ->range(0, 100)
        ;
    if($associe) {
        $usersQuery->fieldCondition('field_sgmap_est_partenaire', 'value', 1);
    }
    $result = $usersQuery->execute();
    $users = user_load_multiple(array_keys($result['user']));
    foreach ($users as $user) {
        unset($user->pass);
    }

    return $app->json($users);
});

$app->get('/terms', function () use ($app) {
    $usersQuery = new EntityFieldQuery();
    $result = $usersQuery
            ->entityCondition('entity_type', 'taxonomy_term')
            ->execute()
            ;

    $terms = taxonomy_term_load_multiple(array_keys($result['taxonomy_term']));

    return $app->json($terms);
});

$app->get('/campagnes', function (Request $request) use ($app) {
    $p = $request->query->get('p');

    $usersQuery = new EntityFieldQuery();
    $usersQuery
            ->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'campagne');

    if(!empty($p)){
       if(!in_array($p, array('p', 'e', 'a'))){
            $p = 'e';
        }
        $usersQuery
                ->fieldCondition('field_sgmap_public', 'value', array($p));
    }
    $result = $usersQuery->execute();
    $campagnes = node_load_multiple(array_keys($result['node']));

    return $app->json($campagnes);
});

$app->get('/propositions', function (Request $request) use ($app) {
    $min_nid = $request->query->get('min_id', 0);
    $p = $request->query->get('p');

    $propositionsQuery = new EntityFieldQuery();
    $propositionsQuery
            ->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'proposition')
            ->propertyCondition('nid', $min_nid, '>=')
            ->range(0, 10);

    if(!empty($p)){
        if(!in_array($p, array('p', 'e', 'a'))){
            $p = 'p';
        }
        $propositionsQuery
                ->fieldCondition('field_sgmap_public', 'value', array($p));
    }
    $result = $propositionsQuery->execute();
    $propositions = node_load_multiple(array_keys($result['node']));

    return $app->json($propositions);
});

$app->get('/users/stats/total', function () use ($app) {
    $data = file_get_contents('../sites/default/files/exports/users_stats_total.json', true);
    return $data;
});

$app->get('/users/stats/semestre', function () use ($app) {
    $data = file_get_contents('../sites/default/files/exports/users_stats_semestre.json', true);
    return $data;
});

$app->get('/store/annotations', function (Request $request) use ($app) {
    $atelier = $request->get('atelier');

    $text_nid = 0;
    if ($request->get('nid')) {
        $text_nid = $request->get('nid');
    }

    $query = db_select('sgmap_annotator_annotations', 'anno');
    $query->fields('anno');
    $query->condition('anno.id_atelier', $atelier);
    // we test equal to 0 for old annotation (before changing to annotate many texts)
    $query->condition('anno.text_nid', array(0, $text_nid));
    $results = $query->execute()->fetchAll();
    $data = array();
    $i = 0;
    foreach ($results as $key => $value) {
        $data[$i] = json_decode($value->annotation);
        $data[$i]->id = "$value->id_annotation";
        $i++;
    }
    return $app->json($data);
});

$app->post('/store/annotations', function (Request $request) use ($app) {
    $atelier = $request->get('atelier');
    
    // text annotator
    $text_nid = 0;
    if ($request->get('nid')) {
        $text_nid = $request->get('nid');
    } 

    $data = json_decode($request->getContent(), true);
    
    //notification queue
    // img annotator
    if (isset($data['uid'])) {
        $creator_uid = $data['uid'];
        $url = $data['context'];
    }
    // text annotator
    else {
        $creator_uid = $data['user']['id'];
        $base_url = url('node/' . $atelier, array('absolute' => TRUE, 'query' => array('step' => 'work')));
        $url = str_replace('api/', '', $base_url).'#block-sgmap-group-annotator-sgmap-group-annotator-work';
    }
    db_insert('sgmap_group_notif_queue') 
        ->fields(array(
            'creator_uid' => $creator_uid,
            'nid'         => 0,
            'url'         => $url,
            'gid'         => $atelier,
            'type'        => 'nouveau_commentaire_sur_un_contenu_dans_mon_atelier',
            'created'     => REQUEST_TIME,
        ))
        ->execute();

    $id = db_insert('sgmap_annotator_annotations') // Table name no longer needs {}
            ->fields(array(
              'id_atelier' => $atelier,
              'annotation' => $request->getContent(),
              'text_nid'   => $text_nid
            ))
            ->execute();
    $data['id'] = $id;


    return $app->json($data);
});

$app->put('/store/annotations', function (Request $request) use ($app) {
    $id = $request->get('id');
    $data = json_decode($request->getContent());
    $num_delete = db_update('sgmap_annotator_annotations')
            ->fields(array(
                'annotation' => $request->getContent(),
              ))
            ->condition('id_annotation', $id)
            ->execute();
    return $app->json($data);
});

$app->delete('/store/annotations', function (Request $request) use ($app) {
    $id = $request->get('id');
    $num_delete = db_delete('sgmap_annotator_annotations')
            ->condition('id_annotation', $id)
            ->execute();
    $data = json_decode($request->getContent());
    return $app->json($data);
});

$app->post('/store/annotations/ajax', function (Request $request) use ($app){
    $body = $request->get('body');
    $uid = $request->get('uid');
    $titre = $request->get('titre');
    $targetID = $request->get('target');
    $atelier = node_load($targetID);
    if(!sgmap_group_firepad_get_recommandation($atelier)){
        $node = new stdClass(); // Création d’un nouvel objet
        $node->type = "recommandation"; // Spécification du type de contenu à créer
        $node->title = $titre;
        $node->language = LANGUAGE_NONE; // Définition de la langue du contenu.
        node_object_prepare($node); // Création des valeurs par défaut.

        $node->uid = $uid;
        $node->body[$node->language][0]['value'] = $body; // Votre texte
        $node->body[$node->language][0]['format'] = 'full_html'; // Format d’entrée du champ.
        $node->og_group_ref[$node->language][0]['target_id'] = $targetID;
         
        $node = node_submit($node); // Prépare le node avant l’enregistrement (date + auteur) si les champs sont vides.
        node_save($node); // Enregistrement du node.

        return $node->nid;
    } else {
        return new Response('Already Exist', 400);
    }
});

$app->post('/store/annotations/ajax/update', function (Request $request) use ($app){
    $body = $request->get('body');
    $atelier = $request->get('id');
    $uid = $request->get('uid');
    $user = user_load($uid);

    $node = node_load($atelier);
    $isEdit = $node->field_recommandation_enable['und'][0]['value'];
    if($isEdit) {
        $node->body[$node->language][0]['value'] = $body;
        $node->revision = 1;
        $node->revision_uid = $uid;
        $node->log = 'Version du ' . date('d-m-Y à H\hi') . ' par ' . $user->name . '.';
        node_save($node); // Enregistrement d'une nouvelle révision du node

        return $node->nid;
    } else {
        return new Response('Not Edit', 400);
    }
});

$app->run();
