<?php

use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Features context.
 */
class FeatureContext extends Phase2\Behat\DrupalExtension\Context\DrupalMinkContext
{
    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param   array   $parameters     context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        // Initialize your context here
    }

//
// Place your definition and hook methods here:
//
//    /**
//     * @Given /^I have done something with "([^"]*)"$/
//     */
//    public function iHaveDoneSomethingWith($argument)
//    {
//        doSomethingWith($argument);
//    }
//


    /**
     * @Given /^there is a proposition$/
     */
    public function thereIsAProposition(TableNode $table)
    {
        $data = array();

        foreach($table->getRows() as $row) {
            $datas[$row[0]] = $row[1];
        }

        if (!isset($datas['uid'])) {
            if ($this->loggedInUser) {
                $datas['uid'] = $this->loggedInUser->uid;
            }
            else {
                global $user;
                $datas['uid'] = $user->uid;
            }
        }

        $node = array(
            'title'                => $datas['nid'],
            'title'                => $datas['title'],
            'type'                 => 'proposition',
            'revisions'            => NULL,
            'uid'                  => $datas['uid'],
            'language'             => LANGUAGE_NONE,
            'field_sgmap_public'   => array(LANGUAGE_NONE => array(0 => array('value' => $datas['field_sgmap_public']))),
            'field_sgmap_p_statut' => array(LANGUAGE_NONE => array(0 => array('value' => $datas['field_sgmap_p_statut']))),
        );

        $node = (object) $node;

        node_save($node);

        return $node;
    }
}
