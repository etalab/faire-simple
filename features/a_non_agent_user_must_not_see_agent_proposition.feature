Feature: A non agent user must not see agent proposition
    In order to protect agent proposition
    As a non agent user
    I must not see agent proposition

    Scenario: Anonymous must not see agent proposition
        Given I am an anonymous user
        Given there is a proposition
            | nid                  | 10001                |
            | title                | node behat           |
            | field_sgmap_p_statut | 0                    |
            | field_sgmap_public   | a                    |
        When I go to "node/10001"
        Then I should see "Accès refusé"
