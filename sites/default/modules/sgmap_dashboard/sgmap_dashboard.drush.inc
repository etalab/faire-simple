<?php

function sgmap_dashboard_drush_command() {
  $items = array();
  $items['recalculate-sgmap-stats'] = array(
    'description' => 'Recalculate dashboard stats',
    'aliases' => array('rds'),
  );
  return $items;
}

function drush_sgmap_dashboard_recalculate_sgmap_stats() {
  sgmap_dashboard_recalculate_stats();
}
