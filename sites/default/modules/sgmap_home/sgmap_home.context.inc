<?php
/**
 * @file
 * sgmap_home.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_home_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_home-sgmap_home_intro' => array(
          'module' => 'sgmap_home',
          'delta' => 'sgmap_home_intro',
          'region' => 'header',
          'weight' => '-10',
        ),
        'sgmap_campagne-sgmap_campagne_home_list' => array(
          'module' => 'sgmap_campagne',
          'delta' => 'sgmap_campagne_home_list',
          'region' => 'content',
          'weight' => '-10',
        ),
        'sgmap_group-sgmap_group_home_list' => array(
          'module' => 'sgmap_group',
          'delta' => 'sgmap_group_home_list',
          'region' => 'content',
          'weight' => '-9',
        ),
        'sgmap_home-sgmap_home_chiffre' => array(
          'module' => 'sgmap_home',
          'delta' => 'sgmap_home_chiffre',
          'region' => 'content',
          'weight' => '-8',
        ),
        'sgmap_home-sgmap_base_inscription' => array(
          'module' => 'sgmap_home',
          'delta' => 'sgmap_base_inscription',
          'region' => 'footer_top',
          'weight' => '-10',
        ),
        'sgmap_home-sgmap_base_panel' => array(
          'module' => 'sgmap_home',
          'delta' => 'sgmap_base_panel',
          'region' => 'footer_top',
          'weight' => '-9',
        ),
        'sgmap_home-sgmap_base_social' => array(
          'module' => 'sgmap_home',
          'delta' => 'sgmap_base_social',
          'region' => 'footer_top',
          'weight' => '-8',
        ),
        'sgmap_home-sgmap_home_partenaire' => array(
          'module' => 'sgmap_home',
          'delta' => 'sgmap_home_partenaire',
          'region' => 'footer_aftertop',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['home'] = $context;

  return $export;
}
