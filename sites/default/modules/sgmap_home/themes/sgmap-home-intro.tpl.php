<div class="header-banner-container">
    <div class="container">
        <div role="banner" class="header-banner">
            <div class="bulle-icons">

                <i class="picto-1 icon-phone"></i>
                <i class="picto-2 icon-sgmap-agent"></i>
                <i class="picto-3 icon-file-text-o large"></i>
                <i class="picto-4 icon-sgmap-particulier"></i>
                <i class="picto-5 icon-sgmap-entreprise large"></i>
                <i class="picto-6 icon-file-text-o small"></i>
                <i class="picto-7 icon-coment"></i>
                <i class="picto-8 icon-file-text-o"></i>
                <i class="picto-9 icon-sgmap-faq"></i>
                <i class="picto-10 icon-envelope-o small"></i>
                <i class="picto-11 icon-envelope-o"></i>
                <i class="picto-12 icon-sgmap-comment"></i>

            </div>

            <div class="bulle-icons bulle-icons-2">

                <i class="picto-1 icon-sgmap-agent large"></i>
                <i class="picto-2 icon-sgmap-entreprise large"></i>
                <i class="picto-3 icon-sgmap-particulier large"></i>



                <i class="picto-4 icon-stars small"></i>
                <i class="picto-5 icon-sgmap-faq"></i>
                <i class="picto-6 icon-sgmap-faq large"></i>

                <i class="picto-7 icon-coment"></i>
                <i class="picto-8 icon-coment large"></i>

                <i class="picto-9 icon-stars small"></i>

            </div>

            <h2><?php print $title ?></h2>
            <div class="intro-body"><?php print $body ?></div>
            <?php print $video_url ?>
        </div> <!-- /header-banner -->
    </div>
</div>