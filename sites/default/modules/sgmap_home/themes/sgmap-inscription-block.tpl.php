<div class="inscription block-footer">
	<h3><?php echo $title ?></h3>
	<p><?php echo $body ?></p>
	<div class="sgmap-base-btn-box">
		<a href="<?php echo $url ?>" class="btn btn-large"><?php echo $url_title ?></a>
	</div>
</div>