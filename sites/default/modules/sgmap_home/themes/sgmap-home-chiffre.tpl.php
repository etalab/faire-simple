<div class="chiffre-list">
    <ul class="row">
        <li class="first span4">
            <a class="content-chiffre-home sujet" href="<?php print url('les-sujets-du-moment') ?>" >
                <i class="icon-coment"></i>
                <span><?php print $sujets_count ?></span> sujets
            </a>
        </li>
        <li class="span4">
            <a class="content-chiffre-home proposition" href="<?php print url('les-sujets-du-moment') ?>" >
                <i class="icon-sgmap-ampoule"></i>
                <span><?php print $propositions_count ?></span> propositions
            </a>
        </li>
        <li class="last span4">
            <a class="content-chiffre-home atelier" href="<?php print url('fabrique-de-solutions') ?>" >
                <i class="icon-sgmap-personnes"></i>
                <span><?php print $ateliers_count ?></span> ateliers
            </a>
        </li>
    </ul>
</div>