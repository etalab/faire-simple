<div class="panel block-footer">
	<h3><?php echo $title ?></h3>
	<div>
		<img src="<?php echo $img ?>" alt="<?php echo $title ?>"/>
		<div class="sgmap-base-btn-box">
			<a class="btn btn-large" href="<?php echo $url ?>"><?php echo $url_title ?></a>
		</div>
	</div>
</div>