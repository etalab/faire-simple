<div class="social block-footer">
	<h3>Retrouvez nous sur les <strong>réseaux sociaux</strong></h3>
	<ul>
		<li><a href="<?php echo $facebook_link?>" title="Voir notre page Facebook">facebook<i class="icon-facebook"></i></a></li>
		<li><a href="<?php echo $twitter_link?>" title="Voir notre compte Twitter">twitter<i class="icon-twitter"></i></a></li>
	</ul>
</div>