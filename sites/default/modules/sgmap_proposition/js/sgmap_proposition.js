(function ($) {

  $(document).ready(function(){
    $('#field-group').hide();
    $('#unlogged-user-info').hide();
    $('#sgmap-proposition-save-proposal-form div.required').hide();
    $("#edit-step1").bind("click",function(){
      $('#field-group').show();
      $('#sgmap-proposition-save-proposal-form div.required').show();
      $('#unlogged-user-info').show();
    });
  });

})(jQuery);