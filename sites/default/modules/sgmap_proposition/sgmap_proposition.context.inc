<?php
/**
 * @file
 * sgmap_proposition.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_proposition_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'proposition_show';
  $context->description = '';
  $context->tag = 'proposition';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'proposition' => 'proposition',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_proposition-create_proposal_mini' => array(
          'module' => 'sgmap_proposition',
          'delta' => 'create_proposal_mini',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'sgmap_proposition-proposals_related' => array(
          'module' => 'sgmap_proposition',
          'delta' => 'proposals_related',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('proposition');
  $export['proposition_show'] = $context;

  return $export;
}
