<?php

/**
 * Implements hook_votingapi_insert();
 */
function sgmap_proposition_votingapi_insert($vote) {

  $vote = (object) $vote[0];

  if($vote->entity_type != 'node') {
    return;
  }

  $node = node_load($vote->entity_id);
  $user = user_load($vote->uid);

  if($node->type == 'proposition') {

    $event = new SgmapNotificationEvent();
    $event
      ->setSubject($node)
      ->setArguments(array(
        'user' => $user
      ))
    ;

    sgmap_notification_event_dispatch('proposition.vote.create', $event);
  }

}

/**
 * Implements hook_node_update().
 */
function sgmap_proposition_node_update($node) {

  if($node->type == 'proposition') {
    $old_node = node_load($node->nid);

    // proposition publiée
    if(_sgmap_proposition_is_proposition_newly_published($node, $old_node)) {

      $event = new SgmapNotificationEvent();
      $event->setSubject($node);

      sgmap_notification_event_dispatch('proposition.newly_published', $event);
    }

    // mesure engagée
    if(_sgmap_proposition_is_proposition_newly_engagee($node, $old_node)) {

      $event = new SgmapNotificationEvent();
      $event->setSubject($node);

      sgmap_notification_event_dispatch('proposition.newly_engagee', $event);
    }
    //proposition refusée
    if ($node->field_proposition_refused[LANGUAGE_NONE][0]['value'] == 1) {
      $event = new SgmapNotificationEvent();
      $event->setSubject($node);

      sgmap_notification_event_dispatch('proposition.refused', $event);
    }
  }
}

function _sgmap_proposition_is_proposition_newly_engagee($node, $old_node) {
  return ($old_node->field_sgmap_p_statut[LANGUAGE_NONE][0]['value'] != $node->field_sgmap_p_statut[LANGUAGE_NONE][0]['value'] && $node->field_sgmap_p_statut[LANGUAGE_NONE][0]['value'] == 1);
}

function _sgmap_proposition_is_proposition_newly_published($node, $old_node) {
  return ($old_node->status != $node->status && $node->status == 1);
}

/**
 * Implements hook_comment_insert().
 */
function sgmap_proposition_comment_insert($comment) {

  $node = node_load($comment->nid);

  if($node->type == 'proposition') {

    $event = new SgmapNotificationEvent();
    $event->setSubject($node);
    $event->setArguments(array(
      'comment' => $comment
    ));

    sgmap_notification_event_dispatch('proposition.comment.create', $event);
  }
}

/**
 * Implements hook_node_insert().
 */
function sgmap_proposition_node_insert($node) {

  // send notification when new proposition is created
  if($node->type == 'proposition') {

    $event_arguments = array();

    $campagne = sgmap_proposition_find_proposition_campagne($node);

    if($campagne) {
      $event_arguments['campagne'] = $campagne;
    }

    $event = new SgmapNotificationEvent();
    $event->setSubject($node);
    $event->setArguments($event_arguments);

    sgmap_notification_event_dispatch('proposition.create', $event);
  }
}

/**
 * Implements hook_mail().
 */
function sgmap_proposition_mail($key, &$message, $params) {

  $supported_notifications = sgmap_proposition_sgmap_notification_mail_mail_keys();

  if(!in_array($key, array_keys($supported_notifications))) {
    // mail key not supported
    return;
  }

  $notification_types = array();

  sgmap_proposition_sgmap_notification_notification_types_alter($notification_types);

  if(in_array($key, array_keys($notification_types))) {
    $signature = 'unsubscribe';
  } else {
    $signature = 'no_reply';
  }

  $message['#user'] = $params['user'];

  $event = $params['event'];

  $template_file_messages_path = drupal_get_path('module', 'sgmap_proposition') . '/themes/mail';

  $proposition = $event->getSubject();
  $campagne    = $event->getArguments('campagne');
  $message_parameters = array(
    '@proposition.href'  =>  url('node/'. $proposition->nid, array('absolute' => TRUE)),
    '@proposition.title' => $proposition->title,
  );

  if(is_array($campagne)) {
    $campagne = current($campagne);
    $message_parameters['@campagne.href'] = url('node/'. $campagne->nid, array('absolute' => TRUE));
    $message_parameters['@campagne.title'] = $campagne->title;

    $enddate = date_create($campagne->field_campagne_enddate[LANGUAGE_NONE][0]['value']);
    $enddate_timestamp = date_format($enddate, 'U');
    $message_parameters['@campage.date_fin'] = format_date($enddate_timestamp, 'custom', 'd/m/Y');
  }

  // new proposition
  if(in_array($key, array(
    'nouvelle_proposition_dans_ma_campagne',
    'nouvelle_proposition_sur_le_site'
  ))) {
    $message_parameters['@proposition.href'] = $message_parameters['@proposition.href'] . '/edit';
  }

  // new vote
  if(in_array($key, array(
    'nouveau_soutien_sur_une_de_mes_propositions',
    'nouveau_soutien_sur_une_proposition_suivie'
  ))) {
    $user = $event->getArgument('user');
    $message_parameters['@user.name'] = $user->name;
  }

  sgmap_mail_notification_prepare_message($message, $template_file_messages_path, $key, $message_parameters, $signature);
}

/**
 * Implements hook_query_TAG_alter().
 * filter subscribtion query
 */
function sgmap_proposition_query_sgmap_subscription_filter_alter(QueryAlterableInterface $query) {

  // retreive notification object
  $event                = $query->getMetaData('sgmap_notification_event');
  $notification_type_id = $query->getMetaData('sgmap_notification_type_id');

  if(in_array($notification_type_id, array(
    'nouveau_commentaire_sur_une_de_mes_propositions',
    'nouveau_soutien_sur_une_de_mes_propositions',
  ))) {
    $proposition = $event->getSubject();
    $query->condition('s.uid', $proposition->uid);
  }

  // bookmarked propositions ?
  if(in_array($notification_type_id, array(
    'nouveau_commentaire_sur_une_proposition_suivie',
    'nouveau_soutien_sur_une_proposition_suivie',
    'une_proposition_suivie_a_ete_engagee',
  ))) {
    $proposition = $event->getSubject();
    $query->leftJoin('flagging', 'f', 'f.uid = s.uid');
    $query->condition('f.entity_id', $proposition->nid);
  }

}

/**
 * Implements hook_sgmap_notification_event_process.
 */
function sgmap_proposition_sgmap_notification_event_process($event_type, $event) {
  if($event_type == 'proposition.create') {

    $proposition = $event->getSubject();
    $campagne = $event->getArgument('campagne');

    if(!$campagne) {
      $webmasters = sgmap_user_get_webmasters();
      foreach($webmasters as $webmaster) {
        $notification = new SgmapNotification($webmaster, 'nouvelle_proposition_sur_le_site', $event);
        sgmap_notification_send($notification);
      }
    } else {
      // load campagne author
      $campagne_author = user_load($campagne->uid);
      $notification = new SgmapNotification($campagne_author, 'nouvelle_proposition_dans_ma_campagne', $event);
      sgmap_notification_send($notification);
    }

  }

  if (in_array($event_type, array('proposition.newly_published', 'proposition.newly_engagee', 'proposition.refused'))) {
    $proposition = $event->getSubject();
    $proposition_author = user_load($proposition->uid);

    $map = array(
      'proposition.newly_published' => 'une_de_mes_propositions_a_ete_publiee',
      'proposition.newly_engagee'   => 'une_de_mes_propositions_a_ete_engagee',
      'proposition.refused'         => 'une_de_mes_propositions_a_ete_refusee',
    );

    $notification_key = $map[$event_type];

    $campagne = sgmap_campagne_get_propositon_campagne($proposition);

    if($campagne) {
      $event->setArgument('campagne', $campagne);
      $notification_key .= "_dans_une_campagne";
    }

    $notification = new SgmapNotification($proposition_author, $notification_key, $event);
    sgmap_notification_send($notification);
  }

}

/**
 * Implements hook_sgmap_notification_mail_mail_keys.
 */
function sgmap_proposition_sgmap_notification_mail_mail_keys(){
  return array(
    'nouveau_commentaire_sur_une_de_mes_propositions',
    'nouveau_commentaire_sur_une_proposition_suivie',
    'nouveau_soutien_sur_une_de_mes_propositions',
    'nouveau_soutien_sur_une_proposition_suivie',
    'changement_de_status_d_une_de_mes_propositions',
    'nouvelle_proposition_dans_ma_campagne',
    'nouvelle_proposition_sur_le_site',
    'une_de_mes_propositions_a_ete_engagee',
    'une_proposition_suivie_a_ete_engagee',
    'une_de_mes_propositions_a_ete_engagee_dans_une_campagne',
    'une_de_mes_propositions_a_ete_publiee',
    'une_de_mes_propositions_a_ete_publiee_dans_une_campagne',
    'une_de_mes_propositions_a_ete_refusee',
    'une_de_mes_propositions_a_ete_refusee_dans_une_campagne',
  );
}

/**
 * Implements hook_sgmap_notification_notification_types_alter.
 */
function sgmap_proposition_sgmap_notification_notification_types_alter(&$notification_types) {

  $notification_types['nouveau_commentaire_sur_une_de_mes_propositions'] = array(
    'name'       => 'Nouveau commentaire sur une de mes propositions',
    'event_type' => 'proposition.comment.create',
  );

  $notification_types['nouveau_commentaire_sur_une_proposition_suivie'] = array(
    'name'       => 'Nouveau commentaire sur une proposition suivie',
    'event_type' => 'proposition.comment.create',
  );

  $notification_types['nouveau_soutien_sur_une_de_mes_propositions'] = array(
    'name'       => 'Nouveau vote sur une de mes propostions',
    'event_type' => 'proposition.vote.create',
  );

  $notification_types['nouveau_soutien_sur_une_proposition_suivie'] = array(
    'name'       => 'Nouveau vote sur une proposition suivie',
    'event_type' => 'proposition.vote.create',
  );

  $notification_types['une_proposition_suivie_a_ete_engagee'] = array(
    'name'       => 'Changement de statut sur une proposition suivie',
    'event_type' => 'proposition.newly_engagee',
  );

}
