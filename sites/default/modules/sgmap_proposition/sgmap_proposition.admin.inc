<?php

function sgmap_proposition_settings_form() {
  $form['sgmap_home_proposition_sqid'] = array(
     '#type'           => 'textfield',
     '#title'          => t("SQID des propositions de la home"),
     '#default_value'  => variable_get('sgmap_home_proposition_sqid', NULL),
  );

  return system_settings_form($form);
}

function _sgmap_proposition_get_admin_proposition_query($filter_values, $user) {
  $public_names = sgmap_base_get_public_choices();

  $query = db_select('node', 'n')
    ->addTag('node_access')
    ->addMetaData('account', $user)
    ->addMetaData('op', 'update')
    ->condition('n.type', 'proposition')
    ->fields ('n', array (
      'title',
      'nid',
      'type',
      'changed',
      'created',
      'status',
    ));

  // author mail
  $query->innerJoin('users', 'u', 'u.uid = n.uid');
  $query->addField('u', 'mail', 'author_mail');

  // refused
  $query->leftJoin('field_data_field_proposition_refused', 'pr', 'pr.entity_id = n.nid');
  $query->addField('pr', 'field_proposition_refused_value', 'is_refused');

  // public
  $query->leftJoin('field_data_field_sgmap_public', 'public', 'public.entity_id = n.nid');
  $query->addField('public', 'field_sgmap_public_value');
  $query->condition('public.bundle', 'proposition');

  // statut
  $query->leftJoin('field_data_field_sgmap_p_statut', 'statut', 'statut.entity_id = n.nid');
  $query->addField('statut', 'field_sgmap_p_statut_value');

  // comment_count
  $query->innerJoin('node_comment_statistics', 'ncs', 'ncs.nid = n.nid');
  $query->addField('ncs', 'comment_count');

  // voting count
  $query->leftJoin('votingapi_cache', 'vc', 'vc.entity_id = n.nid AND vc.function = :function AND vc.tag = :tag', array(':function' => 'count', ':tag' => 'plus1_node_vote'));
  $query->addField('vc', 'value', 'vote_count');

  // thematiques joins
  $query->leftJoin('field_data_field_thematique_tid', 'pt', 'pt.entity_id = n.nid');
  $query->leftJoin('taxonomy_term_data', 'ttd', 'pt.field_thematique_tid_value = ttd.tid');
  $query->addField('ttd', 'name', 'thematique');

  // campagne join
  $query->leftJoin('field_data_field_proposition_campagne', 'pc', 'pc.entity_id = n.nid');
  $query->leftJoin('node', 'campagne', 'campagne.nid = pc.field_proposition_campagne_target_id');
  $query->addField('campagne', 'title', 'campagne_title');

  // public filter
  if($filter_values['public']) {
    $query->condition('public.field_sgmap_public_value', $filter_values['public']);
  }


  if($filter_values['contexte'] && $filter_values['contexte'] == -1) { // forum permanent
    $condition = db_or()
      ->condition('pc.field_proposition_campagne_target_id', 0)
      ->isNull('pc.field_proposition_campagne_target_id');
    $query->condition($condition);
  }

  // contexte filter
  if($filter_values['contexte'] && $filter_values['contexte'] > 0) {

    $thematique_tid = $filter_values['thematique'] ? $filter_values['thematique'] : $filter_values['contexte'];

    $sous_thematiques = sgmap_base_get_vocabulary_terms_choices('sgmap_thematique_campagne', null, $thematique_tid, 2);

    $condition_tids = array_keys($sous_thematiques);
    $condition_tids[] = $thematique_tid;

    $query->condition('pt.field_thematique_tid_value', $condition_tids, 'IN');

  } elseif($filter_values['public'] && $filter_values['thematique']) { // thematique filter


    $public_name = $public_names[$filter_values['public']];

    $taxonomy_field_name = 'thematique_' . $public_name . '_tid';
    $sous_thematiques = sgmap_base_get_vocabulary_terms_choices('thematique_' . $public_name, null, $filter_values['thematique'], 2);

    $condition_tids = array_keys($sous_thematiques);
    $condition_tids[] = $filter_values['thematique'];

    $query->condition('pt.field_thematique_tid_value', $condition_tids, 'IN');
  }

  if($filter_values['date_debut']) {
    $query->condition('n.created', strtotime($filter_values['date_debut']), '>=');
  }

  if($filter_values['date_fin']) {
    $query->condition('n.created', strtotime($filter_values['date_fin']), '<=');
  }

  if($filter_values['statut']) {
    $query->condition('statut.field_sgmap_p_statut_value', $filter_values['statut'] - 1);
  }

  if($filter_values['etat_de_publication']) {
    switch($filter_values['etat_de_publication']) {
      case 1:
        $query->condition('n.status', 1);
        break;
      case 2:
        $query->condition('n.status', 0);
        break;
      case 3:
        $query->condition('pr.field_proposition_refused_value', 1);
        break;
    }
  }

  $query->groupBy('n.nid');

  return $query;
}

function sgmap_proposition_admin_header() {
  return array(
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('E-mail de l\'auteur'), 'field' => 'author_mail'),
    array('data' => t('Date de publication'), 'field' => 'created'),
    array('data' => t('Public'), 'field' => 'field_sgmap_public_value'),
    array('data' => t('Campagne'), 'field' => 'campagne_title'),
    array('data' => t('Thématique'), 'field' => 'thematique'),
    array('data' => t('Updated'), 'field' => 'changed', 'sort' => 'desc'),
    array('data' => t('État de publication'), 'field' => 'status'),
    array('data' => t('Refusé'), 'field' => 'field_proposition_refused_value'),
    array('data' => t('Statut'), 'field' => 'field_sgmap_p_statut_value'),
    array('data' => t('Nombre de commentaires'), 'field' => 'comment_count'),
    array('data' => t('Nombre de recommandations'), 'field' => 'vote_count'),
    array('data' => t('Actions')),
  );
}

function sgmap_proposition_admin_propositions_export() {
  $filter_values = sgmap_propostion_get_admin_filters_values();

  global $user;

  $cols = array(
    'titre',
    'URL',
    'description',
    'email de l\'auteur',
    'date de publication (validation du webmaster)',
    'contexte de publication (forum général ou campagne)',
    'public',
    'thématique',
    'statut',
    'état',
    'nombre de likes',
    'nombre de commentaires',
    'nombre de partages FB',
    'nombre de partages TW',
    'nombre de partages G+',
    'nombre de personnes abonnées',
  );

  array_walk($cols, 'sgmap_export_format_cel');

  $public_names   = sgmap_base_get_public_choices();
  $statut_choices = sgmap_proposition_get_statut_choices();

  $export_file = sgmap_export_get_file_object('propositions', $user, $cols);

  $batch = array(
    'operations' => array(
      array('sgmap_proposition_admin_propositions_export_process', array($filter_values, $user, $public_names, $statut_choices, $export_file)),
    ),
    'finished'         => 'sgmap_proposition_admin_propositions_export_finished',
    'title'            => t('Processing export batch'),
    'init_message'     => t('Export batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Export batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'sgmap_proposition') . '/sgmap_proposition.admin.inc',
  );

  batch_set($batch);
  batch_process('admin/sgmap_reports/propositions');
}

function sgmap_proposition_admin_propositions_prepare_row($record, $public_names, $statut_choices) {
  $is_refused = $record['is_refused'] ? 'refusée' : ($record['status'] ? t('published') : t('not published'));
  $row = array(
    $record['title'],
    url('node/' . $record['nid'], array('absolute' => TRUE)),
    strip_tags($record['body']),
    $record['author_mail'],
    format_date($record['created'], 'custom', 'd/m/Y'),
    empty($record['campagne_title']) ? 'forum permanent' :  $record['campagne_title'],
    $public_names[$record['field_sgmap_public_value']],
    $record['thematique'],
    $is_refused,
    $statut_choices[$record['field_sgmap_p_statut_value'] + 1],
    $record['vote_count'],
    $record['comment_count'],
    $record['facebook_shares'],
    $record['twitter_shares'],
    $record['googleplus_shares'],
    $record['nb_abonnes'],
  );

  array_walk($row, 'sgmap_export_format_cel');

  return $row;

}

function sgmap_proposition_admin_propositions_export_process($filter_values, $user, $public_names, $statut_choices, $export_file, &$context) {

  $query = _sgmap_proposition_get_admin_proposition_query($filter_values, $user);

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results'] = array('file' => $export_file);

  }

  // pagination
  $query->condition('n.nid', $context['sandbox']['current_node'], '>');
  $query->orderBy('n.nid');

  // body
  $query->leftJoin('field_data_body', 'body_data', 'body_data.entity_id = n.nid');
  $query->addField('body_data', 'body_value', 'body');

  // social shares
  $query->leftJoin('lns_social_stats', 'facebook_stats', 'facebook_stats.entity_id = n.nid AND facebook_stats.social_network = :social_network', array(':social_network' => 'facebook'));
  $query->leftJoin('lns_social_stats', 'twitter_stats', 'twitter_stats.entity_id = n.nid AND twitter_stats.social_network = :social_network', array(':social_network' => 'twitter'));
  $query->leftJoin('lns_social_stats', 'googleplus_stats', 'googleplus_stats.entity_id = n.nid AND googleplus_stats.social_network = :social_network', array(':social_network' => 'googleplus'));
  $query->addField('facebook_stats', 'share_count', 'facebook_shares');
  $query->addField('twitter_stats', 'share_count', 'twitter_shares');
  $query->addField('googleplus_stats', 'share_count', 'googleplus_shares');

  // abonnés
  $bookmark_flag = flag_get_flag('bookmark');
  $query->leftJoin('flag_counts', 'flag_counts', 'flag_counts.entity_id = n.nid AND flag_counts.entity_type = :entity_type AND flag_counts.fid = :fid', array(':entity_type' => 'node', ':fid' => $bookmark_flag->fid));
  $query->addField('flag_counts', 'count', 'nb_abonnes');

  $query->range(0, 100);

  $result = $query->execute();
  $fp = fopen(drupal_realpath($export_file->uri), 'a');


  while($record = $result->fetchAssoc()) {
    $row = sgmap_proposition_admin_propositions_prepare_row($record, $public_names, $statut_choices);
    fputcsv($fp, $row);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $record['nid'];

  }

  fclose($fp);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function sgmap_proposition_admin_propositions_export_finished($success, $results, $operations) {
  if ($success) {
    global $user;
    $file = $results['file'];
    $file = file_save($file);

    sgmap_export_send_export($file);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}

function sgmap_proposition_admin_propositions() {

  global $user;

  $filter_values = sgmap_propostion_get_admin_filters_values();
  $query         = _sgmap_proposition_get_admin_proposition_query($filter_values, $user);

  $count_result = $query->countQuery()->execute()->fetchField();

  $header = sgmap_proposition_admin_header();

  $results = $query
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(50)
    ->execute()
  ;

  $public_names   = sgmap_base_get_public_choices();
  $statut_choices = sgmap_proposition_get_statut_choices();
  $terms          = sgmap_base_get_vocabulary_terms_choices('sgmap_thematique_campagne', null, 0, 2);

  $rows = array();

  foreach ($results as $node) {

    $operations = array();
    if (node_access('update', $node)) {
      $operations['edit'] = array(
        'title' => t('edit'),
        'href'  => 'node/' . $node->nid . '/edit',
      );
    }
    if (node_access('delete', $node)) {
      $operations['delete'] = array(
        'title' => t('delete'),
        'href'  => 'node/' . $node->nid . '/delete',
      );
    }
    $options['operations'] = array(
      'data' => array(
        '#theme'      => 'links__node_operations',
        '#links'      => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );

    $rows[] = array(
      'data' => array(
        l($node->title, 'node/'. $node->nid .'/edit'),
        $node->author_mail,
        format_date($node->created),
        $public_names[$node->field_sgmap_public_value],
        $node->campagne_title,
        $node->thematique,
        format_date($node->changed),
        $node->status ? t('published') : t('not published'),
        $node->is_refused ? 'Oui' : 'Non',
        $statut_choices[$node->field_sgmap_p_statut_value + 1],
        $node->comment_count,
        $node->vote_count,
        $options['operations'],
      )
    );
  }

  return array(
    'filter_form' => drupal_get_form('sgmap_proposition_admin_filter_form'),
    'count' => array('#markup' => $count_result . ' résultat(s)'),
    'table'       => array(
      '#theme'   => 'table',
      '#header'  => $header,
      '#rows'    => $rows,
      '#sticky'  => TRUE,
      '#empty'   => 'Aucune proposition',
    ),
    'pager' => array(
      '#theme' => 'pager',
      '#tags' => array()
    ),
    'export' => sgmap_proposition_admin_get_export_btn()
  );

}

function sgmap_proposition_admin_get_export_btn() {
  $query_parameters = drupal_get_query_parameters();

  if(isset($query_parameters['page'])) {
    unset($query_parameters['page']);
  }

  return array(
    '#theme'   => 'link',
    '#text'    => 'Exporter au format csv',
    '#path'    => 'admin/sgmap_reports/propositions/export',
    '#options' => array(
      'query'      => $query_parameters,
      'attributes' => array()
    )
  );
}

function sgmap_proposition_admin_filter_form_submit($form, &$form_state) {

  $query = array('filter' => 1);

  $filters_mapping = sgmap_propostion_get_admin_filters_mapping();

  foreach($filters_mapping as $filter_key => $filter_name) {
    if(isset($form_state['values'][$filter_name]) && $form_state['values'][$filter_name]) {
      $query[$filter_key] = $form_state['values'][$filter_name];
    }
  }

  $form_state['redirect'] = array(
    'admin/sgmap_reports/propositions',
    array('query' => $query)
  );

}

function sgmap_proposition_get_thematique_choices($public = null, $campagne_tid = null) {
  if($campagne_tid && $campagne_tid > 0) {
    return sgmap_base_get_vocabulary_terms_choices('sgmap_thematique_campagne', null, $campagne_tid, 2);
  }

  if($public) {
    $public_choices = sgmap_base_get_public_choices();
    return sgmap_base_get_vocabulary_terms_choices('thematique_' . $public_choices[$public], null, 0, 2);
  }

  return array();
}

function sgmap_proposition_get_etat_de_publication_choices() {
  return array(
    1 => 'Publié',
    2 => 'Non publié',
    3 => 'Refusé'
  );
}


function sgmap_proposition_get_statut_choices() {
  return array(
    1 => 'En discussion',
    2 => 'Fait'
  );
}

function sgmap_propostion_get_admin_filters_mapping() {
  return array(
    'c'  => 'contexte',
    'p'  => 'public',
    't'  => 'thematique',
    'dd' => 'date_debut',
    'df' => 'date_fin',
    's'  => 'statut',
    'ep' => 'etat_de_publication',
    'filter' => 'filter',
  );
}

function sgmap_propostion_get_admin_filters_values() {
  $query_parameters = drupal_get_query_parameters();
  $filters_mapping = sgmap_propostion_get_admin_filters_mapping();

  $filters_values = array();

  foreach($filters_mapping as $filter_key => $filter_name) {
    $filter_value = $query_parameters[$filter_key];
    $filters_values[$filter_name] = $filter_value;
  }

  return $filters_values;
}

function sgmap_proposition_update_thematique_filter_callback($form, $form_state) {
  return $form['filters']['thematique'];
}

function sgmap_proposition_admin_filter_form($form, $form_state) {
  if(!isset($form_state['values'])) {
    $filters_values = sgmap_propostion_get_admin_filters_values();
    $form_state['values'] = $filters_values;
  }
  $campagnes_choices = sgmap_base_get_vocabulary_terms_choices('sgmap_thematique_campagne', null, 0, 1);
  $thematique_choices = sgmap_proposition_get_thematique_choices($form_state['values']['public'], $form_state['values']['contexte']);

  $form['filters'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Filtres',
    '#collapsible' => TRUE,
    '#collapsed' => $filters_values['filter'] ? FALSE : TRUE,
  );

  $contexte_choices = $campagnes_choices;
  $contexte_choices = array_reverse($contexte_choices, true);
  $contexte_choices[-1] = 'Forum permanent';
  $contexte_choices[0] = 'Tous';
  $contexte_choices = array_reverse($contexte_choices, true);

  $form['filters']['contexte'] = array(
    '#title'         => 'Contexte de publication',
    '#type'          => 'select',
    '#options'       => $contexte_choices,
    '#ajax' => array(
      'callback' => 'sgmap_proposition_update_thematique_filter_callback',
      'wrapper'  => 'thematiques-div',
      'method'   => 'replace',
    )
  );

  $form['filters']['etat_de_publication'] = array(
    '#title'         => 'État de publication',
    '#type'          => 'select',
    '#options'       => sgmap_proposition_get_etat_de_publication_choices(),
    '#empty_option' => 'Tous',
    '#empty_value'   => 0,
  );

  $public_choices = sgmap_base_get_public_choices();

  $form['filters']['public'] = array(
    '#title'         => 'Public',
    '#type'          => 'select',
    '#options'       => $public_choices,
    '#empty_option' => 'Tous',
    '#empty_value'   => 0,
    '#ajax' => array(
      'callback' => 'sgmap_proposition_update_thematique_filter_callback',
      'wrapper'  => 'thematiques-div',
      'method'   => 'replace',
    )
  );



  $form['filters']['thematique'] = array(
    '#title'       => 'Thématique',
    '#prefix'      => '<div id="thematiques-div">',
    '#suffix'      => '</div>',
    '#type'        => 'select',
    '#empty_option' => 'Toute',
    '#empty_value' => 0,
    '#options'     => $thematique_choices,
  );

  // dates
  $form['filters']['dates'] = array(
    '#type' => 'fieldset',
    '#title' => 'Dates',
  );

  $form['filters']['dates']['date_debut'] = array(
    '#title'       => 'Début',
    '#type'        => 'date_popup',
    '#date_format' => 'd/m/Y',
  );

  $form['filters']['dates']['date_fin'] = array(
    '#title'       => 'Fin',
    '#type'        => 'date_popup',
    '#date_format' => 'd/m/Y',
  );

  $form['filters']['statut'] = array(
    '#title'         => 'Statut',
    '#type'          => 'select',
    '#options'       => sgmap_proposition_get_statut_choices(),
    '#empty_value'   => 0,
  );

  $form['filters']['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Filtrer'
  );

  // set default values
  foreach($form_state['values'] as $key => $value) {
    if($key == 'date_fin' || $key == 'date_debut') {
      if($value) {
        $form['filters']['dates'][$key]['#default_value'] = date('Y-m-d H:i:s', strtotime($value));
      }
    } else {
      $form['filters'][$key]['#default_value'] = $value;
    }
  }

  return $form;
}

