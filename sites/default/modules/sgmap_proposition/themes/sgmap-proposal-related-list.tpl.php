<?php if($proposition_list) : ?>
<div class="propositions-related">
	<p><?php print t('Dans la même thématique :'); ?></p>
	<div class="proposition-list">
		<?php print $proposition_list ?>
	</div>
</div>
<?php endif; ?>