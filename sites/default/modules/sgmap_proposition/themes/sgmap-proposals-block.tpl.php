    <div class="propositions-block">
        <h2>Les propositions</h2>
        <div class="propositions_menu"><?php print $form ?></div>
        <div class="propositions_menu_links">
            <ul class="nav nav-pills">
                <li><a href="">La sélection de la semaine</a></li>
                <li><a href="?f=les_plus_recentes">Les plus récentes</a></li>
                <li><a href="?f=les_plus_soutenues">Les plus soutenues</a></li>
            </ul>
        </div>
        <div class="proposition-list row-fluid" id="ajax-propositions-list">
            <?php print $content ?>
        </div>
    </div>
