<div class="pull-right not-logged-like">
  <p class="red"><i class="icon-heart-empty"></i> Soutenir cette idée</p>
  <div class="btn-box">
    <?php if ($login_url): ?>
      <a href="<?php print $login_url ?>" class="btn btn-red">Je me connecte</a>
      <a href="<?php print $register_url ?>" class="btn btn-red">Je crée mon compte</a>
    <?php else : ?>
      <p>Je dois finaliser mon inscription pour pouvoir soutenir cette idée</p>
      <a href="<?php print $edit_profile_url ?>" class="btn btn-red">Je finalise mon inscription</a>
    <?php endif ?>
  </div>
</div>