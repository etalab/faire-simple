<?php if ($content): ?>
<div class="propositions-related">
	<p><?php print t('Dans la même thématique :'); ?></p>
	<div class="proposition-list">
		<?php print $content ?>
	</div>
</div>
<?php endif; ?>