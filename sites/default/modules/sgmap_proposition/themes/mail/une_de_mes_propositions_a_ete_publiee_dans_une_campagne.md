[Faire Simple] Votre proposition vient d'être publiée
Bonjour @user.firstname @user.lastname,

Merci de votre contribution et de votre intérêt pour la consultation en cours "[@campagne.title](@campagne.href)".
Votre proposition est maintenant en ligne à l'adresse suivante : [@proposition.href](@proposition.href)

Vous pouvez partager votre proposition via messagerie électronique, Facebook, Google+, Twitter.

Vous pouvez aussi soumettre d'autres idées de simplification sur d'autres sujets qui vous intéressent.
