[Faire Simple] Nouvelle proposition dans la consultation @campagne.title
Bonjour,

Une nouvelle proposition vient d'être déposée dans la consultation [@campagne.title](@campagne.href) dont vous êtes l'administrateur.

Cliquez sur le lien ci-après pour accepter ou refuser cette proposition : [@proposition.title](@proposition.href).

Pour rappel, les propositions ne sont pas publiées par défaut sur la page de la consultation.
