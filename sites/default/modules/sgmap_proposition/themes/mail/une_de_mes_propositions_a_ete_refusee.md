[Faire Simple] Votre proposition a été refusée
Bonjour @user.firstname @user.lastname,

Vous avez soumis une idée sur le site faire-simple.gouv.fr et nous vous en remercions.

Néanmoins, nous ne pouvons publier votre proposition "@proposition.title" car celle‐ci ne respecte pas la charte d'utilisation du site.

Vous pouvez toutefois soumettre d'autres idées de simplification sur d'autres sujets qui vous intéressent.

