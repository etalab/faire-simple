[Faire Simple] Nouvelle proposition sur le site
Bonjour,

Une nouvelle proposition vient d'être déposée sur le site.

Cliquez sur le lien ci-après pour accepter ou refuser cette proposition : [@proposition.title](@proposition.href).

Pour rappel, les propositions ne sont pas publiées par défaut sur le site.
