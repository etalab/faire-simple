[Faire Simple] Nouveau soutien sur une proposition
Bonjour @user.firstname @user.lastname,

Une nouvelle recommandation vient d'être enregistrée sur la proposition [@proposition.title](@proposition.href) dont vous suivez l'activité.

