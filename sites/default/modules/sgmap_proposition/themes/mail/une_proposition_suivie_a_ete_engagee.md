[Faire Simple] Une proposition a été engagée.
Bonjour @user.firstname @user.lastname,

La proposition [@proposition.title](@proposition.href) dont vous suivez l'activité vient de passer en statut "Mesure engagée". Elle sera déployée prochainement.
