[Faire Simple] Nouveau commentaire sur une proposition
Bonjour @user.firstname @user.lastname,

Un nouveau commentaire vient d'être publié sur la proposition [@proposition.title](@proposition.href) dont vous suivez l'activité.

