[Faire Simple] Nouveau commentaire sur votre proposition
Bonjour @user.firstname @user.lastname,

Un nouveau commentaire vient d'être publié sur votre proposition [@proposition.title](@proposition.href).
