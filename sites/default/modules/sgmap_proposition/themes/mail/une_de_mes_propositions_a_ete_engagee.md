[Faire Simple] Votre proposition a été engagée.
Bonjour @user.firstname @user.lastname,

Votre proposition [@proposition.title](@proposition.href) publiée sur le site Faire Simple vient de passer en statut "Mesure engagée". Elle sera déployée prochainement.
