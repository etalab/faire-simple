[Faire Simple] Votre proposition a été engagée.
Bonjour @user.firstname @user.lastname,

Votre proposition [@proposition.title](@proposition.href) publiée dans le cadre de la consultation [@campagne.title](@campagne.href) vient de passer en statut "Mesure engagée". Elle sera déployée prochainement.
