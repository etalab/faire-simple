<?php $node = menu_get_object() ?>
<?php
    $destination = 'vos-idees';
    if($node) {
        $destination = 'node/'.$node->nid;
    }
 ?>
<div id="proposition-box">
    <div class="container">
      <?php print $form ?>
      <?php if(!$user_logged): ?>
        <div class="unlogged-user-info" id="unlogged-user-info">
          <p>Merci de vous connecter pour participer</p>
          <a class="btn btn-large btn-red" href="/user/register?destination=<?php echo $destination?>"><?php print t("Je m'inscris maintenant") ?></a>
          <a class="btn btn-large btn-red" href="/user?destination=<?php echo $destination?>"><?php print t("J'ai déjà un compte") ?></a>
        </div>
      <?php elseif($user_fc): ?>
        <div class="unlogged-user-info" id="unlogged-user-info">
          <p>Je finalise mon inscription pour pouvoir déposer une idée </p>
          <a class="btn btn-large btn-red" href="/user/<?php print $uid ?>/edit?destination=<?php echo $destination?>">Finaliser mon inscription</a>
        </div>
      <?php endif; ?>
  </div>
</div>