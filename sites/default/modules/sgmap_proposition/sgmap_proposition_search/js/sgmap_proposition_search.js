(function ($) {

  Drupal.behaviors.sgmapProposition = {
    attach: function (context, settings) {
      $('#agent-tooltip').tooltip();

      var setPublicValue = function (value) {
        $('#public-form-hidden').val(value);
      }


      $("#particulier").on( "click", function() {
          setPublicValue('p');
      });
      $("#entreprise").on( "click", function() {
          setPublicValue('e');
      });
      $("#agent").on( "click", function() {
          setPublicValue('a');
      });

      $(".selectall").on( "click", function() {
          var $this = $(this);
          if ($this.hasClass("active")) {
            $this
              .closest('.tab-pane')
              .find('input')
              .iCheck('uncheck');

            $this.removeClass("active").text('Tout sélectioner');

          } else {
            $this
              .closest('.tab-pane')
              .find('input')
              .iCheck('check');

            $this.addClass("active").text('Tout désélectioner');
          }
      });
    }
  };

  Drupal.ajax.prototype.commands.icheck_update_command = function(ajax, response, status) {
      initICheck('#' + response['wrapper-id']);
  };

})(jQuery);

