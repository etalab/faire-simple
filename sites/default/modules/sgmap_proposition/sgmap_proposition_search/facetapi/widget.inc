<?php

class SgmapPropositionSeachFacetapiWidgetSelect extends FacetapiWidget {
   /**
   * Renders the form.
   */
  public function execute() {
    $elements = &$this->build[$this->facet['field alias']];
    $elements = drupal_get_form('sgmap_proposition_search_facetapi_select', $elements);
  }
}