<div class="tag-line clearfix">
	<span class="pull-left"><?php print t('Pour qui ?') ?></span>
	<ul>
	  <li><a href="<?php print $particulier_url ?>"><?php print t('Particuliers') ?></a></li>
	  <li><a href="<?php print $entreprise_url ?>"><?php print t('Entreprises') ?></a></li>
	  <?php if($agent_url): ?>
	    <li><a href="<?php print $agent_url ?>"><?php print t('Agents') ?></a></li>
	  <?php else: ?>
	    <li><a id="agent-tooltip" href="#" data-toggle="tooltip" data-original-title="Tooltip on top" title="Connectez-vous avec votre profil Agent pour accéder à ce contenu">Agents</a></li>
	  <?php endif; ?>
	</ul>
</div>
<div class="tag-line clearfix">
	<span class="pull-left"><?php print t('Les thématiques :') ?></span>
	<?php print $thematiques ?>
</div>