<!-- Debut code AdPerf v2.0 http://weborama.com -->
<script type="text/javascript" src="https://cstatic.weborama.fr/js/advertiser/wbo_performance.js"></script>
<script type="text/javascript">
WBO_AMOUNT="0.0"; /* <== set here the command amount */
WBO_CLIENT="";     /* <== set here your client id */
WBO_INVOICE="";     /* <== set here your bill id */
WBO_NUM_ITEMS=1; /* <== set here the number of item */
WBO_POST_VALIDATION=0; /* <== set to 1 if need backoffice validation */

if (webo_performance) {
transfo = new performanceTransfo(SITE=435246, WBO_ID_TRANSFO=25581);
transfo.setAmount(WBO_AMOUNT);
transfo.setClient(WBO_CLIENT);
transfo.setId(WBO_INVOICE);
transfo.setQuantity(WBO_NUM_ITEMS);
transfo.setPostValidation(WBO_POST_VALIDATION);
transfo.setHost("gae.solution.weborama.fr");

/* == Optional parameters == */
//~optional parameters~
/* == Type parameters == */
/* == Free parameters == */
transfo.execute();
}
</script>
<!-- Fin code AdPerf -->