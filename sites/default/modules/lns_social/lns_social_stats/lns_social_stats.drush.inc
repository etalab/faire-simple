<?php

function lns_social_stats_drush_command() {
  $items = array();
  $items['generate-social-stats'] = array(
    'description' => 'Generate social stats',
    'aliases' => array('gcs'),
  );
  return $items;
}

function drush_lns_social_stats_generate_social_stats() {
  lns_social_stats_fetch_nodes_share_counts();
}
