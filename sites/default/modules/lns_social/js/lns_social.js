(function($) {

/**
 * Initialise fb-twitter-g+ button for ajax loaded content
 */
Drupal.behaviors.lns_social = {
  attach: function(context, settings) {
    FB.XFBML.parse();
    gapi.plusone.go();
    $.ajax({
      url: 'http://platform.twitter.com/widgets.js',
      dataType: 'script',
      cache: true
    });
}

})(jQuery);
