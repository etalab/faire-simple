<?php

/**
 *  Implementation of hook_theme_preprocess()
 */
function lns_social_preprocess_lns_social_share_buttons(&$vars) {
  usort($vars['append'], 'drupal_sort_weight');
  usort($vars['prepend'], 'drupal_sort_weight');
  $vars['twitter_text'] = $vars['title'];
}
