<ul class="nav_share" role="social links">
  <?php foreach ($prepend as $item) : ?>
    <li id="<?php print $item['class']  ?>" class="<?php print $item['class']  ?>"><?php print $item['data'] ?></li>
  <?php endforeach;  ?>
  <li class="fb-recommend">
    <iframe src="//www.facebook.com/plugins/like.php?href=<?php print $url; ?>&amp;width=175&amp;height=21&amp;colorscheme=light&amp;layout=button_count&amp;action=recommend&amp;show_faces=false&amp;send=false&amp;appId=133725703484557" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:175px; height:21px;" allowTransparency="true"></iframe>
  </li>
  <li class="twitter"><a href="https://twitter.com/share" class="twitter-share-button" data-text="<?php print $twitter_text; ?>" data-url="<?php print $tiny_url ?>" data-count="none" data-lang="fr" rel="canonical"></a></li>
  <li class="google-plus"><g:plusone size="medium" href="<?php print $url ?>"></g:plusone></li>
</ul>
