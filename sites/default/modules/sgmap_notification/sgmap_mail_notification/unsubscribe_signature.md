Faire Simple. Innovons. Simplifions.

*Ce message vous a été envoyé automatiquement car vous êtes membre de la plateforme www.faire-simple.gouv.fr. Merci de ne pas y répondre. Si vous ne souhaitez plus recevoir les messages de ce type, rendez-vous dans [votre espace de gestion des notifications](@user.notifications.settings.url).*
