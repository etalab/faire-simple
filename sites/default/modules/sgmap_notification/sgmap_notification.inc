<?php

/**
 * Class SgmapNotification 
 */
class SgmapNotification
{
  /**
   * user
   *
   * @var mixed
   */
  private $user;

  /**
   * type
   *
   * @var string
   */
  private $type;

  /**
   * parameters
   *
   * @var array
   */
  private $event;

  /**
   * __construct
   *
   * @param mixed $user
   * @param mixed $type
   * @param SgmapNotificationEvent $event
   */
  public function __construct($user, $type, SgmapNotificationEvent $event)
  {
    $this->user = $user;
    $this->type = $type;
    $this->event = $event;
  }

  /**
   * Get type.
   *
   * @return string type.
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Get user.
   *
   * @return string user.
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Get parameters.
   *
   * @return array parameters.
   */
  public function getEvent()
  {
    return $this->event;
  }
}


/**
 * Class SgmapNotificationEvent
 */
class SgmapNotificationEvent
{
  /**
   * subject
   *
   * @var string
   */
  private $subject;

  /**
   * arguments
   *
   * @var array
   */
  private $arguments = array();

  /**
   * setSubject
   *
   * @param mixed $subject
   */
  public function setSubject($subject) {
    $this->subject = $subject;
    return $this;
  }

  /**
   * setArguments
   *
   * @param mixed $arguments
   */
  public function setArguments(array $arguments) {
    $this->arguments = $arguments;
    return $this;
  }

  /**
   * setArgument
   *
   * @param string $key
   * @param mixed $argument
   */
  public function setArgument($key, $argument) {
    $this->arguments[$key] = $argument;
    return $this;
  }

  /**
   * getArgument
   *
   * @param string $key
   * @param mixed $default
   */
  public function getArgument($key, $default = null) {
    return isset($this->arguments[$key]) ? $this->arguments[$key] : $default;
  }

  /**
   * Get subject.
   *
   * @return string subject.
   */
  public function getSubject()
  {
    return $this->subject;
  }

  /**
   * Get arguments.
   *
   * @return array arguments.
   */
  public function getArguments()
  {
    return $this->arguments;
  }
}

class SgmapNotificationSubscription {
  /**
   * type
   *
   * @var mixed
   */
  private $type;

  /**
   * event_type
   *
   * @var mixed
   */
  private $eventType;

  /**
   * filters
   *
   * @var mixed
   */
  private $filters;

  /**
   * user
   *
   * @var mixed
   */
  private $user;

  /**
   * Get type.
   *
   * @return type.
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Get eventType.
   *
   * @return eventType.
   */
  public function getEventType()
  {
    return $this->eventType;
  }

  /**
   * Get filters.
   *
   * @return filters.
   */
  public function getFilters()
  {
    return $this->filters;
  }

  /**
   * Get user.
   *
   * @return user.
   */
  public function getUser()
  {
    return $this->user;
  }
}


