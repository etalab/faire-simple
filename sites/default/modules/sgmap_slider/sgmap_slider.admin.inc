<?php

function sgmap_slider_settings_form() {
  $form['sgmap_home_slider_sqid'] = array(
     '#type'           => 'textfield',
     '#title'          => t("SQID du slider de la home"),
     '#default_value'  => variable_get('sgmap_home_slider_sqid', NULL),
  );
  $form['sgmap_home_text'] = array(
     '#type'           => 'textfield',
     '#title'          => t("Text de la home"),
     '#default_value'  => variable_get('sgmap_home_text', NULL),
  );

  return system_settings_form($form);
}