<?php
/**
 * @file
 * sgmap_slider.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_slider_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_slider_image_default_styles() {
  $styles = array();

  // Exported image style: sgmap_slider.
  $styles['sgmap_slider'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 940,
          'height' => 522,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'sgmap_slider',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sgmap_slider_node_info() {
  $items = array(
    'slider' => array(
      'name' => t('Slider'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
