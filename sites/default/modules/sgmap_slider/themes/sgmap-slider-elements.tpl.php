<div class="slider-image"><?php print l($img, $url, array('html' => true)) ?></div>
<div class="slider-content-left">
  <h2><?php print l($title, $url) ?></h2>
  <div class="slider-txt"><?php print $content ?></div>
  <a href="/les-sujets-du-moment" class="btn btn-red"><?php print t('voir tous les sujets du moment') ?></a>
</div>