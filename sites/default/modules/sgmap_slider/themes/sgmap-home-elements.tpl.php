<div class="span6">
  <div class="home-lnks-content">
      <div class="img-box">
        <?php print l($img, $url, array('html' => true)) ?>
      </div>
      <div class="txt-box">
        <a href="<?php print url($url) ?>">
          <h2><?php print $title ?></h2>
          <p><?php print $content ?></p>
        </a>
      </div>
  </div>
</div>