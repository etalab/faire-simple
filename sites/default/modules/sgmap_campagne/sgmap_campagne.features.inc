<?php
/**
 * @file
 * sgmap_campagne.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_campagne_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_campagne_image_default_styles() {
  $styles = array();

  // Exported image style: campagne_show.
  $styles['campagne_show'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 940,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'campagne_show',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sgmap_campagne_node_info() {
  $items = array(
    'campagne' => array(
      'name' => t('Campagne'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
