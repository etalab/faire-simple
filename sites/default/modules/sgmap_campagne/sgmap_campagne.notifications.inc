<?php

/**
 * Implements hook_node_insert().
 */
function sgmap_campagne_node_insert($node) {

  // send notification_event when new proposition is created
  if($node->type == 'campagne') {

    $event = new SgmapNotificationEvent();
    $event->setSubject($node);

    sgmap_notification_event_dispatch('campagne.create', $event);
  }
}

/*
 * Implements hook_sgmap_notification_event_process.
 */
function sgmap_campagne_sgmap_notification_event_process($event_type, $event) {
  if($event_type == 'campagne.create') {

    $webmasters = sgmap_user_get_webmasters();

    foreach($webmasters as $webmaster) {
      $notification = new SgmapNotification($webmaster, 'nouvelle_campagne', $event);
      sgmap_notification_send($notification);
    }
  }
}

/**
 * Implements hook_sgmap_notification_mail_mail_keys.
 */
function sgmap_campagne_sgmap_notification_mail_mail_keys(){
  return array(
    'nouvelle_campagne',
  );
}

/**
 * Implements hook_mail().
 */
function sgmap_campagne_mail($key, &$message, $params) {

  if($key == 'nouvelle_campagne') {
    $event = $params['event'];
    $message['#user'] = $params['user'];

    $campagne = $event->getSubject();
    $template_file_messages_path = drupal_get_path('module', 'sgmap_campagne') . '/themes/mail';

    sgmap_mail_notification_prepare_message($message, $template_file_messages_path, $key, array(
      '@campagne.href'  => url('node/' . $campagne->nid),
      '@campagne.title' => $campagne->title,
    ));
  }
}



