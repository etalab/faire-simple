<?php

/**
 * page callback: campgne list
 */
function sgmap_campagne_list_page() {
  $filter = drupal_get_query_parameters();
  $public = 0;
  if (isset($filter['public'])) {
    $public = $filter['public'];
  }
  $campagnes = sgmap_campagne_load(10, $public);
  $items = array();

  if($campagnes) {
    foreach ($campagnes as $nid => $campagne) {
      $items[] = array(
        'data' => render(node_view($campagne, 'teaser')),
        'class' => array('span6'),
      );
    }
   $content = array(
      '#theme'  => 'item_list',
      '#items'  => $items,
      '#attributes' => array('class' => array('row'))
    );
  }
  else {
    $content = array(
      '#markup' => '<div class="search-result-count"><p>Actuellement aucun sujet n\'est destiné à ce public.</p></div>'
    );
  }

  return array(
    'content' => array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(
        'class' => array('campagne-list campagne-list-all'),
      ),
      'filter' => array(
        '#markup' => render(drupal_get_form('sgmap_campagne_list_filter_form', $public)),
      ),
      'content' => $content,
      'pager' => array(
        '#theme' => 'pager'
      )
    )
  );
}

/**
 * campagne list filter form
 */
function sgmap_campagne_list_filter_form($form, $form_state, $public) {
  $form['public'] = array(
    '#type' => 'select',
    '#title' => 'Filtrer par public',
    '#options' => sgmap_base_get_public_choices(),
    '#empty_value' => 0,
    '#empty_option' => 'Tout',
    '#default_value' => $public
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'filtrer'
  ); 
  return $form;
}

/**
 * submit handler for sgmap_campagne_list_filter_form
 */
function sgmap_campagne_list_filter_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
    'les-sujets-du-moment',
    array(
      'query' => array(
        'public' => $form_state['values']['public'],
      )
    )
  );
} 
