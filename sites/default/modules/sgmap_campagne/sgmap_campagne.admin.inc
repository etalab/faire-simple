<?php

function _sgmap_campagne_get_admin_query($user, $query_parameters) {

  $query = db_select('node', 'n')
    ->condition('n.type', 'campagne')
    ->addTag('node_access')
    ->addMetaData('account', $user)
    ->addMetaData('op', 'update')
    ->fields ('n', array (
      'title',
      'nid',
      'type',
      'created',
      'status',
    ));
  $query->leftJoin('field_revision_field_sgmap_public', 'public', 'public.entity_id = n.nid');

  $query->innerJoin('field_data_field_campagne_cloture', 'cloture', 'cloture.entity_id = n.nid');
  $query->addField('cloture', 'field_campagne_cloture_value');

  $query->innerJoin('field_data_field_campagne_enddate', 'end_date', 'end_date.entity_id = n.nid');
  $query->addField('end_date', 'field_campagne_enddate_value');

  // status
  if (isset($query_parameters['s'])) {
    $query->condition('n.status', $query_parameters['s']);
  }

  // public
  if (isset($query_parameters['p'])) {
    $query->condition('public.field_sgmap_public_value', $query_parameters['p']);
  }

  // cloturé
  if (isset($query_parameters['c'])) {
    $query->condition('cloture.field_campagne_cloture_value', $query_parameters['c']);
  }

  // date de creation debut
  if (isset($query_parameters['dcd'])) {
    $query->condition('n.created', strtotime($query_parameters['dcd']), '>=');
  }

  // date de creation fin
  if (isset($query_parameters['dcf'])) {
    $query->condition('n.created', strtotime($query_parameters['dcf']), '<=');
  } 

  // date de cloture debut
  if (isset($query_parameters['dcld'])) {
    $query->condition('end_date.field_campagne_enddate_value', $query_parameters['dcld'], '>=');
  }

  // date de cloture fin
  if (isset($query_parameters['dclf'])) {
    $query->condition('end_date.field_campagne_enddate_value', $query_parameters['dclf'], '<=');
  }

  return $query; 
}
 
function sgmap_campagne_admin_mes_campagnes() {
  global $user;
  $query_parameters = drupal_get_query_parameters();

  $header = array(
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Date de publication'), 'field' => 'created'),
    array('data' => t('Date de fin'), 'field' => 'field_campagne_enddate_value'),
    array('data' => t('état'), 'field' => 'field_campagne_cloture_value'),
    array('data' => t('Actions')),
  );

  $query = _sgmap_campagne_get_admin_query($user, $query_parameters);

  $count_result = $query->countQuery()->execute()->fetchField();

  $results = $query
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(50)
    ->execute()
  ;

  $rows = array();

  foreach ($results as $node) {
    $operations = array();
    if (node_access('update', $node)) {
      $operations['edit'] = array(
        'title' => t('edit'),
        'href'  => 'node/' . $node->nid . '/edit',
      );
    }
    if (node_access('delete', $node)) {
      $operations['delete'] = array(
        'title' => t('delete'),
        'href'  => 'node/' . $node->nid . '/delete',
      );
    }
    $options['operations'] = array(
      'data' => array(
        '#theme'      => 'links__node_operations',
        '#links'      => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
    $enddate = date_create($node->field_campagne_enddate_value);
    $enddate_timestamp = date_format($enddate, 'U');
    $rows[] = array(
      'data' => array(
        l($node->title, 'node/'. $node->nid .'/edit'),
        format_date($node->created, 'custom', 'd/m/Y'),
        format_date($enddate_timestamp, 'custom', 'd/m/Y'),
        $node->field_campagne_cloture_value == 1 ? t('Cloturée') : t('ouverte'),
        $options['operations'],
      )
    );
  }

  return array(
    'filter_form' => drupal_get_form('sgmap_campagne_admin_filter_form'),
    'count' => array('#markup' => $count_result . ' résultat(s)'),
    'table'       => array(
      '#theme'   => 'table',
      '#header'  => $header,
      '#rows'    => $rows,
      '#sticky'  => TRUE,
      '#empty'   => 'Aucune campagne',
      '#attributes' => array('id' => 'results-table'),
    ),
    'pager' => array(
      '#theme' => 'pager',
      '#tags'  => array(),
    ),
    'export' => array(
      '#theme'   => 'link',
      '#text'    => 'Exporter au format csv',
      '#path'    => 'admin/sgmap_reports/campagnes/export',
      '#options' => array(
        'query'      => $query_parameters,
        'attributes' => array(),
      ),
    ),
    'update' => $user->uid == 1 ? drupal_get_form('sgmap_campagne_participation_count_form') : ''
  );
}

function sgmap_campagne_get_admin_filters_mapping() {
  return array(
    'p'    => 'public',
    'dcd'  => 'date_created_debut',
    'dcf'  => 'date_created_fin',
    'dcld' => 'date_cloture_debut',
    'dclf' => 'date_cloture_fin',
    's'    => 'status',
    'c'    => 'cloture',
  );
}

function sgmap_campagne_admin_export() {

  global $user;

  $cols = array(
    'titre',
    'URL',
    'description',
    'date de publication ',
    'date de fin',
    'public',
    'statut',
    'état',
    'nombre propositions publiées',
    'nombre de recommandations',
    'nombre de commentaires',
  );

  array_walk($cols, 'sgmap_export_format_cel');

  $export_file = sgmap_export_get_file_object('campagnes', $user, $cols);
  $query_parameters = drupal_get_query_parameters();
  
  $batch = array(
    'operations' => array(
      array('sgmap_campagne_admin_export_process', array($export_file, $user, $query_parameters)),
    ),
    'finished'         => 'sgmap_campagne_admin_export_finished',
    'title'            => t('Processing export batch'),
    'init_message'     => t('Export batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Export batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'sgmap_campagne') . '/sgmap_campagne.admin.inc',
  );

  batch_set($batch);
  batch_process('admin/sgmap_reports/campagnes');
}

function sgmap_campagne_admin_export_process($export_file, $user, $query_parameters, &$context) {
  $query = _sgmap_campagne_get_admin_query($user, $query_parameters);
  $public_names   = sgmap_base_get_public_choices();



  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results'] = array('file' => $export_file);
  }

  $query->condition('n.nid', $context['sandbox']['current_node'], '>');

  $query->innerJoin('field_data_body', 'b', 'b.entity_id = n.nid');
  $query->addField('b', 'body_value');

  $query->addField('public', 'field_sgmap_public_value');

  $query->innerJoin('sgmap_campagne_participation_data', 'data', 'data.nid = n.nid');
  $query->fields('data', array('propositions', 'propositions_comments', 'propositions_recommandations'));

  $query->range(0, 10);
  $results = $query->execute();
  
  $fp = fopen(drupal_realpath($export_file->uri), 'a');
  

  foreach ($results as $node) {
    $enddate = date_create($node->field_campagne_enddate_value);
    $enddate_timestamp = date_format($enddate, 'U');
    $row = array(
      $node->title,
      url('node/' . $node->nid, array('absolute' => TRUE)),
      strip_tags($node->body_value),
      format_date($node->created, 'custom', 'd/m/Y'),
      format_date($enddate_timestamp, 'custom', 'd/m/Y'),
      $public_names[$node->field_sgmap_public_value],
      $node->status ? t('published') : t('not published'),
      $node->field_campagne_cloture_value == 1 ? t('Cloturée') : t('ouverte'),
      $node->propositions,
      $node->propositions_recommandations,
      $node->propositions_comments,
    );
    
    array_walk($row, 'sgmap_export_format_cel');

    fputcsv($fp, $row);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
  }

  fclose($fp);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

function sgmap_campagne_admin_export_finished($success, $results, $operations) {
  if ($success) {
    global $user;
    $file = $results['file'];
    $file = file_save($file);

    sgmap_export_send_export($file);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}


function sgmap_campagne_admin_filter_form($form, $form_state) {

  $query_parameters = drupal_get_query_parameters();
  $filters_mapping = sgmap_campagne_get_admin_filters_mapping();

  $form = array();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => 'Filtres',
    '#collapsible' => TRUE,
    '#collapsed' => $query_parameters['filter'] ? FALSE : TRUE,
  );

  $form['filters']['public'] = array(
    '#title'         => 'public',
    '#type'          => 'select',
    '#options'       => sgmap_base_get_public_choices(),
    '#empty_value'   => 0,
  );


  $form['filters']['created'] = array(
    '#type'  => 'fieldset',
    '#title' => 'Date de creation'
  );
  $form['filters']['created']['date_created_debut'] = array(
    '#title' => 'du',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
  );
  $form['filters']['created']['date_created_fin'] = array(
    '#title' => 'au',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
  );

  $form['filters']['cloture_date'] = array(
    '#type'  => 'fieldset',
    '#title' => 'Date de cloture'
  );
  $form['filters']['cloture_date']['date_cloture_debut'] = array(
    '#title' => 'du',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
  );
  $form['filters']['cloture_date']['date_cloture_fin'] = array(
    '#title' => 'au',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
  );

  $form['filters']['status'] = array(
    '#title' => 'Etat de publication',
    '#type' => 'select',
    '#empty_value'   => '',
    '#options' => array(
      1 => 'publié',
      0 => 'non publié'
      )
  );

  $form['filters']['cloture'] = array(
    '#title' => 'Cloturée',
    '#type' => 'select',
    '#empty_value'   => '',
    '#options' => array(
      1 => 'oui',
      0 => 'non'
      )
  );
  
  // set default values
  foreach($filters_mapping as $key => $value) {
    if (isset($query_parameters[$key])) {
      if ($key == 'dcd' || $key == 'dcf') {
        $form['filters']['created'][$value]['#default_value'] = $query_parameters[$key];
      }
      else if ($key == 'dcld' || $key == 'dclf') {
        $form['filters']['cloture_date'][$value]['#default_value'] = $query_parameters[$key];
      }
      else {
      $form['filters'][$value]['#default_value'] = $query_parameters[$key];
      }
    }
  }

  $form['filters']['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Filtrer'
  );

  return $form;
}

function sgmap_campagne_admin_filter_form_submit($form, &$form_state) {

  $query = array('filter' => 1);

  $filters_mapping = sgmap_campagne_get_admin_filters_mapping();

  foreach($filters_mapping as $filter_key => $filter_name) {
    if(isset($form_state['values'][$filter_name]) && $form_state['values'][$filter_name] != '') {
      $query[$filter_key] = $form_state['values'][$filter_name];
    }
  }

  $form_state['redirect'] = array(
    'admin/sgmap_reports/campagnes',
    array(
      'query' => $query,
      'fragment' => 'results-table')
  );

}

/**
 * form to update user participation data
 */
function sgmap_campagne_participation_count_form() {
  $form['description'] = array(
    '#type'  => 'item',
    '#title' => t('Appuyez sur le boutton ci-dessous pour mettre à jour les données des campagnes'),
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Mettre à jour'),
  );
  $form['#redirect'] = FALSE;
  return $form;
}

/**
 * submit handler for sgmap_campagne_participation_count_form
 */
function sgmap_campagne_participation_count_form_submit($form, &$form_state) {
  $path = drupal_get_path('module', 'sgmap_campagne');
  $batch = array(
    'title' => t('Updating'),
    'operations' => array(
      array('sgmap_campagne_participation_count_update_batch', array()),
    ),
    'file' => $path . '/sgmap_campagne.admin.inc',
  );
  batch_set($batch);
}

/**
 * batch process update user data
 */
function sgmap_campagne_participation_count_update_batch(&$context) {
  sgmap_campagne_participation_count_update();
}