function sgmap_campagne_redirect(url) {
  var options = document.getElementById('sgmap-campagne-filter-select');
  var index = options.selectedIndex;
  var assocUrl = new Array();

  assocUrl['les_plus_recentes']  = '#search-result-count';
  assocUrl['les_plus_soutenues'] = '?filter=soutenues#search-result-count';
  assocUrl['les_plus_commentes'] = '?filter=commentes#search-result-count';

  window.location.replace(url + assocUrl[options[index].value]);
}