<?php

/**
 * Implements hook_menu().
 */
function sgmap_export_menu() {
  $items['exports/%file'] = array(
    'page arguments'   => array(1),
    'page callback'    => 'sgmap_export_download',
    'access arguments' => array(1),
    'access callback'  => 'sgmap_export_download_access',
    'type'             => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Access callback
 *
 * @param mixed $file
 */
function sgmap_export_download_access($file) {
  global $user;

  return dirname($file->uri) == sgmap_export_get_exports_directory() && $file->uid == $user->uid;
}

/**
 * sgmap_export_get_exports_directory
 *
 */
function sgmap_export_get_exports_directory() {
  return 'public://exports';
}

function sgmap_export_get_file_object($export_type, $user, $cols = null) {
  $exports_directory = sgmap_export_get_exports_directory();
  // ensure export directory exists
  if(!file_prepare_directory($exports_directory)) {
    // create and protect exports directory
    drupal_mkdir($exports_directory);
    file_create_htaccess($exports_directory);
  }

  $file = new stdClass();
  $file->uri = sprintf('%s/%s_%s_%s.csv', $exports_directory, $export_type, $user->uid, date('Y-m-d-H-i-s'));
  $file->filename = sprintf('export-%s-%s.csv', $export_type, date('Y-m-d-H-i-s'));
  $file->uid = $user->uid;

  if($cols) {
    $fp = fopen(drupal_realpath($file->uri), 'a');
    fputcsv($fp, $cols);
    fclose($fp);
  }

  return $file;
}

function sgmap_export_send_export($file) {
  $user = user_load($file->uid);

  $params = array(
    'file' => $file
  );

  drupal_set_message(t('Le lien de téléchargement de votre export vient de vous être envoyé sur votre adresse mail : !mail', array('!mail' => $user->mail)));
  drupal_mail('sgmap_export', 'nouvel_export_disponible', $user->mail, $user->language, $params);
}

/**
 * Implements hook_mail().
 */
function sgmap_export_mail($key, &$message, $params) {
  if($key != 'nouvel_export_disponible') {
    return;
  }

  $path = drupal_get_path('module', 'sgmap_export');

  $file = $params['file'];

  sgmap_mail_notification_prepare_message($message, $path, $key, array(
    '@export.title' => $file->filename,
    '@export.href'  => url('exports/' . $file->fid, array('absolute' => TRUE))
  ));
}

/**
 * sgmap_export_download
 *
 * @param mixed $file
 */
function sgmap_export_download($file) {
  file_transfer($file->uri, array(
    'Content-disposition' => 'attachment; filename=' . $file->filename,
    'Content-Type'        => 'text/csv'
  ));
}

/**
 * format cel in windows format for excel
 */
function sgmap_export_format_cel(&$value, $key) {
  $value = iconv("UTF-8", "Windows-1252//TRANSLIT", $value);
}
