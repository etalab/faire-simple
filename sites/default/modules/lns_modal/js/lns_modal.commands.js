(function ($) {

  Drupal.behaviors.lnsModal = {
    attach: function (context, settings) {

      Drupal.ajax.prototype.commands.lns_modal_open = function(ajax, response, status) {
          // open modal, on show load iframe, on hide unload iframe (useful for video players)
          $('#' + response.modalId)
              .modal({})
              .on('show', function () {
                $(this).find('iframe').each(function(){
                   var src = $(this).attr('src');
                   if(src == '') {
                       var src = $(this).data('src');
                       $(this).attr('src', src);
                   }
                });
                $('body, html').animate({
                    scrollTop: 0
                }, 0, "linear");
              })
              .on('hide', function () {
                   $(this).find('iframe').each(function(){
                       $(this)
                          .data('src', $(this).attr('src'))
                          .attr('src', '')
                       ;
                   });
              })
              .modal('show')
          ;
      }

      Drupal.ajax.prototype.commands.lns_modal_init = function(ajax, response, status) {

          // ensure that body contain a div with lns_modal_container id
          var LnsModalContainer = $('body').find('#lns_modal_container');

          if(!LnsModalContainer.length) {
              $('body').append('<div id="lns_modal_container"/>');
          } else {
              LnsModalContainer.find('.lns-modal').each(function() {
                  $(this).modal('hide');
              });
          }

      }
    }
  };
})(jQuery);

