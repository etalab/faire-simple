<?php

/**
 * Display the atelier list in teaser mode
 * @return [array]  array with the theme to display a list of atelier
 */
function sgmap_group_solution_page() {

  $solutions = sgmap_group_get_solutions();

  return array(
    'head_solutions' => array(
      '#theme' => 'sgmap_group_page_solution',
      '#codeSolutions' => variable_get('sgmap_group_solutions_value', ''),
      ),
    'list_solutions' => array(
      '#theme' => 'item_list',
      '#items' => $solutions,
      ),
    'themepager' => array(
      '#theme' => 'pager',
    ),
  );
}

/**
 * page callback
 * group members page
 * @param  [integer] $gid [group nid]
 */
function sgmap_group_list_member_page($gid) {
  $path = drupal_get_path('module', 'lns_modal');
  $group = node_load($gid);
  
  drupal_add_js($path . '/js/lns_modal.commands.js');
  $result = db_select('og_membership', 'm')
      ->fields('m', array('id','etid'))
      ->condition('entity_type', 'user', '=')
      ->condition('gid', $gid, '=')
      ->execute();

  while($record = $result->fetchAssoc()) {
    $users[] = $record['etid'];
    $user = user_load($record['etid']);
    $test = array(
      '#theme' => 'sgmap_group_user_teaser',
      '#user' => $user,
      '#group'  => $group
    );
    $items[] = drupal_render($test);
  }
  $users =  array(
    'list_users' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      ),
    );

  $commands = array();
  $commands[] = lns_modal_command_init();

  $member_count = sgmap_group_get_member_count($gid);

  $modal = array(
    '#theme'      => 'bootstrap_modal',
    '#body'       => render($users),
    '#heading'    => '<i class="icon-sgmap-particulier"></i>' . format_plural($member_count, $member_count.' membre', $member_count.' membres'),
    '#attributes' => array(
      'id'              => 'modal-membre',
      'class'           => array('hide', 'lns-modal'),
      'tabindex'        => '-1',
      'role'            => 'dialog',
      'aria-hidden'     => 'true'
    )
  );

  $commands[] = ajax_command_html('#lns_modal_container', render($modal));
  $commands[] = lns_modal_command_modal_open($modal['#attributes']['id']);

  ajax_deliver(array(
    '#type'     => 'ajax',
    '#commands' => $commands
  ));
}