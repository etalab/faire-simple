<?php
/**
 * @file
 * sgmap_group.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_group_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_group_image_default_styles() {
  $styles = array();

  // Exported image style: atelier_liste.
  $styles['atelier_liste'] = array(
    'label' => 'atelier_liste',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sgmap_group_node_info() {
  $items = array(
    'sgmap_group' => array(
      'name' => t('Atelier'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
