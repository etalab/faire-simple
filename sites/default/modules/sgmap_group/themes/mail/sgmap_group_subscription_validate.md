[Faire Simple] Votre demande de rejoindre l'atelier "@group.group_title" a été acceptée
Bonjour @group.user_name,

Votre demande de rejoindre l'atelier "@group.group_title" a été acceptée. Vous pouvez y accéder dès maintenant en suivant l'url : @group.group_url