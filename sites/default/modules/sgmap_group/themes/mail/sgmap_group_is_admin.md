[Faire Simple] Vous êtes animateur de l'atelier "@group.group_title"
Bonjour @group.user_name,

Le webmaster du site Faire Simple vient de vous attribuer le statut d'Animateur de l'atelier "@group.group_title".
Vous pouvez désormais : 
- Modifier les données de l'atelier 
- Inviter et gérer les utilisateurs dans l'atelier
- Modérer l'Atelier
 
Vous pouvez accéder dès maintenant à l'atelier en suivant l'url: @group.group_url