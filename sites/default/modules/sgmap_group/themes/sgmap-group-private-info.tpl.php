<span class="detailsbox hidden-phone">
  <a class="link-details btn" data-toggle="popover" data-placement="top" data-content='<button type="button" id="close" class="close">×</button>Cet atelier est privé. Seules les personnes ayant reçu une invitation peuvent participer.'>
    <i class="icon-lock"></i> Atelier privé
  </a>
</span>