<?php

/**
 *  Implementation of hook_theme_preprocess()
 */
function sgmap_group_preprocess_sgmap_group_user_teaser(&$vars) {
  $user = $vars['user'];
  $node = $vars['group'];

  if ($user->picture) {
    $vars['user_img'] = file_create_url($user->picture->uri);
  }
  else {
    $path = drupal_get_path('module', 'sgmap_user');
    $vars['user_img'] = '/' . $path . '/img/default_image_picture.jpg';
  }
  
  $vars['is_admin'] = sgmap_group_is_user_admin($user->uid, $node);
  $vars['user_url'] = '/user/' . $user->uid;
}

/**
 *  Implementation of hook_theme_preprocess()
 */
function sgmap_group_preprocess_sgmap_group_private_info(&$vars) {
  //drupal_add_js('libraries/sass-bootstrap/js/bootstrap-popover.js');
}