<div class="atelier-content">
  <h3><a href="<?php print $url ?>"><?php print $title ?></a></h3>
  <div><i class="icon-sgmap-particulier"></i> <?php print $members ?> membres</div>
  <?php if ($type == 1): ?>
    <div><span class="padlock-close"><i class="icon-lock"></i> Atelier privé</span></div>
  <?php else: ?>
    <div class="padlock-open"><i class="icon-unlock-alt"></i> Atelier ouvert</div>
  <?php endif; ?>
  <?php if ($type == 0 || $is_member): ?>
  	<a href="<?php print $url ?>" class="btn btn-orange">Je souhaite participer</a>
  <?php endif; ?>
</div>