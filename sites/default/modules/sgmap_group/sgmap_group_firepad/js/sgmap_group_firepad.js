(function ($) {
  $(document).ready(function(){
    var atelier      = Drupal.settings.atelier;
    var admin        = Drupal.settings.admin;
    var userid       = Drupal.settings.userID;
    var username     = Drupal.settings.userName;
    var baseurl      = Drupal.settings.base;
    var titreatelier = Drupal.settings.titre;
    var padSaved     = Drupal.settings.saved;
    var padState     = Drupal.settings.state;
    var token        = Drupal.settings.firebase_token;
    var firebase_url = Drupal.settings.firebase_url;

    /// Create instance of FirePad
    var firepadDiv = document.getElementById('firepad');
    var firepadRef = new Firebase(firebase_url + '/Firepad/atelier_' + atelier);

    firepadRef.auth(token, function(error) {
      if(error) {
        // login fail
      } else {
        // login success
      }
    });

    var firepadRefState = firepadRef.child('state');
    firepadRefState.once('value', function(snapshot){
      if(snapshot.val() === null){
        firepadRefState.set({state: padState});
      } else {
        if(snapshot.val().state != padState){
          firepadRefState.set({state : padState});
        }
      }

      firepadRefState.on('child_changed', function(snapshot){
        console.log(snapshot.val());
        window.location.reload();
        });

    });
    
    
    //// Create CodeMirror (with lineWrapping on).
    var codeMirror = CodeMirror(firepadDiv, { lineWrapping: true});

    // Create a random ID to use as our user ID (we must give this to firepad and FirepadUserList).
    var userId = Math.floor(Math.random() * 9999999999).toString();

    //// Create Firepad (with rich text features and our desired userId).
    var firepad = Firepad.fromCodeMirror(firepadRef, codeMirror,
        { richTextToolbar: true, richTextShortcuts: true, userId: userId});

    //// Create FirepadUserList (with our desired userId).
    var firepadUserList = FirepadUserList.fromDiv(firepadRef.child('users'),
        document.getElementById('userlist'), userId);

    firepad.on('ready', function() {
      if(admin) {
        if(padSaved) {

          document.getElementById('closePad').addEventListener('click', function(event) {
            firepadRefState.off('child_changed');
            firepadRefState.set({state: false});
          });

          var updateLink = document.getElementById('updateLink');
          updateLink.addEventListener('click', function(event) {
            event.returnValue = false;
            if (event.preventDefault) {
              event.preventDefault();
            }
            var bodyText = firepad.getHtml();
            $.ajax({
              url : baseurl + "/api/store/annotations/ajax/update",
              type: "POST",
              data : {body: bodyText, id: padSaved, uid: userid},
              success: function(data, textStatus, jqXHR)
              {
                var alertDiv = createAlert('La révision a bien été créée', true);
                document.getElementById('buttonAlert').appendChild(alertDiv);
              },
              error: function(data, textStatus, jqXHR){
                var alertDiv = createAlert('Erreur lors de la sauvegarde de la nouvelle révision. Merci de recharger la page si le problème persiste.', false);
                document.getElementById('buttonAlert').appendChild(alertDiv);
              }
            });
          }, false);
        } else {
          var addLink = document.getElementById('addLink');
          addLink.addEventListener('click', function(event) {
            event.preventDefault();
            var bodyText = firepad.getHtml();
            $.ajax({
              url : baseurl + "/api/store/annotations/ajax",
              type: "POST",
              data : {body: bodyText, uid: userid, titre: titreatelier, target: atelier},
              success: function(data, textStatus, jqXHR)
              {
                window.location.reload();
              },
              error: function(data, textStatus, jqXHR){
                // var alertDiv = createAlert('Erreur lors de la création du document.', false);
                // firepadDiv.appendChild(alertDiv);
                window.location.reload();
              }
            });
          }, false);
        }
      }
    });
  });

})(jQuery);

function createAlert(text, success) {
  var alertDiv = document.createElement('div');
  if(success){
    alertDiv.setAttribute('class', 'alert alert-success');
  } else {
    alertDiv.setAttribute('class', 'alert alert-error');
  }
  var alertButton = document.createElement('button');
  alertButton.setAttribute('type', 'button');
  alertButton.setAttribute('class', 'close');
  alertButton.setAttribute('data-dismiss', 'alert');
  alertButton.innerHTML = "&times;";
  var alertDivTxt = document.createTextNode(text);
  alertDiv.appendChild(alertDivTxt);
  alertDiv.appendChild(alertButton);
  return alertDiv;
}