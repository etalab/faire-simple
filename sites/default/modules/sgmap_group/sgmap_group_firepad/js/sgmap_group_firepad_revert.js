(function ($) {
  $(document).ready(function(){
    myInfo = createAlert('Synchronisation en cours...', false);
    document.getElementById('alertInfo').appendChild(myInfo);

    var atelier   = Drupal.settings.atelier;
    var codeBody  = Drupal.settings.codebody;
    var token     = Drupal.settings.firebase_token;
    var firebase_url = Drupal.settings.firebase_url;
    /// Create instance of FirePad
    var firepadDiv = document.getElementById('firepad');
    var firepadRef = new Firebase(firebase_url + '/Firepad/atelier_' + atelier);

    firepadRef.auth(token, function(error) {
      if(error) {
        // login fail
      } else {
        // login success
      }
    });
    
    //// Create CodeMirror (with lineWrapping on).
    var codeMirror = CodeMirror(firepadDiv, { lineWrapping: true});

    // Create a random ID to use as our user ID (we must give this to firepad and FirepadUserList).
    var userId = Math.floor(Math.random() * 9999999999).toString();

    //// Create Firepad (with rich text features and our desired userId).
    var firepad = Firepad.fromCodeMirror(firepadRef, codeMirror,
        { richTextToolbar: false, richTextShortcuts: false, userId: userId});

    var firepadUserList = FirepadUserList.fromDiv(firepadRef.child('users'),
        document.getElementById('userlist'), userId);

    firepad.on('ready', function() {
      firepad.setHtml(codeBody);
      myAlert = createAlert('La version a bien été restauré.', true);
      setTimeout(function(){
        document.getElementById('alertInfo').appendChild(myAlert);
        document.getElementById('buttonReturn').style.display = 'inline';
      },2000);
    });    
  });
})(jQuery);

function createAlert(text, success) {
  var alertDiv = document.createElement('div');
  if(success){
    alertDiv.setAttribute('class', 'alert alert-success');
  } else {
    alertDiv.setAttribute('class', 'alert alert-info');
  }
  var alertButton = document.createElement('button');
  alertButton.setAttribute('type', 'button');
  alertButton.setAttribute('class', 'close');
  alertButton.setAttribute('data-dismiss', 'alert');
  alertButton.innerHTML = "&times;";
  var alertDivTxt = document.createTextNode(text);
  alertDiv.appendChild(alertDivTxt);
  alertDiv.appendChild(alertButton);
  return alertDiv;
}
