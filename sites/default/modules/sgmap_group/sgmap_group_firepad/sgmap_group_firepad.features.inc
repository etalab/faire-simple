<?php
/**
 * @file
 * sgmap_group_firepad.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_group_firepad_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sgmap_group_firepad_node_info() {
  $items = array(
    'recommandation' => array(
      'name' => t('recommandation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'sgmap_group_wiki' => array(
      'name' => t('Texte à réviser'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
