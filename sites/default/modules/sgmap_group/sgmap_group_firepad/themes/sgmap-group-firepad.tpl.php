<div class="container">
  <div id="userlist" class="container"></div>
</div>
<div class="container">
  <div id="firepad"></div>
</div>
<?php if($admin): ?>
  <div id="buttonAlert" class="container">
    <?php if($padSaved): ?> 
      <a id="updateLink" href="node/add" title="Créer une révision" class="btn btn-orange btn-large">Créer une révision</a>
      <a id="closePad" href="/firepad/<?php print $padSaved ?>/update/0" title="" class="btn btn-orange btn-large">Fermer à l'édition</a>
      <a href="/firepad/<?php print $padSaved ?>" class="btn btn-orange btn-large">Voir les versions</a>
    <?php else: ?>
      <a id="addLink" href="node/add" title="Enregistrer le texte" class="btn btn-orange btn-large">Enregistrer le texte</a>
    <?php endif; ?>
  </div>
<?php endif; ?>