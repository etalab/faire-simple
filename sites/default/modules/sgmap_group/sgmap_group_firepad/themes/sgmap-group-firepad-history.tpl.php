<section id="firepad-history">
  <div>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td>Révision</td>
          <td colspan="3">Actions</td>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($versions as $version): ?>
        <tr>
          <td>
            <?php print date('d\/m\/Y - H\hi',$version->timestamp) ?><br/>
            <?php print $version->log ?>
          </td>
          <?php if($version->current_vid == $version->vid): ?>
            <td colspan="3">Version actuelle</td>
          <?php else: ?>
            <td><a class="btn" href="/firepad/<?php print $pad->nid ?>/revision/<?php print $version->vid ?>/view" >Voir</a></td>
            <td><a class="btn" href="/firepad/<?php print $pad->nid ?>/revision/<?php print $version->vid ?>/revert">Rétablir</a></td>
            <td><a class="close" href="/firepad/<?php print $pad->nid ?>/revision/<?php print $version->vid ?>/delete"><i class="icon-remove"></i></a></td>
          <?php endif; ?>
        </tr>
      <?php endforeach; ?>
    </tbody>
    </table>
  </div>
  <div class="version-box">
    <h3>Version actuelle : </h3>
    <?php print $pad->body[LANGUAGE_NONE][0]['value'] ?>
  </div>
  <a href="<?php print url('node/' . $atelier . '') . '?step=synthese' ?>" class="btn btn-orange btn-large">Retour à l'atelier</a>
</section>