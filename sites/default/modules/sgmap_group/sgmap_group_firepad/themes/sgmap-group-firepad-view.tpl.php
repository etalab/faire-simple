<div class="version-box">
  <?php print $body ?>
  <hr/>
  <em><?php print $pad->log ?></em>
</div>
<div class="container">
  <a href="/firepad/<?php print $pad->nid ?>" class="btn btn-orange btn-large">Retour</a>
  <a class="btn btn-orange btn-large" href="/firepad/<?php print $pad->nid ?>/revision/<?php print $revID ?>/revert">Rétablir cette vesrion</a>
  <a class="btn btn-orange btn-large" href="/firepad/<?php print $pad->nid ?>/revision/<?php print $revID ?>/delete">Supprimer cette version</a>
</div>