<div class="container wiki">
  <div id="firepad">
    <?php if (!$blocked) : ?>
      <a class="btn" href="<?php print $edit_url ?>">Modifier</a>
    <?php endif; ?>
    <?php if ($admin) : ?>
      <a class="btn" href="<?php print $revision_url ?>">Voir les révisions</a>
    <?php endif; ?>
    <?php print $body ?>
  </div>
</div>