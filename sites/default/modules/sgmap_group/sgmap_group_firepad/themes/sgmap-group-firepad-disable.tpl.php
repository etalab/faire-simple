<section class="block block-sgmap-group-annotator" id="block-sgmap-group-annotator-sgmap-group-annotator-work">
  <div class="container annotator-wrapper">
    <?php print $content ?>
  </div>
  <?php if($admin): ?>
    <div id="buttonAlert" class="container">
      <a href="/firepad/<?php print $padid ?>/update/1" title="" class="btn btn-orange btn-large">Ouvrir à l'édition</a>
      <a href="/firepad/<?php print $padid ?>" class="btn btn-orange btn-large">Voir les versions</a>
    </div>
  <?php endif; ?>
</section>