(function ($) {

  $(document).ready(function(){
    
    var userid = Drupal.settings.userID;
    var atelierid = Drupal.settings.atelier;
    var isAdmin = Drupal.settings.sgmap_group_img_annotator.is_admin;
    var userName = Drupal.settings.userName;
    var blocked = Drupal.settings.sgmap_group_img_annotator.blocked;
    var elm = $('#annotatable')[0];
    anno.makeAnnotatable(elm);
    var img_url = $('#annotatable').attr('src');

    // gets and print annotators
    if (Drupal.settings.sgmap_group_img_annotator && Drupal.settings.sgmap_group_img_annotator.img_annotator_data) {
      var data = Drupal.settings.sgmap_group_img_annotator.img_annotator_data;
      $.each(data, function(index, value) {
        value_object = jQuery.parseJSON(value);
        var editable_comment = false;
        // if atelier is not blocked and user is admin or writed the comment
        // => comment is editable
        if (blocked == 0 && (isAdmin == 1 || value_object.uid == userid) ) {
          editable_comment = true;
        }
        var myAnnotation = {
          id: index,
          src : img_url,
          text : value_object.text,
          shapes : value_object.shapes,
          editable: editable_comment,
          uid: value_object.uid,
          userName: value_object.userName,
        } 
        anno.addAnnotation(myAnnotation);
      });
   }
    
 
    // create comment
    anno.addHandler('onAnnotationCreated', function(annotation) {
      annotation.uid = userid;
      annotation.userName = userName;
      if(annotation.text) {
        jQuery.post('/api/store/annotations?atelier=' + atelierid, JSON.stringify(annotation), function(response) {
          //for instant edit/delete
          annotation.id = response.id ;
        });
      }
      else {
        anno.removeAnnotation(annotation);
      }
    });

    // edit comment
    anno.addHandler('onAnnotationUpdated', function(annotation) {
      if(annotation.text) {
        jQuery.ajax({
          url: '/api/store/annotations?id=' + annotation.id,
          type: 'PUT',
          data: JSON.stringify(annotation)
        }); 
      }
    });

    // delete comment
    anno.addHandler('onAnnotationRemoved', function(annotation) {
      jQuery.ajax({
        url: '/api/store/annotations?id=' + annotation.id,
        type: 'DELETE'
      });
    });

    // traduction 
    $('.annotorious-editor-button-save').text('Enregister');
    $('.annotorious-editor-button-cancel').text('Annuler');
    $('.annotorious-editor-text').prop("placeholder", "Ajouter un commentaire");
    $('.annotorious-popup-button-edit').prop("title", "Modifier");
    $('.annotorious-popup-button-delete').prop("title", "Supprimer");

    // hide anotator if atelier is blocked
    if (blocked == 1) {
      $('.annotorious-editor-button-save').addClass('hidden');
      $('.annotorious-editor-text').addClass('hidden');
      $('.annotorious-editor').prepend('Cette phase de l\'atelier est bloquée, vous ne pouvez plus commenter.')
      $('.annotorious-editor-button-cancel').text('Fermer');
    }

  });
})(jQuery);

// plugin to add field userName on annotation
annotorious.plugin.AddUserNamePlugin = function(opt_config_options) { }
annotorious.plugin.AddUserNamePlugin.prototype.initPlugin = function(anno) { }
annotorious.plugin.AddUserNamePlugin.prototype.onInitAnnotator = function(annotator) {
  annotator.popup.addField(function(annotation) {
    return '<em>' + annotation.userName + '</em>'
  });
}
anno.addPlugin('AddUserNamePlugin', {});