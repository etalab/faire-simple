<?php

/**
 * menu callback
 * export annotator data form 
 */
function sgmap_group_img_annotator_export_form($form, &$form_state, $group_type, $gid) {
  drupal_set_title('Exporter les données des textes et images à annoter');
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));

  $form['gid'] = array(
    '#type' => 'hidden',
    '#value' => $gid
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Exporter'
  );
  return $form;
}

/**
 * submit handler for sgmap_group_img_annotator_export_form
 */
function sgmap_group_img_annotator_export_form_submit($form, &$form_state) {
  $gid = $form_state['values']['gid'];

  global $user;

  $cols = array(
    'Titre',
    'URL',
    'Commentaire',
    'Auteur',
  );

  array_walk($cols, 'sgmap_export_format_cel');

  $export_file = sgmap_export_get_file_object('annotator', $user, $cols);

  $batch = array(
    'operations' => array(
      array('sgmap_group_img_annotator_export_process', array($gid, $export_file)),
    ),
    'finished'         => 'sgmap_group_img_annotator_export_finished',
    'title'            => t('Processing export batch'),
    'init_message'     => t('Export batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Export batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'sgmap_group_img_annotator') . '/sgmap_group_img_annotator.pages.inc',
  );

  batch_set($batch);
  batch_process('group/node/'.$gid.'/admin/annotator');

}

function sgmap_group_img_annotator_export_process($gid, $export_file, &$context) {

  $query = db_select('sgmap_annotator_annotations', 'a')
    ->condition('a.id_atelier', $gid)
    ->fields ('a', array (
      'id_annotation',
      'id_atelier',
      'annotation',
      'text_nid',
    ));

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results'] = array('file' => $export_file);
  }

  // pagination
  $query->condition('a.id_annotation', $context['sandbox']['current_node'], '>');
  $query->orderBy('a.id_annotation');

  $query->range(0, 100);

  $fp = fopen(drupal_realpath($export_file->uri), 'a');

  $results = $query->execute();
  foreach ($results as $value) {
    $annotation = json_decode($value->annotation);
    // img case
    if(is_array($annotation->shapes)) {
      $user_name = $annotation->userName;
      $comment = $annotation->text;
      if ($annotation->context) {
        $url = $annotation->context;
      }
      
      // get the image title
      if ($url) {
        $url_parsed = parse_url($url);
        $group = node_load($gid);
        parse_str($url_parsed['query'], $query_array);
        $folder_id = 0;
        if ($query_array['folder']) {
          $folder_id = $query_array['folder'];
        }
        $img_id = 0;
        if ($query_array['img']) {
          $img_id = $query_array['img'];
        }
        if ($group->field_group_img_folder[LANGUAGE_NONE][$folder_id]['value']) {
          $current_folder = field_collection_item_load($group->field_group_img_folder[LANGUAGE_NONE][$folder_id]['value']);
          $current_img = field_collection_item_load($current_folder->field_group_img_collection[LANGUAGE_NONE][$img_id]['value']);
          $text_title = $current_img->field_group_img_annotator[LANGUAGE_NONE][0]['title'];
        }
      }
    } 
    // text case
    else {
      $user_name = $annotation->user->name;
      $comment = $annotation->text;
      $url_options = array(
        'query' => array(
          'step' => 'work',
        ),
        'absolute' => TRUE
      );
      if ($value->text_nid) {
        $url_options['query']['nid'] = $value->text_nid;
        $textAnnotation = node_load($value->text_nid);
        $text_title = $textAnnotation->title;
      }
      else {
        // get the first text
        $text_query = db_select('og_membership', 'm')
          ->fields('m', array('etid'))
          ->condition('m.gid', $gid);
        $text_query->innerJoin('node', 'n', 'm.etid = n.nid');
        $text_query->condition('n.type', 'sgmap_group_annotation');
        $text_query->orderBy('n.nid', 'ASC');
        $text_query->range(0, 1);
        $result = $text_query->execute()->fetchField();
        $textAnnotation = node_load($result);
        $url_options['query']['nid'] = $textAnnotation->nid;
        $text_title = $textAnnotation->title;
      }
      $url = url('node/' . $gid , $url_options);
    }

    $record['title']   = $text_title;
    $record['comment'] = $comment;
    $record['url']     = $url;
    $record['auteur']  = $user_name;
    $row = sgmap_group_img_annotator_prepare_row($record);
    fputcsv($fp, $row);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $value->id_annotation;

  }

  fclose($fp);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function sgmap_group_img_annotator_prepare_row($record) {

  $row = array(
    $record['title'],
    $record['url'],
    $record['comment'],
    $record['auteur']
  );
  array_walk($row, 'sgmap_export_format_cel');

  return $row;

}

function sgmap_group_img_annotator_export_finished($success, $results, $operations) {
  if ($success) {
    global $user;
    $file = $results['file'];
    $file = file_save($file);

    sgmap_export_send_export($file);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}