<?php
/**
 * @file
 * sgmap_group_img_annotator.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_group_img_annotator_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_group_img_annotator_image_default_styles() {
  $styles = array();

  // Exported image style: sgmap_group_img_annotator.
  $styles['sgmap_group_img_annotator'] = array(
    'label' => 'SGMAP Group Img Annotator',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 940,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: sgmap_group_img_annotator_teaser.
  $styles['sgmap_group_img_annotator_teaser'] = array(
    'label' => 'SGMAP Group Img Annotator teaser',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
