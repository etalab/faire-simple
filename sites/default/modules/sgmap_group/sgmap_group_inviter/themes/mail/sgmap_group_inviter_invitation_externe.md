[Faire Simple] Invitation à rejoindre un atelier sur faire-simple.gouv.fr
Bonjour,

Nous vous invitons à rejoindre l'atelier "@group.group_title" sur [Faire simple](@group.site_url). Pour participer à cet atelier :

- Inscrivez-vous (ou connectez-vous, si vous avez déjà un compte sur Faire Simple) en suivant ce lien : [@group.register_url](@group.register_url).
- Une fois inscrit et connecté, rejoignez l'atelier en suivant ce lien [@group.group_url](@group.group_url).

Bonne participation !

Votre animateur.