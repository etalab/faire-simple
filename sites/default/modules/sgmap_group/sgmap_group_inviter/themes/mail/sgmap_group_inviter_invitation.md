[Faire Simple] Vous êtes invités à rejoindre l'atelier "@group.group_title"
Bonjour @group.invited_name,

@group.inviter_name vous invite à rejoindre l'atelier "@group.group_title". 
Pour le rejoindre il vous suffit de suivre ce lien: @group.group_url puis de cliquer sur le bouton "Rejoindre l'atelier".