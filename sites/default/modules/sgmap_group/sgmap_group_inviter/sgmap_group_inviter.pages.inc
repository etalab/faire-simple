<?php

/**
 * page callback: invitation page
 */
function sgmap_group_inviter_invitation_page($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));
  $form['users'] = array(
    '#type' => 'textfield',
    '#title' => 'Inviter des utilisateurs',
    '#size' => '100',
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#autocomplete_path' => 'sgmap_group/' . $gid .'/inviter_autocomplete'
  );

  $form['gid'] = array(
    '#type' => 'hidden',
    '#value' => $gid
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Envoyer une invitation'
  );
  return $form;
}

/**
 * submit handler for sgmap_group_inviter_invitation_page
 */
function sgmap_group_inviter_invitation_page_submit($form, &$form_state) {
  $users_string = $form_state['values']['users'];
  $gid = $form_state['values']['gid'];
  $users = explode(',', $users_string);
  foreach ($users as $name) {
    preg_match('/'.preg_quote('[').'(.*?)'.preg_quote(']').'/is', $name, $match);
    sgmap_group_invite_save_invit($match[1], $gid);
  }

  drupal_set_message('Votre invitation a été envoyée');
}

/**
 * page callback: externe invitation  page
 */
function sgmap_group_inviter_externe_invitation_page($form, &$form_state, $group_type, $gid) {
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));

  // group members
  $query = db_select('og_membership', 'om');
  $query->condition('om.gid ', $gid);
  $query->condition('entity_type', 'user');
  $query->innerJoin('users', 'u', 'u.uid = om.etid');
  $query->fields('u', array('mail'));

  $result = $query->execute();
  foreach ($result as $data) {
   $exclude[] = $data->mail;
  }

  $query = db_select('sgmap_group_inviter_externe', 'i')
    ->fields('i', array('invited_mail'))
    ->condition('i.gid', $gid)
    ->condition('i.invited_mail', $exclude, 'NOT IN');
  $result = $query->execute()->fetchAll();
  foreach ($result as $value) {
    $invited[] = $value->invited_mail;
  }

  if (count($invited)) {
    $form['invited_container'] = array(
      '#type' => 'fieldset',
      '#title' => 'Emails des personnes déjà invitées mais qui n\'ont pas encore rejoint la plateforme',
    );
    $form['invited_container']['invited'] = array(
      '#markup' => implode(', ', $invited),
    );
  }

  $form['mails'] = array(
    '#type' => 'textarea',
    '#title' => 'Inviter des utilisateurs externe',
    '#description' => 'Saisir les adresses mails séparés par des virgules',
    '#size' => '100',
    '#required' => TRUE,
  );

  $form['gid'] = array(
    '#type' => 'hidden',
    '#value' => $gid
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Envoyer une invitation'
  );
  return $form;
}

/**
 * submit handler for sgmap_group_inviter_externe_invitation_page
 */
function sgmap_group_inviter_externe_invitation_page_submit($form, &$form_state) {

  $mails = $form_state['values']['mails'];
  $gid = $form_state['values']['gid'];
  $group = node_load($gid);
  $mails = explode(',', $mails);
  foreach ($mails as $mail) {
    // save invited mail
    db_merge('sgmap_group_inviter_externe')
      ->key(array('invited_mail' => trim($mail), 'gid' => $gid))
      ->execute();
    $params = array(
        'group_title'  => $group->title,
        'group_url'    => url('node/' . $gid, array('absolute' => TRUE)),
        'site_url' => $GLOBALS['base_url'],
        'register_url' => url('user/register', array('absolute' => TRUE)),
      );
    // send mail notification
    drupal_mail('sgmap_group_inviter', 'sgmap_group_inviter_invitation_externe', $mail, LANGUAGE_NONE, $params);

  }

  drupal_set_message('Votre invitation a été envoyée');
}

/**
 * page callback: autocomplete function for sgmap_group_inviter_invitation_page
 */
function sgmap_group_inviter_get_users($node, $string) {

  $fragments = explode(',', $string);
  foreach ($fragments as $name) {
    if ($name = trim($name)) {
      $uid = preg_match('/'.preg_quote('[').'(.*?)'.preg_quote(']').'/is', $name, $match);
      if(count($match)){
        $uids[] = $match[1];
      }
      $names[$name] = $name;
    }
  }

  $excludes = sgmap_group_inviter_get_exclude($node->nid);

  $fragment = array_pop($names);
  $query = db_select('users', 'u');
  $query->fields('u', array('name', 'uid'));
  $query->condition('u.status', 1);
  $query->innerJoin('field_data_field_user_lastname', 'lastname', 'lastname.entity_id = u.uid');
  $query->innerJoin('field_data_field_user_firstname', 'firstname', 'firstname.entity_id = u.uid');
  $query->condition('lastname.field_user_lastname_value', db_like($fragment) . '%', 'LIKE');
  $query->fields('lastname', array('field_user_lastname_value'));
  $query->fields('firstname', array('field_user_firstname_value'));
  $query->range(0, 10);

  if (!empty($uids)) {
    $query->condition('u.uid', $uids, 'NOT IN');
  }
  if (!empty($excludes)) {
    $query->condition('uid', $excludes, 'NOT IN');
  }

  $results = $query->execute()->fetchAll();
  foreach ($results as $item) {
    $matches['user_'.$item->uid] = $item->field_user_lastname_value . ' ' .  $item->field_user_firstname_value . ' [' . $item->uid .']';
  }

  $prefix = count($names) ? implode(", ", $names) . ", " : '';
  $suggestions = array();
  if(!empty($matches)){
    foreach ($matches as $match) {
      $suggestions[$prefix . $match . ', '] = $match;
    }
  }
  drupal_json_output((object)$suggestions);
}

/**
 * page callback, subscribe user to the group
 * @param  object $node group node
 */
function sgmap_group_inviter_join_group($node) {
  $field_name = og_get_best_group_audience_field('user', $GLOBALS['user']->uid, 'node');
  $og_membership = og_membership_create('node', $node->nid, 'user', $GLOBALS['user']->uid, $field_name);
  // save memership
  $og_membership->save();  

  drupal_set_message('Vous avez rejoint l\'atelier ' . $node->title);
  drupal_goto('node/' .$node->nid);
}