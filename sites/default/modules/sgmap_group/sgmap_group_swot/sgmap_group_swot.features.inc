<?php
/**
 * @file
 * sgmap_group_swot.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_group_swot_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sgmap_group_swot_node_info() {
  $items = array(
    'sgmap_group_proposition' => array(
      'name' => t('Proposition SWOT'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
