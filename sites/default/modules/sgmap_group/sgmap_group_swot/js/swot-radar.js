(function ($) {

  $(document).ready(function(){
    var labels = Drupal.settings.sgmap_group_swot.radar_labels;
    var radar_values = Drupal.settings.sgmap_group_swot.radar_values;
    var edit_default_values = Drupal.settings.sgmap_group_swot.edit_default_values;
    var slider_ids = [];
    for (i = 0; i < labels.length; i++) {
      slider_ids[i] = "#label-" + i;
    }
    var slider_ids_concat = slider_ids.join(); 

    var radarOptions = {
        meshSize: 18,
        labels: labels,
        labelFontSize: 12,
        drawLabels: true,
        labelFontColor: '#FFF',
        armFill: 'rgba(251, 96, 40, 0.8)',
        armStroke: '#DDD',
        armStrokeWidth: 1,
        drawArms: true,
        meshFill: 'none',
        meshStroke: '#BBB',
        meshStrokeWidth: 1,
        drawMesh: true,
        pathFill: 'rgba(251, 96, 40, 0.8)',
        pathStroke: 'rgba(251, 96, 40, 0.8)',
        pathStrokeWidth: 2,
        pathCircleOuterRadius: 2.2,
        pathCircleInnerRadius: 1.5,
        drawPathCircles: true,
        drawValues: false,
        max: 10
    };

    var paper = Raphael('radar', 320, 240);
    var radar = paper.radar(160, 120, 90, radar_values, radarOptions);

    
    // editing radar
    var editPaper = Raphael('edit-radar', 320, 240);
    var editRadar = editPaper.radar(160, 120, 90, edit_default_values, radarOptions);
    
    $('#cancel-btn').click(function() {
      editRadar.updateValues(edit_default_values);
      set_slider_default_values(edit_default_values);
    });
    
    // change radar on slider actions
    var label_values = [];
    $( slider_ids_concat ).slider({
      min: 0,
      max: 10,
      change: function( event, ui ) {
        for (i = 0; i < labels.length; i++) {
          label_values[i] = $( "#label-" + i ).slider( "value" );
          $( "#input-label-" + i ).val($( "#label-" + i ).slider( "value" ));
        }
        editRadar.updateValues(label_values);
      }
    });

    set_slider_default_values(edit_default_values);

    //set slider default values
    function set_slider_default_values(edit_default_values) {
      var values_count = 0;
      $.each(edit_default_values, function(index, value) {
        $( "#label-" + values_count ).slider( "option", "value", value );
        values_count++;
      });
    }

    var tabPane = $('.tab-pane');
    $(".personal-radar").hide();
    $(".tab-link").click(function(event){
      var targetTabUrl = $(this).prop("href");
      var targetTab = targetTabUrl.substring(targetTabUrl.indexOf("#")+1);
      tabPane.hide();
      $('.' + targetTab).show();
    });
    
    // hide/show slider comment form
    $(".open-comment").click(function(event){
      $(this).addClass("open");
      var comment_form_class = $(this).prop("target");
      var comment_form = $('.' + comment_form_class);
      if(comment_form.hasClass('commentFormClosed')){
        comment_form.removeClass('commentFormClosed');
        $(this).addClass("open");
      }
      else{
        comment_form.addClass('commentFormClosed');
        $(this).removeClass("open");
      }
    });

  });
})(jQuery);



