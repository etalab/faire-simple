(function ($) {
  Drupal.behaviors.radarSwot = {
    attach: function (context, settings) {
      var labels = settings.sgmap_group_swot.radar_labels;
      var propositions_id = $('.propositions_id').text();
      var teaser_propositions_id = jQuery.parseJSON(propositions_id);

      var radarOptions = {
        meshSize: 18,
        labels: labels,
        labelFontSize: 12,
        drawLabels: true,
        labelFontColor: '#FFF',
        armFill: 'rgba(251, 96, 40, 0.8)',
        armStroke: '#DDD',
        armStrokeWidth: 1,
        drawArms: true,
        meshFill: 'none',
        meshStroke: '#BBB',
        meshStrokeWidth: 1,
        drawMesh: true,
        pathFill: 'rgba(251, 96, 40, 0.8)',
        pathStroke: 'rgba(251, 96, 40, 0.8)',
        pathStrokeWidth: 2,
        pathCircleOuterRadius: 2.2,
        pathCircleInnerRadius: 1.5,
        drawPathCircles: true,
        drawValues: false,
        max: 10
      };
      if(teaser_propositions_id) {
        $.each(teaser_propositions_id, function(index, value) {
          if($('radar-teaser-' + index).hasClass('processed') == false) {
            Raphael('radar-teaser-' + index, 320, 240).radar(160, 120, 90, value, radarOptions);
            $('#radar-teaser-' + index).addClass('processed');
          }
        });
      }
      
      $('.propositions_id').text('');
    }
  };

})(jQuery);



