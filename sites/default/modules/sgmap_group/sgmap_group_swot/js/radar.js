!(function (Raphael) {
    "use strict";
    function Radar(paper, cx, cy, r, values, opts) {
        if (!values || values.length === 0) {
            throw 'Values array is required';
        }

        this.$r = Raphael; //for minification;

        this.paper      = paper;
        this.cx         = cx;
        this.cy         = cy;
        this.r          = r;
        this.values     = values;
        this.opts       = opts || {};
        this.startAngle = 270;
        this.points = new Array();

        var defaultOpts = {
            meshSize: 30,
            labels: [],
            labelFontSize: 14,
            labelFontColor: '#000',
            valueFontSize: 14,
            drawLabels: true,
            drawValues: true,
            armFill: 'none',
            armStroke: 'rgba(255, 106, 0, .5)',
            armStrokeWidth: 1,
            drawArms: true,
            meshFill: 'none',
            meshStroke: 'rgba(120, 120, 120, .5)',
            meshStrokeWidth: 1,
            drawMesh: true,
            max: Math.max.apply(Math, this.values),
            pathFill: 'none',
            pathStroke: '#0026ff',
            pathStrokeWidth: 3,
            pathCircleOuterRadius: 4,
            pathCircleInnerRadius: 2,
            drawPathCircles: true,
            closePath: true
        };

        //replacing default opts with explicitly provided
        for (var prop in this.opts) {
            defaultOpts[prop] = this.opts[prop];
        }

        this.opts = defaultOpts;

        this.render(this.cx, this.cy, this.r, this.startAngle, this.values, this.opts);

    };


    Radar.prototype.render = function (cx, cy, r, startAngle, values, opts) {

        var angle = 360 / values.length;

        // arms
        if (opts.drawArms) {
            this.renderArms(cx, cy, r, startAngle, angle, values, opts);
        }

        // mesh
        if (opts.drawMesh) {
            this.renderMesh(cx, cy, r, startAngle, angle, values, opts);
        }

        this.path = this.renderPath(cx, cy, r, startAngle, values, opts.max);

        if (opts.drawPathCircles) {
            this.renderPoints(cx, cy, r, startAngle, angle, values, opts);
        }

        if (opts.drawLabels) {
            this.renderLabels(cx, cy, r, startAngle, angle, values, opts);
        }
    };

    Radar.prototype.renderLabels = function (cx, cy, r, startAngle, angle, values, opts) {
        var i = opts.labels.length;
        while (i--) {
            var textObject = this.renderLabel(cx, cy, r, startAngle + angle * i, opts.labelFontSize, opts.labelFontColor);

            var fulltext = opts.labels[i]; 
            var text = this.paper.text(textObject.x, textObject.y, fulltext).attr(textObject.attr);
            fulltext = fulltext.replace("-", "- ").replace("/", "/ ");

            var words = fulltext.split(" ");
            var wordsCount = words.length;

            var maxwidth = Math.min(textObject.x, this.paper.width - textObject.x);
            var maxheight = Math.min(textObject.y, this.paper.height - textObject.y);


            var fontSizeMin = 8;
            for (var fontSize = opts.labelFontSize; fontSize >= fontSizeMin; --fontSize) {
                text.attr("font-size", fontSize);
                var tempText = "";
                for (var i1 = 0; i1 < wordsCount; ++i1) {
                    text.attr("text", tempText + " " + words[i1]);
                    if (text.getBBox().width > maxwidth) {
                        tempText += "\n" + words[i1];
                    } else {
                        tempText += " " + words[i1];
                    }
                }
                text.attr("text", tempText);
                if (text.getBBox().width > maxwidth) {
                    tempText = tempText.replace("-", "-\n");
                    text.attr("text", tempText);
                }
                if (fontSize == fontSizeMin ||
                    (text.getBBox().height <= maxheight &&
                     (text.getBBox().width <= maxwidth || wordsCount > 1))) {
                    var titleCountLines = tempText.split("\n").length;
                text.attr("text", tempText.substring(1));
                break;
                }
            }
        }
    };

    Radar.prototype.updateValues = function(values) {
        var angle = 360 / values.length;
        this.values = values;
        this.renderPoints(this.cx, this.cy, this.r, this.startAngle, angle, this.values, this.opts);
        this.updatePath(this.cx, this.cy, this.r, this.startAngle, this.values, this.opts.max);
    }

    Radar.prototype.renderPoints = function (cx, cy, r, startAngle, angle, values, opts) {
        for (var i = 0, l = this.values.length; i < l; i ++) {
            if(typeof this.points[i] !== 'undefined') {
                this.movePoint(this.points[i], cx, cy, r, startAngle + angle * i, values[i], opts.max, opts.labels[i])
            } else {
                this.points.push(
                    this.createPoint(cx, cy, r, startAngle + angle * i, values[i], opts.max, opts.labels[i])
                );
            }
        }
    };

    Radar.prototype.renderArms = function (cx, cy, r, startAngle, angle, values, opts) {
        var i = 0, l = values.length;
        while (i < l) {
            this.paper.path(this.buildArm(cx, cy, r, startAngle + angle * i)).attr({
                fill: opts.armFill,
                stroke: opts.armStroke,
                "stroke-width": opts.armStrokeWidth
            });
            ++i;
        }
    };

    Radar.prototype.renderMesh = function (cx, cy, r, startAngle, angle, values, opts) {
        var meshCount = Math.floor(r / opts.meshSize);
        var meshHeight = r / meshCount;
        var meshRadius = meshHeight;
        var meshes = [];
        while (meshCount--) {
            meshes.push(this.buildMeshLine(cx, cy, meshRadius, startAngle, angle));
            meshRadius += meshHeight;
        }
        var i = 0, l = meshes.length;
        while (i < l) {
            this.paper.path(meshes[i]).attr({
                fill: opts.meshFill,
                stroke: opts.meshStroke,
                "stroke-width": opts.meshStrokeWidth
            });
            ++i;
        }
    };

    Radar.prototype.renderLabel = function (cx, cy, r, angle, labelFontSize, labelFontColor) {
        var rad = this.$r.rad(angle);
        var x = cx + r * Math.cos(rad);
        var y = cy + r * Math.sin(rad);
        return {
            x: (Math.round(x) === cx) ? x : (x < cx ? x - 10 : x + 10),
            y: (Math.round(y) === cy) ? y : (y < cy ? y - 10 : y + 10),
            attr: {
                "text-anchor": (Math.round(x) === cx) ? "middle" : (x < cx ? "end" : "start"),
                "font-size": labelFontSize,
                "fill": labelFontColor
            }
        };
    };

    Radar.prototype.buildArm = function (cx, cy, r, angle) {
        var rad = this.$r.rad(angle);
        var x = cx + (r * Math.cos(rad));
        var y = cy + (r * Math.sin(rad));
        return ["M", cx, cy, "L", x, y, "Z"].join(",");
    };

    Radar.prototype.buildMeshLine = function (cx, cy, r, startAngle, angle) {
        var mesh = ["M"];
        var circle = startAngle + 360;
        while (startAngle < circle) {
            var x = cx + r * Math.cos(this.$r.rad(startAngle));
            var y = cy + r * Math.sin(this.$r.rad(startAngle));
            mesh.push(x);
            mesh.push(y);
            mesh.push("L");
            startAngle += angle;
        }
        mesh.push("Z");
        return mesh.join(",");
    };

    Radar.prototype.getPath = function (cx, cy, r, startAngle, values, max) {
        var pathData = [];
        var i = 0, l = values.length;
        while (i < l) {
            var rad = this.$r.rad(startAngle + 360 / values.length * i);
            pathData.push(i == 0 ? "M" : "L");
            pathData.push(cx + r / max * values[i] * Math.cos(rad));
            pathData.push(cy + r / max * values[i] * Math.sin(rad));
            ++i;
        }

        if (this.opts.closePath) {
            pathData.push("Z");
        }

        return pathData.join(",");
    }

    Radar.prototype.updatePath = function (cx, cy, r, startAngle, values, max) {

        var pathString = this.getPath(cx, cy, r, startAngle, values, max);

        return this.path.animate({ 'path' : pathString }, 300);
    }

    Radar.prototype.renderPath = function (cx, cy, r, startAngle, values, max) {

        var pathString = this.getPath(cx, cy, r, startAngle, values, max);

        return this.paper.path(pathString).attr({
            "stroke": this.opts.pathStroke,
            "fill": this.opts.pathFill,
            "stroke-width": this.opts.pathStrokeWidth,
            "stroke-linejoin": 'round'
        });
    };

    Radar.prototype.getPointCoords = function (cx, cy, r, angle, value, max) {
        var rad = this.$r.rad(angle);

        var p = {
            x: cx + r / max * value * Math.cos(rad),
            y: cy + r / max * value * Math.sin(rad)
        };

        return p;
    };

    Radar.prototype.createPoint = function (cx, cy, r, angle, value, max, title) {

        var p = this.getPointCoords(cx, cy, r, angle, value, max);

        var point = this.paper.set();

        point.push(
            this.paper.circle(p.x, p.y, this.opts.pathCircleOuterRadius * 2).attr({ fill: this.opts.pathStroke, stroke: 'none', title: title + ": " + value }),
            this.paper.circle(p.x, p.y, this.opts.pathCircleInnerRadius * 2).attr({ fill: "#fff", stroke: "none", title: title + ": " + value })
        );

        return point;
    };

    Radar.prototype.movePoint = function (point, cx, cy, r, angle, value, max, title) {

        var p = this.getPointCoords(cx, cy, r, angle, value, max);

        point.forEach(function(el) {
            el.animate({cx: p.x, cy: p.y}, 300);
        });
    };

    Raphael.fn.radar = function (cx, cy, r, values, opts) {
        return new Radar(this, cx, cy, r, values, opts);
    };

})(Raphael);
