<?php
/**
 * @file
 * sgmap_group_swot.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_group_swot_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sgmap_group_swot';
  $context->description = '';
  $context->tag = 'SGMAP Group';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'sgmap_group' => 'sgmap_group',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_group_swot-sgmap_group_swot_publisher' => array(
          'module' => 'sgmap_group_swot',
          'delta' => 'sgmap_group_swot_publisher',
          'region' => 'out_container_bottom_second',
          'weight' => '-10',
        ),
        'sgmap_group_swot-sgmap_group_swot_content_list' => array(
          'module' => 'sgmap_group_swot',
          'delta' => 'sgmap_group_swot_content_list',
          'region' => 'out_container_bottom_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('SGMAP Group');
  $export['sgmap_group_swot'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sgmap_group_swot_content_full';
  $context->description = '';
  $context->tag = 'SGMAP Group';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'sgmap_group_proposition' => 'sgmap_group_proposition',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_group_swot-sgmap_group_swot_content_full' => array(
          'module' => 'sgmap_group_swot',
          'delta' => 'sgmap_group_swot_content_full',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('SGMAP Group');
  $export['sgmap_group_swot_content_full'] = $context;

  return $export;
}
