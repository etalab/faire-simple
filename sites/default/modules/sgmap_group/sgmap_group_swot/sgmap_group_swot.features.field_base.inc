<?php
/**
 * @file
 * sgmap_group_swot.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function sgmap_group_swot_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_comment_swot'.
  $field_bases['field_comment_swot'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_comment_swot',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        's' => 'Forces',
        'w' => 'Faiblesse',
        'o' => 'Opportunités',
        't' => 'Menaces',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
