<article class="comment comment-new comment-by-node-author comment-by-viewer odd">
  <div class="submitted">
    <div class="user-picture">
      <?php print $user_picture ?>
    </div>
    <span>
      <strong>Écrit par <?php print $user_name ?></strong>
      <br>
      <em>Le <?php print $date?></em>
    </span>
  </div>
  <div class="comment-content">
      <div class="field-name-comment-body"><?php print $comment ?></div>
  </div>

</article>