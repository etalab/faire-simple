<?php
/**
 *  Implementation of hook_theme_preprocess()
 */
function sgmap_group_swot_preprocess_sgmap_group_swot_comment_item(&$vars) {
  $vars['user_picture'] =  theme('user_picture', array('account' => $vars['user']));
  $vars['user_name'] = theme('username', array('account' => $vars['user']));
  $vars['date'] = format_date($vars['timestamp'], 'custom', 'd/m/Y - H:i');
}