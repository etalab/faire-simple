<?php

/**
 * menu callback
 * export content data form
 */
function sgmap_group_swot_export_form($form, &$form_state, $group_type, $gid) {
  drupal_set_title('Exporter les données "Appel à proposition avec évaluation détaillée"');
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));

  $form['gid'] = array(
    '#type' => 'hidden',
    '#value' => $gid
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Exporter'
  );
  return $form;
}

/**
 * submit handler for sgmap_group_content_export_form
 */
function sgmap_group_swot_export_form_submit($form, &$form_state) {
  global $user;
  $gid = $form_state['values']['gid'];
  $group = node_load($gid);

  $cols = array(
    'Titre',
    'Description',
    'URL',
    'Auteur',
    'Date de publication',
    'Nombre de personnes qui ont évalué'
  );
  foreach ($group->field_group_tec_choices[LANGUAGE_NONE] as $value) {
    $cols[] = $value['value'];
  }
  $cols[] = 'Moyenne des notes';

  array_walk($cols, 'sgmap_export_format_cel');

  $export_file = sgmap_export_get_file_object('propostions_swot', $user, $cols);
  $batch = array(
    'operations' => array(
      array('sgmap_group_swot_export_process', array($gid, $export_file)),
    ),
    'finished'         => 'sgmap_group_swot_export_finished',
    'title'            => t('Processing export batch'),
    'init_message'     => t('Export batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Export batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'sgmap_group_swot') . '/sgmap_group_swot.export.inc',
  );

  batch_set($batch);
  batch_process('group/node/'.$gid.'/admin/swot/export');
}

function sgmap_group_swot_export_process($gid, $export_file, &$context) {
  $group = node_load($gid);
  $group_tec_choices = count($node->field_group_tec_choices[LANGUAGE_NONE]);
  $query = db_select('node', 'n')
    ->condition('n.type', 'sgmap_group_proposition')
    ->fields ('n', array (
      'nid',
      'title',
      'created'
    ));
  $query->leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
  $query->fields('b', array('body_value'));
  $query->innerJoin('og_membership', 'o', 'o.etid = n.nid');
  $query->condition('o.gid', $gid);
  $query->innerJoin('users', 'u', 'u.uid = n.uid');
  $query->fields('u', array('name'));

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results'] = array('file' => $export_file);
  }

  // pagination
  $query->condition('n.nid', $context['sandbox']['current_node'], '>');
  $query->orderBy('n.nid');

  $query->range(0, 100);

  $fp = fopen(drupal_realpath($export_file->uri), 'a');

  $results = $query->execute();
  foreach ($results as $value) {
    $record['Titre'] = $value->title;
    $record['Description'] = $value->body_value;
    $record['URL'] = url('node/' . $value->nid, array('absolute' => TRUE));
    $record['Auteur'] = $value->name;
    $record['Date de publication'] = format_date($value->created, 'custom', 'd/m/Y');
    $record['Nombre de personnes qui ont évalué'] = _sgmap_group_swot_participations_count($value->nid);
    $averages = sgmap_group_swot_radar_average($value->nid, $group_tec_choices);

    $row = sgmap_group_swot_prepare_row($record, $averages);
    fputcsv($fp, $row);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $value->nid;

  }

  fclose($fp);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

function sgmap_group_swot_prepare_row($record, $averages) {

  $row = array(
    $record['Titre'],
    $record['Description'],
    $record['URL'],
    $record['Auteur'],
    $record['Date de publication'],
    $record['Nombre de personnes qui ont évalué']
  );
  foreach ($averages as $value) {
    $row[] = $value;# code...
  }
  $row[] = array_sum($averages) / count($averages);
  array_walk($row, 'sgmap_export_format_cel');

  return $row;
}

function sgmap_group_swot_export_finished($success, $results, $operations) {
  if ($success) {
    global $user;
    $file = $results['file'];
    $file = file_save($file);

    sgmap_export_send_export($file);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}