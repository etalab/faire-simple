<?php
/**
 * @file
 * Code for the SGMAP Group feature.
 */
define("DAYINSECONDE", 60*60*24);
define("DAYINMONTH", 31);

include_once 'sgmap_group.features.inc';

/**
 * Implements hook_permission().
 */
function sgmap_group_permission() {
  return array(
    'access solutions page admin' =>  array(
      'title' => t('Access solutions page admin'),
      'description' => t('Allow user to modify the page solutions'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function sgmap_group_menu() {
  $items['fabrique-de-solutions'] = array(
    'title' => 'La Fabrique de solutions',
    'page callback' => 'sgmap_group_solution_page',
    'access callback' => true,
    'file' => 'sgmap_group.pages.inc',
    'type' => MENU_CALLBACK,
  );
  $items['liste-membre/group/%'] = array(
    'title' => 'Liste des membres',
    'page callback' => 'sgmap_group_list_member_page',
    'page arguments' => array(2),
    'access callback' => 'sgmap_group_check_permission_view_list',
    'access arguments' => array(2),
    'file' => 'sgmap_group.pages.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/sgmap/solutions'] = array(
    'title' => 'Fabrique de solutions',
    'description' => 'Edition de la page fabrique de solutions',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sgmap_group_config_solutions'),
    'file' => 'sgmap_group.admin.inc',
    'access callback' => 'user_access',
    'access arguments' => array('access solutions page admin'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function sgmap_group_menu_alter(&$items) {
  $items['group/%/%/subscribe']['access callback'] = 'sgmap_group_subscription_access';
  $items['group/%/%/subscribe']['access arguments'] = array(2);
}

/**
 * check group subscription access
 * @param  [integer] $group_nid [group nid]
 * @return [boolean]
 */
function sgmap_group_subscription_access($group_nid) {
  $group = node_load($group_nid);
  if ($group->field_group_type[LANGUAGE_NONE][0]['value'] == 1) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_preprocess_page
 */
function sgmap_group_preprocess_page(&$vars) {
  $node = $vars['node'];
  if ($node->type == 'sgmap_group') {
    $vars['title'] = '';
  }
}

/**
 * Implementation of og_ui_get_group_admin_alter()
 */
function sgmap_group_og_ui_get_group_admin_alter(&$data, $gid) {
  // prevent access to group permissions
  unset($data['permissions'], $data['roles']);
}

/**
 * Implements hook_node_presave().
 */
function sgmap_group_node_presave($node) {
  if ($node->type == 'sgmap_group') {
    $node->comment = 0;
  } 
}

/**
 * Implements hook_node_prepare().
 */
function sgmap_group_node_prepare($node) {

  if ($node->type == 'sgmap_group') {
    $node->comment = 0;
  }
}

/**
 * Implements hook_node_view().
 */
function sgmap_group_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'sgmap_group') {
    global $user;
    $count = sgmap_group_get_member_count($node->nid);
    $node->content['member_count'] = array(
      '#markup' => format_plural($count, $count.' membre', $count.' membres')
    );

    $node->active_phase = sgmap_group_get_active_phase(
      $node->field_group_start_date[LANGUAGE_NONE][0]['value'],
      $node->field_group_p1_end[LANGUAGE_NONE][0]['value'],
      $node->field_group_p2_end[LANGUAGE_NONE][0]['value'],
      $node->field_group_end_date[LANGUAGE_NONE][0]['value'],
      $node
    );

    // no dates given
    if (!$node->field_group_nodate[LANGUAGE_NONE][0]['value']) {
       $node->content['duration'] = array(
        '#markup' => sgmap_group_get_duration(
            $node->field_group_start_date[LANGUAGE_NONE][0]['value'],
            $node->field_group_p1_end[LANGUAGE_NONE][0]['value'],
            $node->field_group_p2_end[LANGUAGE_NONE][0]['value'],
            $node->field_group_end_date[LANGUAGE_NONE][0]['value']
            ),
      );

      $phase = sgmap_group_get_current_phase(
        $node->field_group_start_date[LANGUAGE_NONE][0]['value'],
        $node->field_group_p1_end[LANGUAGE_NONE][0]['value'],
        $node->field_group_p2_end[LANGUAGE_NONE][0]['value'],
        $node->field_group_end_date[LANGUAGE_NONE][0]['value']
      );
    }
   

    $node->content['phase'] = array(
      '#markup' => $phase
    );

    if ($view_mode == 'teaser') {
      $node->content['atelier-policy'] = array(
        '#markup' => $node->field_group_type[LANGUAGE_NONE][0]['value'],
      );

      // no dates given
      if (!$node->field_group_nodate[LANGUAGE_NONE][0]['value']) {
        $node->content['time_left'] = array(
          '#markup' => sgmap_group_get_time_left(
              $node->field_group_start_date[LANGUAGE_NONE][0]['value'],
              $node->field_group_p1_end[LANGUAGE_NONE][0]['value'],
              $node->field_group_p2_end[LANGUAGE_NONE][0]['value'],
              $node->field_group_end_date[LANGUAGE_NONE][0]['value']
              ),
        );
        $node->content['status'] = array(
          '#markup' => sgmap_group_get_status_atelier(
              $node->field_group_start_date[LANGUAGE_NONE][0]['value'],
              $node->field_group_p1_end[LANGUAGE_NONE][0]['value'],
              $node->field_group_p2_end[LANGUAGE_NONE][0]['value'],
              $node->field_group_end_date[LANGUAGE_NONE][0]['value']
              ),
        );
        $node->content['percent'] = array(
          '#markup' => sgmap_group_get_percent_advancement(
              $node->field_group_start_date[LANGUAGE_NONE][0]['value'],
              $node->field_group_p1_end[LANGUAGE_NONE][0]['value'],
              $node->field_group_p2_end[LANGUAGE_NONE][0]['value'],
              $node->field_group_end_date[LANGUAGE_NONE][0]['value']
              ),
        );
      }
      // user is group member
      if (og_is_member('node', $node->nid)) {
        $node->content['user_is_group_member'] = array(
          '#markup' => 1
        );
      }

    }
    if ($view_mode == 'full') {
      $path = drupal_get_path('module', 'sgmap_group');
      drupal_add_js($path . '/js/sgmap_group.js');
      $breadcrumb = array(
        l(t('Home'), '<front>'),
        l('La fabrique de solutions', 'fabrique-de-solutions')
      );
      drupal_set_breadcrumb($breadcrumb);
      // if user is group member
      if (og_is_member('node', $node->nid)) {

        $node->content['member_count'] =  array(
          '#type'  => 'link',
          '#title' => format_plural($count, $count.' membre', $count.' membres'),
          '#href' => 'liste-membre/group/' . $node->nid,
          '#options' => array(
            'html' => TRUE,
            'attributes' => array(
              'class' => array('members-link'),
            ),
          ),
          '#ajax' => array(
            'progress' => array(
              'type' => 'none'
            )
          ),
        );

        $node->content['unsubscribe'] = array(
          '#theme'   => 'link',
          '#text'    => '<i class="icon-exit"></i> Se désinscrire',
          '#path'    => 'group/node/' . $node->nid . '/unsubscribe',
          '#options' => array(
            'html' => TRUE,
            'attributes' => array('class' => array('unsubscription-link','btn', 'btn-grey')),
          )
        );

        $node->content['doc_date'] = array(
         '#markup' => 'jusqu\'au ' . format_date($node->field_group_p1_end[LANGUAGE_NONE][0]['value'], 'custom', 'd M')
        );
        $node->content['work_date'] = array(
           '#markup' => format_date($node->field_group_p1_end[LANGUAGE_NONE][0]['value'], 'custom', 'd M') . ' - ' . format_date($node->field_group_p2_end[LANGUAGE_NONE][0]['value'], 'custom', 'd M')
        );
        $node->content['synth_date'] = array(
           '#markup' => format_date($node->field_group_p2_end[LANGUAGE_NONE][0]['value'], 'custom', 'd M') . ' - ' . format_date($node->field_group_end_date[LANGUAGE_NONE][0]['value'], 'custom', 'd M')
        );
        $node->content['anim_text_date'] = array(
          '#markup' => format_date($node->created, 'custom', 'd.m.y')
        );

      }
      // if user is waitting for subscription validation
      else if (og_is_member('node', $node->nid, 'user', NULL, array(OG_STATE_PENDING))) {
        $node->content['pending_subscription'] = array(
          '#markup' => 'Inscription en attente de validation',
        );
      }
      // if user is already invited to join the group
      else if (module_exists('sgmap_group_inviter') && sgmap_group_inviter_is_user_invited($GLOBALS['user']->uid, $node->nid)) {
        $node->content['subscribe'] = array(
          '#theme'   => 'link',
          '#text'    => '<i class="icon-user-add"></i> rejoindre l\'atelier',
          '#path'    => 'sgmap_group_inviter/join/'. $node->nid,
          '#options' => array(
            'html' => TRUE,
            'attributes' => array('class' => array('subscription-link','btn','btn-orange', 'btn-large')),
          )
        );
      }
      // private group
      else if ($node->field_group_type[LANGUAGE_NONE][0]['value']) {
        $node->content['private_info'] = array(
          '#theme' => 'sgmap_group_private_info',
        );
      }
      // public group
      else if ($node->field_group_type[LANGUAGE_NONE][0]['value'] == 0) {
        $account = user_load($user->uid);
        if (user_is_logged_in()) {
          if (count($account->field_sgmap_public)) {
            $group_path = 'group/node/'. $node->nid . '/subscribe';
          }
          // france connect user
          else {
            $group_path = 'user/' . $user->uid . '/edit';
            $path_query = array('destination' => 'group/node/'. $node->nid . '/subscribe');
          }
        }
        else {
          $group_path = 'user/login';
          $path_query = array('destination' => 'group/node/'. $node->nid . '/subscribe');
        }
        $node->content['subscribe'] = array(
          '#theme'   => 'link',
          '#text'    => '<i class="icon-user-add"></i> rejoindre l\'atelier',
          '#path'    => $group_path,
          '#options' => array(
            'html' => TRUE,
            'attributes' => array('class' => array('subscription-link','btn','btn-orange', 'btn-large')),
            'query' => $path_query
          )
        );
      }
      // METATAG Debatscore

      $elements[] = array(
        '#tag' => 'meta',
        '#attributes' => array(
          'content' => strip_tags($node->field_group_resume[LANGUAGE_NONE][0]['value']),
          'name'    => 'dc.description.abstract',
        ),
      );

      $elements[] = array(
        '#tag' => 'meta',
        '#attributes' => array(
          'content' => $node->status == 1 ? 'en cours' : 'terminé',
          'name'    => 'vp.status',
        ),
      );

      if ($node->field_group_nodate[LANGUAGE_NONE][0]['value'] != 1) {
        $start_date = $node->field_group_start_date[LANGUAGE_NONE][0]['value'];
        $end_date = $node->field_group_end_date[LANGUAGE_NONE][0]['value'];
        $elements[] = array(
          '#tag' => 'meta',
          '#attributes' => array(
            'content' => format_date($start_date, 'custom', 'Y-m-d H:i:s') . ' / ' . format_date($end_date, 'custom', 'Y-m-d H:i:s'),
            'name'    => 'dc.coverage.temporal.periodoftime',
          ),
        );
      }

      sgmap_campagne_debatescore_formater($node, $elements);
    }
  }
}

/**
 * Implements hook_block_info().
 */
function sgmap_group_block_info() {
  $blocks['sgmap_group_animateur_text'] = array(
    'info' => 'SGMAP Group - mot de l\'animateur',
  );
  $blocks['sgmap_group_home_list'] = array(
    'info' => 'SGMAP Group - home list',
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function sgmap_group_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'sgmap_group_animateur_text':
      $block['content'] = sgmap_group_animateur_text();
      break;
    case 'sgmap_group_home_list':
      $block['subject'] = 'Les ateliers en cours';
      $block['content'] = sgmap_group_home_list_block();
      break;
    
  }
  return $block;
}

/**
 * animateur text block
 */
function sgmap_group_animateur_text() {
  global $user;

  $node = menu_get_object();
  if (is_array($node->field_group_anim_date) && count($node->field_group_anim_date)) {
    $date = format_date($node->field_group_anim_date[LANGUAGE_NONE][0]['value'], 'custom', 'd.m.y');
  }
  else {
    $date = format_date($node->created, 'custom', 'd.m.y');
  }
  if (og_is_member('node', $node->nid)) {
    return array(
      '#theme' => 'sgmap_group_animateur_text',
      '#date'  => $date,
      '#text'  => $node->field_group_animateur_text[LANGUAGE_NONE][0]['value'],
    );
  }
}

/**
 * groupe home list block
 */
function sgmap_group_home_list_block() {
  $queue = nodequeue_load_queue_by_name('atelier_home');
  $ateliers = nodequeue_load_nodes($queue->qid, $backward = FALSE, $from = 0, $count = 4, $published_only = FALSE);
  foreach ($ateliers as $node) {
    $node_view = array(
      '#theme'   => 'sgmap_group_home_teaser',
      '#title'   => truncate_utf8($node->title, 100, FALSE, TRUE),
      '#members' => sgmap_group_get_member_count($node->nid),
      '#type'    => $node->field_group_type[LANGUAGE_NONE][0]['value'],
      '#url'     => url('node/' . $node->nid),
      '#is_member' => og_is_member('node', $node->nid)
    );
    $items[] = array(
      'data' => render($node_view),
      'class' => array('span3'),
    );
  }
  return array(
    '#theme_wrappers' => array('container'),
    '#attributes'     => array('class' => array('atelier-list')),
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#attributes'     => array('class' => array('row')),
    ),
    'more' => array(
      '#theme' => 'link',
      '#text' => 'Voir tous les ateliers',
      '#path' => 'fabrique-de-solutions',
      '#options' => array(
        'attributes' => array('class' => array('more-link')),
        'html' => FALSE,
      ),
    )
  );
}

function sgmap_group_form_og_ui_confirm_unsubscribe_alter(&$form, &$form_state, $form_id) {
  $form['description']['#markup'] = '<p>Cette action est irréversible.</p>';
  $form['actions']['submit']['#value'] = 'Quitter';
}

/**
 * Implements hook_form_alter().
 */
function sgmap_group_form_og_ui_confirm_subscribe_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = 'sgmap_group_subscription_request_submit';
}

/**
 * Implements hook_form_alter().
 */
function sgmap_group_form_sgmap_group_node_form_alter(&$form, &$form_state, $form_id) {
  $form['#validate'] = array('sgmap_group_node_form_validate');
}

/**
 * Validate handler for sgmap_group_form_sgmap_group_node_form_alter
 */
function sgmap_group_node_form_validate($form, &$form_state) {
  if (!$form_state['values']['field_group_nodate'][LANGUAGE_NONE][0]['value']) {
    $startDate = $form_state['values']['field_group_start_date'][LANGUAGE_NONE][0]['value'];
    $p1End = $form_state['values']['field_group_p1_end'][LANGUAGE_NONE][0]['value'];
    $p2End = $form_state['values']['field_group_p2_end'][LANGUAGE_NONE][0]['value'];
    $endDate = $form_state['values']['field_group_end_date'][LANGUAGE_NONE][0]['value'];
    if($startDate >= $p1End) {
      form_set_error('field_group_start_date',t('Erreur: La date de début doit être antérieure à la date de fin de la phase 1.'));
      form_set_error('field_group_p1_end');
    }
    if($p1End >= $p2End) {
      form_set_error('field_group_p1_end',t('Erreur: La date de fin de la phase 1 doit être antérieure à la date de fin de la phase 2.'));
      form_set_error('field_group_p2_end');
    }
    if($p2End >= $endDate) {
      form_set_error('field_group_p2_end',t('Erreur: La date de fin de la phase 2 doit être antérieure à la date de fin.'));
      form_set_error('field_group_end_date');
    }
  }
}

/**
 * submit handler for sgmap_group_form_og_ui_confirm_subscribe_alter
 */
function sgmap_group_subscription_request_submit($form, &$form_state) {
  global $user;
  $group = node_load($form_state['values']['gid']);
  $message = $form_state['values']['og_membership_request'][LANGUAGE_NONE][0]['value'];
  $params = array(
    'group_title'    => $group->title,
    'message'        => $message,
    'validation_url' => url('group/node/' . $group->nid . '/admin/people', array('absolute' => TRUE)),
    'user_name'      => $user->name
  );
  $group_admins = sgmap_group_get_roles_uids($group);
  // sending mail to group admins when there is group inscription request 
  foreach ($group_admins as $admin) {
    $mails[] = $admin->mail;
  }
  drupal_mail('sgmap_group', 'sgmap_group_subscription_request', implode(', ', $mails), LANGUAGE_NONE, $params);
  drupal_set_message('Votre demande de rejoindre l\'atelier est envoyée à l\'animateur. Vous recevrez une notification une fois que votre inscription sera validée.');
}

/**
 * Implements hook_field_widget_form_alter().
 */
function sgmap_group_field_widget_form_alter(&$element, &$form_state, $context) {
  // remove drag and drop on group thematique field
  if (isset($element['#field_name'])) {
    switch ($element['#field_name']) {
      case 'field_group_thematique':
      case 'field_group_tec_choices':
        $element['#nodrag'] = TRUE;
      default:
        break;
    }
  }
}

/**
 * Implements hook_og_role_grant().
 */
function sgmap_group_og_role_grant($group_type, $gid, $uid, $rid) {
  // notify user if he is chosen as group admin
  if ($rid == 3) {
    $user  = user_load($uid);
    $group = node_load($gid);

    $params = array(
      'user_name'   => $user->name,
      'group_title' => $group->title,
      'group_url'   => url('node/' . $group->nid, array('absolute' => TRUE)),
    );
    drupal_mail('sgmap_group', 'sgmap_group_is_admin', $user->mail, LANGUAGE_NONE, $params);
  }
}

/**
 * Implements hook_og_membership_update().
 */
function sgmap_group_og_membership_update($og_membership) {
  // notify user if his subscription request is validate
  if ($og_membership->entity_type == 'user' && $og_membership->state == OG_STATE_ACTIVE && $og_membership->original->state == OG_STATE_PENDING) {
    $user  = user_load($og_membership->etid);
    $group = node_load($og_membership->gid);

    $params = array(
      'user_name'   => $user->name,
      'group_title' => $group->title,
      'group_url'   => url('node/' . $group->nid, array('absolute' => TRUE)),
    );
    drupal_mail('sgmap_group', 'sgmap_group_subscription_validate', $user->mail, LANGUAGE_NONE, $params);
  }
}

/**
 * Implements hook_mail().
 */
function sgmap_group_mail($key, &$message, $params) {
  $path = drupal_get_path('module', 'sgmap_group') . '/themes/mail';

  if ($key == 'sgmap_group_subscription_request') {  
    sgmap_mail_notification_prepare_message($message, $path, $key, array(
      '@group.user_name'   => $params['user_name'],
      '@group.group_title' => $params['group_title'],
      '@group.message'     => $params['message'],
      '@group.validation_url'  => $params['validation_url'],
    ));
  }

  if ($key == 'sgmap_group_subscription_validate' || $key == 'sgmap_group_is_admin') {
    sgmap_mail_notification_prepare_message($message, $path, $key, array(
      '@group.user_name'   => $params['user_name'],
      '@group.group_title' => $params['group_title'],
      '@group.group_url'   => $params['group_url'],
    ));
  }
}

/**
 * implements hook_lns_modal
 */
function sgmap_group_lns_modal($key) {
  return array(
    'heading' => 'TEST key',
    'id'      => 'k-id',
    'body'    => $key,
  );
}

/**
 * Implements hook_theme().
 */
function sgmap_group_theme($existing, $type, $theme, $path) {
  return array(
    'sgmap_group_page_solution' => array(
      'template'  => 'sgmap-group-page-solution',
      'variables' => array('codeSolutions' => NULL),
      'path' => $path .'/themes',
    ),
    'sgmap_group_animateur_text' => array(
      'template'  => 'sgmap-group-animateur-text',
      'variables' => array(
        'date' => NULL,
        'text' => NULL,
      ),
      'path' => $path .'/themes',
    ),
    'sgmap_group_user_teaser' => array(
      'template'  => 'sgmap-group-user-teaser',
      'variables' => array(
        'user'   => NULL,
        'group'  => NULL
      ),
      'path'      => $path .'/themes',
      'file'      => 'theme.inc'
    ),
    'sgmap_group_private_info' => array(
      'template'  => 'sgmap-group-private-info',
      'path'      => $path .'/themes',
      'file'      => 'theme.inc'
    ),
    'sgmap_group_home_teaser' => array(
      'template'  => 'sgmap-group-home-teaser',
      'path'      => $path .'/themes',
      'variables' => array(
        'title'   => NULL,
        'members' => NULL,
        'type'    => NULL,
        'url'     => NULL,
        'is_member' => NULL
      ),
    ),
  );
}

/**
 * Get all the atelier and make a list in teaser view
 * @return [array] $item  the render in teaser view of the atelier
 */
function sgmap_group_get_solutions() {
  $items = array();
  $usersQuery = new EntityFieldQuery();
  $results = $usersQuery
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'sgmap_group')
      ->propertyOrderBy('created', 'DESC')
      ->propertyCondition('status', 1)
      ->pager(10)
      ->execute();
  foreach ($results['node'] as $key => $value) {
    $node = node_load($value->nid);
    $node_view = node_view($node, 'teaser');
    $items[] = drupal_render($node_view);
  }
  return $items;
}

/**
 * Get the number of member with the state 1 (active) in the a specific group
 * @param  integer $gid   The groupe id
 * @return integer        The number of member in the group $gid
 */
function sgmap_group_get_member_count($gid) {
  $base_query = new EntityFieldQuery();
  $count = $base_query
      ->entityCondition('entity_type', 'og_membership')
      ->propertyCondition('gid', $gid, '=')
      ->propertyCondition('entity_type', 'user', '=')
      ->propertyCondition('state', '1', '=')
      ->count()
      ->execute();

  return $count;
}

/**
 * Get members with the state 1 (active) in the a specific group
 * @param  integer $gid   The groupe id
 * @return array of users
 */
function sgmap_group_get_members($gid) {
  $uids = db_select('og_membership', 'om')
    ->fields('om', array('etid'))
    ->condition('om.gid', $gid)
    ->condition('om.group_type', 'node')
    ->condition('om.entity_type', 'user')
    ->execute()->fetchCol();

  return user_load_multiple($uids);
}


/**
 * Get the duration for all the atelier
 * @param  int $startDate Start date for the atelier
 * @param  int $p1End     end date for the phase 1
 * @param  int $p2End     end date for the phase 2
 * @param  int $endDate   the end date in the atelier
 * @return [varchar]      The text to display in the atelier view
 */
function sgmap_group_get_duration($startDate, $p1End, $p2End, $endDate) {
  $numDay = ($endDate - $startDate)/(DAYINSECONDE);
  if ($numDay < DAYINMONTH) {
    $count = intval($numDay);
    return format_plural($count, $count . ' jour', $count . ' jours');
  } 
  else {
    $numMois = intval($numDay/DAYINMONTH);
    return $numMois.' mois';
  }
}

/**
 * Get the time left in the current phase
 * @param  int $startDate Start date for the atelier
 * @param  int $p1End     end date for the phase 1
 * @param  int $p2End     end date for the phase 2
 * @param  int $endDate   the end date in the atelier
 * @return [varchar]      Return the text to display in the teaser view of an atelier
 */
function sgmap_group_get_time_left($startDate, $p1End, $p2End, $endDate) {
  $currentDate = time();
  $phaseID = sgmap_group_get_current_phase($startDate, $p1End, $p2End, $endDate);
  $nbDay = '';
  switch ($phaseID) {
    case '1':
      $nbDay = intval(($p1End - $currentDate)/(DAYINSECONDE)); 
      break;
    case '2':
      $nbDay = intval(($p2End - $currentDate)/(DAYINSECONDE));
      break;
    case '3':
      $nbDay = intval(($endDate - $currentDate)/(DAYINSECONDE));
      break;
  }
  if($nbDay > 0) {
    if($nbDay > 1) {
      $nbDay .= ' jours restants';
    } else {
      $nbDay .= ' jour restant';
    }
  }
  return $nbDay;
}

/**
 * Return a correct value to display function to the current phase
 * @param  int $startDate Start date for the atelier
 * @param  int $p1End     end date for the phase 1
 * @param  int $p2End     end date for the phase 2
 * @param  int $endDate   the end date in the atelier
 * @return [varchar]      The text to display in the teaser mode of the atelier
 */
function sgmap_group_get_status_atelier($startDate, $p1End, $p2End, $endDate){
  $phaseID = sgmap_group_get_current_phase($startDate, $p1End, $p2End, $endDate);
  $phase = '';
  switch ($phaseID) {
    case '0':
      $phase = 'A venir';
      break;
    case '1':
      $phase = 'Documentation'; 
      break;
    case '2':
      $phase = 'Travail en commun';
      break;
    case '3':
      $phase = 'Recommandation';
      break;
    case '4':
      $phase = 'Terminé';
      break;
  }
  return $phase;
}

/**
 * Get the current phase for a atelier defined with the 4 dates required
 * @param  int $startDate Start date for the atelier
 * @param  int $p1End     end date for the phase 1
 * @param  int $p2End     end date for the phase 2
 * @param  int $endDate   the end date in the atelier
 * @return [int]          The current phase
 *                            0 : not started
 *                            1 : Phase 1 Documentation
 *                            2 : Phase 2 Common Work
 *                            3 : Phase 3 Synthesis
 *                            4 : finished
 */
function sgmap_group_get_current_phase($startDate, $p1End, $p2End, $endDate, $node = NULL) {
  $currentDate = time();

  // get the current unblocked phase
  if ($node && count($node->field_group_blocked[LANGUAGE_NONE])) {
    foreach($node->field_group_blocked[LANGUAGE_NONE] as $value) {
      $blocked_phase[] = $value['value'];
    }
    $phases = array('1', '2', '3');
    $unblocked = array_diff($phases, $blocked_phase);

    return current($unblocked);
  }


  //no given date case
  if ($startDate == $endDate) {
    return '1';
  }
  else if ($currentDate < $startDate) {
    return '0';
  }
  else if ( ($startDate < $currentDate) && ($currentDate < $p1End) ) {
    return '1';
  }
  else if ( ($p1End < $currentDate) && ($currentDate < $p2End) ) {
    return '2';
  }
  else if ( ($p2End < $currentDate) && ($currentDate < $endDate) ) {
    return '3';
  }
  else if ($currentDate > $endDate) {
    return '4';
  }
}

/**
 * get the active phase for an atelier
 */
function sgmap_group_get_active_phase($startDate, $p1End, $p2End, $endDate, $node = NULL) {
  $query_parameters = drupal_get_query_parameters();
  switch ($query_parameters['step']) {
    case 'doc':
      return '1';
      break;
    case 'work':
      return '2';
      break;
    case 'synthese':
      return '3';
      break;
   
    default:
      return sgmap_group_get_current_phase($startDate, $p1End, $p2End, $endDate, $node);
      break;
  }
}

/**
 * check if user is group member
 * @param  [integer] $gid [group nid]
 * @return boolean
 */
function sgmap_group_check_permission_view_list($gid) {
  if (og_is_member('node', $gid)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * helper: get all group administrator members
 * @param  object $node  [group node]
 * @param  array  $roles
 * @return array of users
 */
function sgmap_group_get_roles_uids($node, $roles = array('administrator member')) {
  $og_roles = og_roles('node', $node->type, $node->nid, FALSE, TRUE); 
  foreach ($og_roles as $rid => $name) {
    if (in_array($name, $roles)) {
      $match_rid = $rid;
    }
  }
  $matchs[] = $node->uid;                                    
  $uids = db_select('og_users_roles', 'ogur')->fields('ogur', array('uid'))
    ->condition('rid', $match_rid, '=')
    ->condition('gid', $node->nid, '=')
    ->condition('group_type', 'node')
    ->execute()->fetchCol();
  $matchs = array_merge($matchs, $uids);
  return user_load_multiple($matchs);
}

/**
 * helper: check if user is group admin
 * @param integer $uid user id
 * @param object $node group node
 */
function sgmap_group_is_user_admin($uid, $node) {
  $admins = sgmap_group_get_roles_uids($node);

  if (array_key_exists($uid, $admins)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Calcul the percentage advancement in the specific phase
 * @param  int $startDate Start date for the atelier
 * @param  int $p1End     end date for the phase 1
 * @param  int $p2End     end date for the phase 2
 * @param  int $endDate   the end date in the atelier
 * @return int            percentage of advancement in the actual phase
 */
function sgmap_group_get_percent_advancement($startDate, $p1End, $p2End, $endDate) {
  $phase = sgmap_group_get_current_phase($startDate, $p1End, $p2End, $endDate);
  $currentDate = time();
  if(!in_array($phase, array(1,2,3))){
    return false;
  }
  switch ($phase) {
    case '1':
      $begin = $startDate;
      $end = $p1End;
      break;

    case '2':
      $begin = $p1End;
      $end = $p2End;
      break;

    case '3':
      $begin = $p2End;
      $end = $endDate;
      break;
  }
  // Calcul of the percentage
  return intval(($currentDate-$begin)/($end-$begin)*100);
}

/**
 * helper: get group tags (thematique)
 */
function _sgmap_group_get_thematiques($gid, $delta = NULL) {
  $query = db_select('field_data_field_group_thematique', 't');
  $query
      ->condition('t.entity_id', $gid)
      ->fields ('t', array ('delta','field_group_thematique_value'));
  if ($delta != NULL) {
    $query->condition('t.delta', $delta);
  }
  $result = $query->execute()->fetchAll();
  $tags = array();
  foreach ($result as $value) {
    $tags[$value->delta] = $value->field_group_thematique_value;
  }
  return $tags;
}

/**
 * helper: check if group phase is blocked
 */
function sgmap_group_is_blocked($node, $active_phase = NULL) {
  if (count($node->field_group_blocked)) {
    if ($active_phase == NULL) {
      $active_phase = $node->active_phase;
    }
    foreach ($node->field_group_blocked[LANGUAGE_NONE] as $value) {
      if ($active_phase == $value['value']) {
        return TRUE;
      }
    }
  }
}

/**
 * helper: get user participation count in group
 */
function sgmap_group_user_participations_count($uid) {
  // documentations count
  $doc_count = db_select('node', 'n')
  ->fields('n', array('nid'))
  ->condition('n.type', array_keys(sgmap_group_content_get_types()), 'IN')
  ->condition('n.uid', $uid)
  ->countQuery()->execute()->fetchField();

  // documentation comments count
  $doc_comment_count_query = db_select('comment', 'c');
  $doc_comment_count_query->fields('c', array('cid'));
  $doc_comment_count_query->condition('c.uid', $uid);
  $doc_comment_count_query->innerJoin('node', 'n', 'n.nid = c.nid');
  $doc_comment_count_query->condition('n.type', array_keys(sgmap_group_content_get_types()), 'IN');
  $doc_comment_count = $doc_comment_count_query->countQuery()->execute()->fetchField();

  // swot content count
  $swot_count = db_select('node', 'n')
  ->fields('n', array('nid'))
  ->condition('n.type', 'sgmap_group_proposition')
  ->condition('n.uid', $uid)
  ->countQuery()->execute()->fetchField();

  //swot participation count
  $swot_participation_query = db_select('sgmap_group_swot_radar', 'r');
  $swot_participation_query->condition('r.uid', $uid);
  $swot_participation_query->fields ('r', array ('nid'));
  $swot_participation_query->innerJoin('node', 'n', 'n.nid = r.nid');
  $swot_participation_query->condition('n.type', 'sgmap_group_proposition');
  $swot_participation_query->groupBy('r.nid');
  $swot_participation_query_count = $swot_participation_query->countQuery()->execute()->fetchField();

  // wiki participation count
  $wiki_participation_query = db_select('node_revision', 'r');
  $wiki_participation_query->fields('r', array('nid'));
  $wiki_participation_query->condition('r.uid', $uid);
  $wiki_participation_query->innerJoin('node', 'n', 'n.nid = r.nid');
  $wiki_participation_query->condition('n.type', 'sgmap_group_wiki');
  $wiki_participation_count = $wiki_participation_query->countQuery()->execute()->fetchField();

  // annotation count
  $annotation_count = db_select('sgmap_annotator_annotations', 'a')
  ->fields('a', array('id_annotation'))
  ->condition('a.annotation', '%"id":"' . $uid. '"%', 'LIKE')
  ->countQuery()->execute()->fetchField();

  return $doc_count + $doc_comment_count + $swot_count + $swot_participation_query_count + $wiki_participation_count + $annotation_count;
}