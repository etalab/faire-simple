
var myApp = angular.module("chatApp", ["firebase", "luegg.directives"]);


myApp.controller('ChatController', ['$scope', '$firebase', function($scope, $firebase) {
  var atelierId = Drupal.settings.atelier;
  var userName  = Drupal.settings.userName;
  var userId    = Drupal.settings.userID;
  var userPic   = Drupal.settings.avatar;
  var chatDate  = Drupal.settings.chatDate;
  var token     = Drupal.settings.firebase_token;
  var firebase_url = Drupal.settings.firebase_url;

  var ref = new Firebase(firebase_url + "/chat/room-" + atelierId);
  // authentication
  ref.auth(token);

  // sending and get messages
  $scope.messagesList = $firebase(ref);

  $scope.addMessage = function() {
    if($scope.message != undefined && $scope.message != "") {
      onLineOffline();
      var chatDate = new Date();
      var chatMinutes = (chatDate.getMinutes()<10?'0':'') + chatDate.getMinutes();
      var chatHour = chatDate.getHours() + ':' + chatMinutes;
      $scope.messagesList.$add({uid: userId, from: userName, pic: userPic, body: $scope.message, date:chatHour});
      $scope.message = "";
    }
  }

  $scope.removeActiveClass = function() {
    $scope.chatActiveClass = 'not-active';
  };

  ref.on('child_added', function(snapshot) {
    var msgData = snapshot.val();
    if (msgData.uid != userId) {
      $scope.chatActiveClass = 'active';
    }
  });

  $scope.messagesList.$on('loaded', function() {
    $scope.chatActiveClass = 'not-active';
  });

  // onLine/Offline manager
  function onLineOffline() {
    var myConnectionsRef = new Firebase(firebase_url + "/connection/room-" + atelierId);
    // authentication
    myConnectionsRef.auth(token);
    $scope.onelineUsers = $firebase(myConnectionsRef);
    myConnectionsRef.child('user-' + userId).set({name: userName, pic: userPic, uid: userId});
    // remove user on disconnect
    myConnectionsRef.child('user-' + userId).onDisconnect().remove();
  }

  onLineOffline();

}]);


(function ($) {
  $(document).ready(function(){
    
    var chatBlockId = $("#block-sgmap-group-chat-sgmap-group-chat");
    // hide/show chat
    $("#chatbox-header").click(function(){
      if(chatBlockId.hasClass('chatClosed')){
        chatBlockId.removeClass('chatClosed');
        $.cookie("chat_closed", "false");
      }
      else{
        chatBlockId.addClass('chatClosed');
        $.cookie("chat_closed", "true")
      }
    });

    // add cookie to remember last chat behavior (open/closed)
    if ($.cookie("chat_closed") == "false") {
      chatBlockId.removeClass('chatClosed');
    }

  });
})(jQuery);

