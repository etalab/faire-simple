<div id="chat-block" ng-app="chatApp" ng-controller="ChatController" class="{{chatActiveClass}}" ng-click="removeActiveClass()">
  <div class="chatbox-header" id="chatbox-header">
    <h2 class="block-title">Discussion instantanée</h2>
    <a class="close"><i class="icon-minus"></i></a>
    <a class="close open"><i class="icon-plus"></i></a>
  </div>
  <div class="chatUsersBox clearfix">
    <ul>
        <li ng-repeat="user in onelineUsers">
          <a href="/user/{{user.uid}}" title="{{user.name}}">
            <img typeof="foaf:Image" src="{{user.pic}}" width="30" height="30">
          </a>
        </li>
    </ul>
  </div>
  <div id="messages" class="chatbox-body clearfix" scroll-glue>
      <div class="msg-box" ng-repeat="msg in messagesList">
          <img typeof="foaf:Image" src="{{msg.pic}}" width="30" height="30">
          <em>{{msg.from}}</em> ({{msg.date}}):
          <div class="message">
            {{msg.body}}
          </div>
      </div>
  </div>
  <div class="chatbox-footer">
    <form class="form-inline" ng-submit="addMessage()">
      <input type="text" ng-model="message" placeholder="Message...">
      <button class="btn btn-orange btn-small" type="submit">Envoyer</button>
    </form>
  </div>
</div>