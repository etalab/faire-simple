<?php
/**
 * @file
 * sgmap_group_chat.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_group_chat_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sgmap_group_chat';
  $context->description = '';
  $context->tag = 'SGMAP Group';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'sgmap_group' => 'sgmap_group',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_group_chat-sgmap_group_chat' => array(
          'module' => 'sgmap_group_chat',
          'delta' => 'sgmap_group_chat',
          'region' => 'out_footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('SGMAP Group');
  $export['sgmap_group_chat'] = $context;

  return $export;
}
