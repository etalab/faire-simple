<?php

function sgmap_group_config_solutions() {
  $form['sgmap_group_solutions_value'] = array(
    '#title' => 'En tête de la plage Fabrique de solutions',
    '#type'  => 'text_format',
    '#value' => variable_get('sgmap_group_solutions_value', NULL),
  );

  $form['panel']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Enregistrer'),
  );

  return $form;
}

function sgmap_group_config_solutions_submit($form, &$form_state) {
  variable_set('sgmap_group_solutions_value', $form_state['input']['sgmap_group_solutions_value']['value']);
}