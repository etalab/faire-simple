<?php
/**
 * @file
 * Code for the sgmap_group_annotator feature.
 */

include_once 'sgmap_group_annotator.features.inc';

/**
 * Implements hook_node_view().
 */
function sgmap_group_annotator_node_view($node, $view_mode, $langcode) {
  global $user;

  $step = $node->active_phase;
  $needAnnotator = false;
  if ($node->type == 'sgmap_group' 
    && $view_mode == 'full' 
    && $step == 2 
    && og_is_member('node', $node->nid))
  {
    // Check if there is already a text to be annotated, if not, we don't need to load Annotator.js
    $query = db_select('og_membership', 'm')
        ->fields('m', array('etid'))
        ->condition('m.gid', $node->nid);
    $query->innerJoin('node', 'n', 'm.etid = n.nid');
    $query->condition('n.type', 'sgmap_group_annotation');
    $result = $query->execute()->fetchField();
    if (!empty($result)){
      $needAnnotator = true;
    }
    if ($needAnnotator 
        && (!count($node->field_group_tec) || $node->field_group_tec[LANGUAGE_NONE][0]['value'] == 'annotation'))
    {
      $url_query = drupal_get_query_parameters();
      $admins = sgmap_group_get_roles_uids($node);
      foreach ($admins as $admin) {
        $uids[] = $admin->uid;
      }
      $path = drupal_get_path('module', 'sgmap_group_annotator');
      drupal_add_js($path . '/js/json2.js');
      drupal_add_js($path . '/js/xpath.js');
      drupal_add_js($path . '/js/annotator-full.min.js');
      drupal_add_css($path . '/css/annotator.min.css');
      drupal_add_js($path . '/js/sgmap_annotator.js');
      $settings['userAnnotatorID'] = $user->uid;
      $settings['userName'] = $user->name;
      $settings['atelier'] = $node->nid;   
      $settings['admins'] = $uids;
      $settings['textNID'] = isset($url_query['nid']) ? $url_query['nid'] : 0;
      // if atelier is blocked
      $settings['blocked'] = 0;
      if (sgmap_group_is_blocked($node)) {
        $settings['blocked'] = 1;
        $settings['userAnnotatorID'] = 0;
      } 
      drupal_add_js($settings, 'setting');
    }
  }
}

/**
 * Implements hook_block_info().
 */
function sgmap_group_annotator_block_info() {
  $blocks['sgmap_group_annotator_work'] = array(
    'info' => t('SGMAP Group annotator work'),
    'cache' => DRUPAL_NO_CACHE
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function sgmap_group_annotator_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'sgmap_group_annotator_work':
      $block['content'] = sgmap_group_annotator_work();
      break;
  }
  return $block;
}

/**
 * Work block
 */
function sgmap_group_annotator_work() {
  $node = menu_get_object();
  global $user;

  if($node->active_phase == 2 && og_is_member('node', $node->nid) && (!count($node->field_group_tec) || $node->field_group_tec[LANGUAGE_NONE][0]['value'] == 'annotation')) {

    // if user is group admin, he can create text to annotate
    $is_admin = sgmap_group_is_user_admin($user->uid,$node);
    $create_btn = array();
    if($is_admin){
      $create_btn = array(
        '#theme' => 'link',
        '#text'  => 'Ajouter un texte à annoter',
        '#path'  => 'node/add/sgmap-group-annotation',
        '#options' => array(
          'query' => array('destination' => request_uri()),
          'attributes' => array('class' => array('btn')),
            'html' => FALSE,
        ),
      );
    }

    // get the text to annotate
    $url_query = drupal_get_query_parameters();
    $atelier = $node->nid;
    $query = db_select('og_membership', 'm')
          ->fields('m', array('etid'))
          ->condition('m.gid', $node->nid);

    $query->innerJoin('node', 'n', 'm.etid = n.nid');
    $query->condition('n.type', 'sgmap_group_annotation');
    if ($url_query['nid']) {
      $query->condition('n.nid', $url_query['nid']);
    }
    $result = $query->execute()->fetchField();
    $textAnnotation = node_load($result);
    $text = array();
    if($result) {
      $text = array(
        '#theme' => 'sgmap_group_annotator_text',
        '#atelier' => $atelier,
        '#body' => $textAnnotation->body[LANGUAGE_NONE][0]['value']
      );
    }
    $path = drupal_get_path('module', 'sgmap_group_annotator');
    drupal_add_css($path . '/css/test.css');
    $build = array();

    return array(
      'content' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('container'),
        ),
        'create_btn' => $create_btn,
        'filter_links' => sgmap_group_annotator_get_texts($node->nid, $url_query['nid']),
        'text' => $text
      )
    );
  }  
}

/**
 * helper : get texts to annotate in the group
 */
function sgmap_group_annotator_get_texts($gid, $active_nid = NULL) {
  $query = db_select('og_membership', 'm')
    ->condition('m.gid', $gid);
  $query->innerJoin('node', 'n', 'm.etid = n.nid');
  $query->condition('n.type', 'sgmap_group_annotation');
  $query->fields('n', array('nid', 'title'));
  $result = $query->execute();
  $i == 0;
  foreach ($result as $value) {
    $i++;
    $is_active = 'not-active';
    if ($active_nid == $value->nid) {
      $is_active = 'active';
    } 
    else if ($i == 1 && $active_nid == NULL) {
      $is_active = 'active';
    }
    $link = array(
      '#theme' => 'link',
      '#text'  => $value->title,
      '#path'  => current_path(),
      '#options' => array(
        'query' => array(
          'step' => 'work',
          'nid'  => $value->nid
        ),
        'fragment' => 'block-sgmap-group-sgmap-group-animateur-text',
        'attributes' => array(
          'class' => array(
            'filter-link',
            $is_active
            )
          ),
          'html' => FALSE,
      ),
    );
    $items[] = render($link); 
  }
  return array(
    '#theme' => 'item_list',
    '#items' => $items,
    '#attributes' => array(
      'class' => array('annotator-text-filter')
    ),
  );
}

/**
 * Implements hook_theme().
 */
function sgmap_group_annotator_theme($existing, $type, $theme, $path) {
  return array(
    'sgmap_group_annotator_text' => array(
      'template'  => 'sgmap-group-annotator-text',
      'variables' => array('atelier' => NULL, 'body' => NULL),
      'path'      => $path . '/themes',
    ),
  );
}