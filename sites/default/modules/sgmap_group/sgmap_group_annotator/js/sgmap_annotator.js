jQuery(function ($) {
  // Ajouter test sur la présence du texte
  var chaine = '#annotemoi_' + Drupal.settings.atelier;
  var admin = Drupal.settings.admins;
  var userid = Drupal.settings.userAnnotatorID;
  var atelierid = Drupal.settings.atelier;
  var username = Drupal.settings.userName;
  var textNID = Drupal.settings.textNID;
  var blocked = Drupal.settings.blocked;
  admin.push(userid);
  var annotator = $(chaine).annotator().data('annotator');
  $(chaine).annotator('addPlugin', 'Store', {
    prefix: '/api/store',
    urls: {
      create:  '/annotations?atelier=' + atelierid + '&nid= ' + textNID,
      read:    '/annotations?atelier=' + atelierid + '&nid= ' + textNID,
      update:  '/annotations?id=:id',
      destroy: '/annotations?id=:id',
      search:  '/search'
    }
  });
  annotator.addPlugin('Unsupported', {
    message: "Votre navigateur ne supporte pas le module pour annoter le texte."
  });
  annotator.addPlugin('Permissions', {
    user: {
      id: userid,
      name: username
    },
    permissions: {
      'read': [],
      'admin': [userid],
      'update': [userid],
      'delete': admin
    },
    userId: function (user) {
      if (user && user.id) { 
        return user.id;
      }
      return user;
    },
    userString: function (user) {
      if (user && user.name) {
        return user.name;
      }
      return user;
    },
    userAuthorize: function(action, annotation, user) {
      var token, tokens, _i, _len;
      if (annotation.permissions) {
        tokens = annotation.permissions[action] || [];
        if (tokens.length === 0) {
          return true;
        }
        for (_i = 0, _len = tokens.length; _i < _len; _i++) {
          token = tokens[_i];
          if (this.userId(user) === token) {
            return true;
          }
        }
        return false;
      } else if (annotation.user) {
        if (user) {
          return this.userId(user) === this.userId(annotation.user);
        } else {
          return false;
        }
      }
      return true;
    },
    showViewPermissionsCheckbox : false,
    showEditPermissionsCheckbox : false
  });
  
  // hide anotator if atelier is blocked
  if (blocked == 1) {
    $('.annotator-adder').addClass('hidden');
  }
});