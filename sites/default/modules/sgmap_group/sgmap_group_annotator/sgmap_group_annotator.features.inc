<?php
/**
 * @file
 * sgmap_group_annotator.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_group_annotator_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sgmap_group_annotator_node_info() {
  $items = array(
    'sgmap_group_annotation' => array(
      'name' => t('annotation de text'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
