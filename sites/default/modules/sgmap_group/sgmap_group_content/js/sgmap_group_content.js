


function sgmap_content_group_redirect(url, step) {
  var options = document.getElementById('sgmap-group-content-filter-select');
  var index = options.selectedIndex;
  var assocUrl = new Array();

  assocUrl['les_plus_recentes'] = '?step=' + step + '&filter=recents';
  assocUrl['les_plus_soutenues'] = '?step=' + step + '&filter=soutenues';
  assocUrl['les_plus_commentes'] = '?step=' + step + '&filter=commentes';

  window.location.replace(url + assocUrl[options[index].value]);
}