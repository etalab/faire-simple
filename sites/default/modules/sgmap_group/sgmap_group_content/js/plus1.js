(function ($) {
  Drupal.behaviors.sgmapPlus1 = {
    attach: function(context){
      $( ".plus1-link" ).on("click",function(){
        var score = Drupal.settings.plus1_score;
        if(Drupal.settings.plus1_voted == 1) {
          if($('.plus1-widget').hasClass('plus1-undo-vote')){
            var newScore = parseInt(score) - 1;            
          } 
          else {
            var newScore = parseInt(score);
          }
        } 
        else {
          if($('.plus1-widget').hasClass('plus1-vote')){
            var newScore = parseInt(score) + 1;
          } 
          else {
            var newScore = parseInt(score);
          }
        }
        $('.plus1_count').html(newScore); 
        
      });

    }
  };
})(jQuery)