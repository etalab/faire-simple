Drupal.ajax.prototype.commands.sgmap_group_content_publisher_submit = function(ajax, response, status) {
  jQuery('#content-list').prepend( '<li class="list-item isotope-item">'+response.content+'</li>' ).isotope('reloadItems').isotope({ sortBy: 'original-order' });

}

Drupal.ajax.prototype.commands.sgmap_group_content_comment_submit = function(ajax, response, status) {
  jQuery('#modal-comments').append(response.content);
  jQuery('#comment-count').html(response.comment_count);
}