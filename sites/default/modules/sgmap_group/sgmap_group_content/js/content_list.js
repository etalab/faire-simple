(function ($) {

  $(document).ready(function(){
    /* infinite scroll */

    $('.infinite-scroll').each(function(index, element) {

      var self = $(element);
      var selfSelector = '#' + self.attr('id');

      var $container = $('#content-list');

      if (typeof $container.isotope == 'function') {
        $container.isotope({
          itemSelector : '.list-item',
        });
      }

      var loadMoreButton = $('<div class="load-more-btn-container" id="load-more-btn-' + self.attr('id') +'"><a class="btn btn-load-more" href="#">'+ self.data('btn-text') +'</a></div>');

      var opts = {
        'nextSelector': 'div.pagination ul li.last a',
        'navSelector': 'div.pagination',
        'itemSelector': selfSelector + ' > ' + self.data('item-selector'),
        'maxPage': self.data('max-page'),
        'loading' : {
          'msgText': "<em>Chargement</em>",
          'finishedMsg': 'Tout le contenu a été chargé'
        },
        'errorCallback': function (message) {
          if(message === 'done') {
            loadMoreButton.hide();
          }
        },
        'state': {
          'currPage': 0
        },
        "pathParse" : function(path, page) {
            var elements = decodeURIComponent(path).match(/^(.*?page=)(.*|$)/).slice(1);
            var parameters = elements[1];
            var newPath = [elements[0], parameters.substr(1)];
            return newPath;
        },
        "infid": self.attr('id')

      }

      if(!$(opts.navSelector).length) {
        return;
      }

      $(opts.navSelector).hide();

      // add a load more button
      self.append(loadMoreButton);

      var infscrollContainer = self.children('ul');

      // init infinite scroll
      infscrollContainer.infinitescroll(opts, 
        // call Isotope as a callback
        function( newElements ) {
          if (typeof $container.isotope == 'function') {
            $container.isotope( 'appended', $( newElements ) );
          }
          Drupal.attachBehaviors($(newElements));
        }
      );
      $(window).unbind('.infscr');

      loadMoreButton.click(function(event){
        infscrollContainer.infinitescroll('retrieve');
        event.preventDefault();
      });

    });

  });
})(jQuery);
