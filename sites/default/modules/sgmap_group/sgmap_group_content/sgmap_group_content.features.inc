<?php
/**
 * @file
 * sgmap_group_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_group_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_group_content_image_default_styles() {
  $styles = array();

  // Exported image style: group_content_list.
  $styles['group_content_list'] = array(
    'label' => 'group_content_list',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 255,
          'height' => 165,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sgmap_group_content_node_info() {
  $items = array(
    'sgmap_group_doc' => array(
      'name' => t('Document (Fabrique de solutions)'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'sgmap_group_link' => array(
      'name' => t('Link (Fabrique de solutions)'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'sgmap_group_picture' => array(
      'name' => t('Photo (Fabrique de solutions)'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'sgmap_group_post' => array(
      'name' => t('Post (Fabrique de solutions)'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'sgmap_group_video' => array(
      'name' => t('Vidéo (Fabrique de solutions)'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
