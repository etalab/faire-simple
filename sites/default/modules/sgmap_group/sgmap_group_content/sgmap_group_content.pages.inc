<?php

/**
 * Menu callback
 * members comments and contents count
 */
function sgmap_group_content_members_stat($gid) {
  $query_parameters = drupal_get_query_parameters();

  $header = array(
    array('data' => 'Nom', 'field' => 'name'),
    array('data' => 'Email', 'field' => 'mail'),
    array('data' => 'Phase 1 - contributions', 'field' => 'p1_contributions'),
    array('data' => 'Phase 1 - commentaires', 'field' => 'p1_comments'),
    array('data' => 'Phase 2 - contributions', 'field' => 'p2_contributions'),
    array('data' => 'Phase 2 - commentaires', 'field' => 'p2_comments'),
  );

  $query = db_select('sgmap_group_user_participation_data', 'p')
    ->condition('p.gid', $gid);
  $query->innerJoin('users', 'u', 'u.uid = p.uid');
  $query->fields('u', array('uid', 'name', 'mail'));
  $query->fields('p', array('p1_contributions', 'p1_comments', 'p2_contributions', 'p2_comments'));
  
  $results = $query
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(50)
    ->execute()
  ;
  $rows = array();

  foreach ($results as $value) {
    $rows[] = array(
      'data' => array(
        $value->name,
        $value->mail,
        $value->p1_contributions,
        $value->p1_comments,
        $value->p2_contributions,
        $value->p2_comments,
      )
    );
  }

  return array(
    'update' => array(
      '#markup' => render(drupal_get_form('sgmap_group_content_participation_count_form', $gid)) 
    ),
    'table' => array(
      '#theme'   => 'table',
      '#header'  => $header,
      '#rows'    => $rows,
      '#sticky'  => TRUE,
      '#empty'   => 'Aucune donnée. Appuyez sur le bouton "Mettre à jour" pour mettre à jour les données des utilisateurs',
    ),
    'pager' => array(
      '#theme' => 'pager',
      '#tags'  => array(),
    ),
  );
}

/**
 * form to update user participation data
 */
function sgmap_group_content_participation_count_form($form, &$form_state, $gid) {
  $form['container'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Appuyez sur le bouton ci-dessous pour mettre à jour les données des utilisateurs'),
  );
  $form['gid'] = array(
    '#type' => 'hidden',
    '#value' => $gid
  );
  $form['container']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Mettre à jour'),
  );
  $form['#redirect'] = FALSE;
  return $form;
}

/**
 * submit handler for sgmap_group_content_participation_count_form
 */
function sgmap_group_content_participation_count_form_submit($form, &$form_state) {
  $gid = $form_state['values']['gid'];
  $path = drupal_get_path('module', 'sgmap_group_content');
  $batch = array(
    'title' => t('Updating'),
    'operations' => array(
      array('sgmap_group_content_update_participation_data', array($gid)),
    ),
    'file' => $path . '/sgmap_group_content.pages.inc',
  );
  batch_set($batch);
}

/**
 * batch process update user data
 */
function sgmap_group_content_update_participation_data($gid, &$context) {
  $query = db_select('og_membership', 'm')
    ->condition('m.entity_type', 'user')
    ->condition('m.gid', $gid);
  $query->innerJoin('users', 'u', 'u.uid = etid');
  $query->fields('u', array('uid', 'name', 'mail'));
  
  $results = $query->execute();

  foreach ($results as $value) {
    db_merge('sgmap_group_user_participation_data')
      ->key(array('uid' => $value->uid, 'gid' => $gid))
      ->fields(array(
          'uid' => $value->uid,
          'gid' => $gid,
          'p1_contributions' => sgmap_group_content_stat_count_p1($value->uid, $gid, 'content'),
          'p1_comments'      => sgmap_group_content_stat_count_p1($value->uid, $gid, 'comment'),
          'p2_contributions' => sgmap_group_content_stat_count_p2($value->uid, $gid, 'content'),
          'p2_comments'      => sgmap_group_content_stat_count_p2($value->uid, $gid, 'comment')
      ))
      ->execute();

  }
}

/**
 * helper count user participation count P1
 * integer $uid: user id
 * integer $gid: group id
 * string $type: comment or content
 */
function sgmap_group_content_stat_count_p1($uid, $gid, $type) {
  $types = array_keys(sgmap_group_content_get_types());

  switch ($type) {
    case 'content':
      $query = db_select('node', 'n')
        ->condition('n.uid', $uid)
        ->condition('n.type', $types, 'IN');
      $query->innerJoin('og_membership', 'm', 'm.etid = n.nid');
      $query->condition('m.gid', $gid);
      return $query->countQuery()->execute()->fetchField();
      break;
    case 'comment':
      $query = db_select('comment', 'c')
        ->condition('c.uid', $uid);
      $query->innerJoin('node', 'n', 'n.nid = c.nid');
      $query->condition('n.type', $types, 'IN');
      $query->innerJoin('og_membership', 'm', 'm.etid = n.nid');
      $query->condition('m.gid', $gid);
      return $query->countQuery()->execute()->fetchField();
      break;    
  }

}

/**
 * helper count user participation count P2
 * integer $uid: user id
 * integer $gid: group id
 * string $type: comment or content
 */
function sgmap_group_content_stat_count_p2($uid, $gid, $type) {
  $types = array_keys(sgmap_group_content_get_types());

  switch ($type) {
    case 'content':
      $query = db_select('node', 'n')
        ->condition('n.uid', $uid)
        ->condition('n.type', 'sgmap_group_proposition');
      $query->innerJoin('og_membership', 'm', 'm.etid = n.nid');
      $query->condition('m.gid', $gid);
      return $query->countQuery()->execute()->fetchField();
      break;
    case 'comment':
      // annotation counter
      $annotator_uid = '"id":"' . $uid . '"';
      $annotor_img_uid = '"uid":"' . $uid . '"';

      $query = db_select('sgmap_annotator_annotations', 'a')
        ->condition('a.id_atelier', $gid);

      $or = db_or();
      $or->condition('a.annotation', '%' . $annotator_uid . '%', 'LIKE');
      $or->condition('a.annotation', '%' . $annotor_img_uid . '%', 'LIKE');

      $query->condition($or);

      $annotation_count = $query->countQuery()->execute()->fetchField();

      // SWOT counter
      $query = db_select('sgmap_group_swot_radar', 's')
        ->condition('s.uid', $uid);
      $query->innerJoin('node', 'n', 'n.nid = s.nid');
      $query->condition('n.type', 'sgmap_group_proposition');
      $query->innerJoin('og_membership', 'm', 'm.etid = n.nid');
      $query->condition('m.gid', $gid);
      $swot_count = $query->countQuery()->execute()->fetchField();
      
      return $annotation_count + $swot_count;
      break;    
  }

}