<?php

/**
 * menu callback
 * export content data form
 */
function sgmap_group_content_export_form($form, &$form_state, $group_type, $gid) {
  drupal_set_title('Exporter les données de la phase de documentation');
  og_set_breadcrumb($group_type, $gid, array(l(t('Group'), "$group_type/$gid/group")));

  $form['gid'] = array(
    '#type' => 'hidden',
    '#value' => $gid
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Exporter'
  );
  return $form;
}

/**
 * submit handler for sgmap_group_content_export_form
 */
function sgmap_group_content_export_form_submit($form, &$form_state) {
  global $user;
  $gid = $form_state['values']['gid'];
  $cols = array(
    'Identifiant',
    'Type',
    'Titre',
    'Description',
    'URL',
    'Auteur',
    'Date de publication',
    'nombre de soutiens',
    'nombre de commentaires'
  );

  array_walk($cols, 'sgmap_export_format_cel');

  $export_file = sgmap_export_get_file_object('suggestions', $user, $cols);
  $batch = array(
    'operations' => array(
      array('sgmap_group_content_export_process', array($gid, $export_file)),
    ),
    'finished'         => 'sgmap_group_content_export_finished',
    'title'            => t('Processing export batch'),
    'init_message'     => t('Export batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Export batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'sgmap_group_content') . '/sgmap_group_content.export.inc',
  );

  batch_set($batch);
  batch_process('group/node/'.$gid.'/admin/content/export');
}

function sgmap_group_content_export_process($gid, $export_file, &$context) {
  $content_types = sgmap_group_content_get_types();
  $types = array_keys($content_types);
  $query = db_select('node', 'n')
    ->condition('n.type', $types, 'IN')
    ->fields ('n', array (
      'nid',
      'title',
      'type',
      'created'
    ));
  $query->leftJoin('field_data_body', 'b', 'b.entity_id = n.nid');
  $query->fields('b', array('body_value'));
  $query->innerJoin('og_membership', 'o', 'o.etid = n.nid');
  $query->condition('o.gid', $gid);
  $query->condition('o.entity_type', 'node');
  $query->innerJoin('users', 'u', 'u.uid = n.uid');
  $query->fields('u', array('name'));
  // comment count
  $query->leftJoin('node_comment_statistics', 'c', 'c.nid = n.nid');
  $query->fields('c', array('comment_count'));
  // voting count
  $query->leftJoin('votingapi_cache', 'vc', 'vc.entity_id = n.nid AND vc.function = :function AND vc.tag = :tag', array(':function' => 'count', ':tag' => 'plus1_node_vote'));
  $query->addField('vc', 'value', 'vote_count');

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results'] = array('file' => $export_file);
  }

  // pagination
  $query->condition('n.nid', $context['sandbox']['current_node'], '>');
  $query->orderBy('n.nid');

  $query->range(0, 100);

  $fp = fopen(drupal_realpath($export_file->uri), 'a');

  $results = $query->execute();

  foreach ($results as $value) {
    $record['Identifiant'] = $value->nid;
    $record['Type'] =  $content_types[$value->type]['name'];
    $record['Titre'] = $value->title;
    $record['Description'] = strip_tags($value->body_value);
    $record['URL'] = url('node/' . $value->nid, array('absolute' => TRUE));
    $record['Auteur'] = $value->name;
    $record['Date de publication'] = format_date($value->created, 'custom', 'd/m/Y');
    $record['nombre de soutiens'] = $value->vote_count ? $value->vote_count : 0;
    $record['nombre de commentaires'] = $value->comment_count;
    $row = sgmap_group_content_prepare_row($record);
    fputcsv($fp, $row);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $value->nid;

  }

  fclose($fp);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

function sgmap_group_content_prepare_row($record) {
  $row = array(
    $record['Identifiant'],
    $record['Type'],
    $record['Titre'],
    $record['Description'],
    $record['URL'],
    $record['Auteur'],
    $record['Date de publication'],
    $record['nombre de soutiens'],
    $record['nombre de commentaires'],
  );
  array_walk($row, 'sgmap_export_format_cel');

  return $row;
}

function sgmap_group_content_export_finished($success, $results, $operations) {
  if ($success) {
    global $user;
    $file = $results['file'];
    $file = file_save($file);

    sgmap_export_send_export($file);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}