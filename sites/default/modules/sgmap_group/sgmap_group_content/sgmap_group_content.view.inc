<?php

/**
 * Implements hook_node_view().
 */
function sgmap_group_content_node_view($node, $view_mode, $langcode) {

  if(array_key_exists($node->type, sgmap_group_content_get_types())) {
    
    $groupe_content_types = sgmap_group_content_get_types();
    $gid = $node->og_group_ref[LANGUAGE_NONE][0]['target_id'];
    $group = node_load($gid);

    if ($view_mode == 'teaser') {
      if (sgmap_group_is_user_admin($node->uid, $group)) {
        $node->is_admin_content = TRUE;
      }
      
      $node->content['node-tile-link'] = array(
        '#theme'  => 'link',
        '#text' => $node->title,
        '#path' => 'modal/node/' . $node->nid,
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(
            'class' => array('use-ajax'),
            'id' => 'link-node-'.$node->nid,
          ),
        ),
      );
    }

    if ($view_mode == 'full') {

      // if atelier is blocked
      if (sgmap_group_is_blocked($group, 1)) {
        $node->content['blocked'] = TRUE;
      }
      //thematique
      $content_them_id = $node->field_group_content_them_id[LANGUAGE_NONE][0]['value'];
      if ($content_them_id && $content_them_id != 1000) {
        $content_thematique = _sgmap_group_get_thematiques($gid, $content_them_id);
        $node->content['thematique'] = array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('thematique content-thematique'),
          ),
          'thematique' => array(
            '#markup' => '<span>'.$content_thematique[$content_them_id].'</span>',
          ),
          '#weight' => 0,
        );
      }
      if (arg(0) == 'modal') {
        global $user;
        $voted = plus1_get_votes('node', $node->nid, $user->uid, 'plus1_node_vote');
        $settings['plus1_voted'] = $voted;
        $settings['plus1_score'] = $node->content['plus1_widget']['#score'];   
        drupal_add_js($settings, 'setting');
        $path = drupal_get_path('module', 'sgmap_group_content');
        drupal_add_js($path . '/js/plus1.js');

        $node->modal_title = theme('sgmap_group_content_modal_title', array(
          'icon' => $groupe_content_types[$node->type]['icon'],
          'name' => $groupe_content_types[$node->type]['name'],
          'score' => $node->content['plus1_widget']['#score'],
          )
        );

        //comments on modal
        $node->content['modal_comment_form'] = array(
          '#markup' => render(drupal_get_form('sgmap_group_content_modal_comment_form', $node->nid)),
        );
        if (node_access('delete', $node)) {
          $node->content['edit_link'] = array(
            '#theme' => 'link',
            '#text' => 'Modifier',
            '#path' => 'node/' . $node->nid . '/edit',
            '#options' => array(
              'attributes' => array('class' => array('node-edit')),
                //REQUIRED:
                'html' => FALSE,
             ),
          );
        }
        if ($cids = comment_get_thread($node, 1, 100)) {
          $comments = comment_load_multiple($cids);
          comment_prepare_thread($comments);
          $build = comment_view_multiple($comments, $node);
          $node->content['modal_comment_list'] = array(
            '#markup' => render($build),
          );
        }
      }
      else {
        $breadcrumb = array(
          l(t('Home'), '<front>'),
          l('La fabrique de solutions', 'fabrique-de-solutions'),
          l($group->title, 'node/' . $group->nid, array('query' => array('step' => 'doc')))
        );
        drupal_set_breadcrumb($breadcrumb);
      }
    }

    if ($node->type == 'sgmap_group_picture' && $view_mode == 'teaser') {
      $vars = array(
        'style_name' => 'group_content_list',
        'path' => $node->field_group_content_picture[LANGUAGE_NONE][0]['uri']
      );
      $picture = theme('image_style', $vars);
      $node->content['picture-thumbnail'] = array(
        '#theme'  => 'link',
        '#text' => $picture,
        '#path' => 'modal/node/' . $node->nid,
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(
            'class' => array('use-ajax', 'post-img'),
          ),
        ),
      );
    }

    if ($node->type == 'sgmap_group_video') {
      $url = $node->field_group_video_url[LANGUAGE_NONE][0]['value'];
      $video_data = sgmap_group_content_get_data_by_host($url);

      if ($view_mode == 'full') {
        $node->content['view-video'] = array(
          '#markup' => $video_data['video'],
        );
      }
      if ($view_mode == 'teaser') {
        $thumbnail = array(
          '#theme' => 'image',
          '#path' => $video_data['thumbnail'],
          '#width' => '265',
          '#height' => '165'
        );
        $node->content['video-thumbnail'] = array(
          '#theme'  => 'link',
          '#text' => render($thumbnail),
          '#path' => 'modal/node/' . $node->nid,
          '#options' => array(
            'html' => TRUE,
            'attributes' => array(
              'class' => array('use-ajax', 'post-img'),
            ),
          ),
        );
      }  
    }
  }
  
}

/*
 * Implements hook_preprocess_node()
 */
function sgmap_group_content_preprocess_node(&$vars) {
  $node = $vars['node'];
  if (array_key_exists($node->type, sgmap_group_content_get_types())) {
    $vars['theme_hook_suggestions'][] = 'node__sgmap_group_content__'.$vars['view_mode'];
    $groupe_content_types = sgmap_group_content_get_types();
    $vars['type_name'] = $groupe_content_types[$node->type]['name'];
    $vars['type_icon'] = $groupe_content_types[$node->type]['icon'];
    if ($node->is_admin_content) {
      $vars['classes_array'][] = 'admin_content';
    }
    if ($node->type == 'sgmap_group_link') {
      $vars['content']['field_group_link_url'][0]['#element']['title'] = $vars['content']['field_group_link_url'][0]['#element']['url'];
    }
  }
}

/**
 * Helper function
 */
function sgmap_group_content_get_data_by_host($url) {
  extract(parse_url($url));
  parse_str($query, $q);
  switch ($host) {
    case 'www.youtube.com':
      if ($q['v']) { // video id
        return sgmap_group_content_get_youtube_data($q['v']);          
      }
      break;
    case 'www.dailymotion.com':
      return sgmap_group_content_get_daylimotion_data($url);
      break;
    case 'www.vimeo.com':
    case 'vimeo.com':
      return sgmap_group_content_get_vimeo_data($url);
      break;

  }
}

/**
 * helper: get youtube video data
 * @param  strig $url : video url
 * @return array (thumbnail, video_iframe)
 */
function sgmap_group_content_get_youtube_data($vid) {
  // $url = 'http://gdata.youtube.com/feeds/api/videos/'.$vid.'?alt=json';
  // $res = drupal_http_request($url);
  // $youtube_data = drupal_json_decode($res->data);
  return array(
    'thumbnail' => 'http://img.youtube.com/vi/' . $vid . '/0.jpg',
    'video' => '<iframe width="640" height="360" src="//www.youtube.com/embed/' . $vid . '" frameborder="0" allowfullscreen></iframe>'
  );
}

/**
 * helper: get daylimotion video data
 * @param  strig $url : video url
 * @return array (thumbnail, video_iframe)
 */
function sgmap_group_content_get_daylimotion_data($url) {
  $vid = strtok(basename($url), '_');
  return array(
    'thumbnail' => 'http://www.dailymotion.com/thumbnail/video/' . $vid,
    'video' => '<iframe width="640" height="360" src="http://www.dailymotion.com/embed/video/' . $vid . '"></iframe>'
  );
}


/**
 * helper: get vimeo video data
 * @param  strig $url : video url
 * @return array (thumbnail, video_iframe)
 */
function sgmap_group_content_get_vimeo_data($url) {
  sscanf(parse_url($url, PHP_URL_PATH), '/%d', $vid);
  $api_url  = 'http://vimeo.com/api/v2/video/'. $vid .'.json';
  $res = drupal_http_request($api_url);
  $data = drupal_json_decode($res->data);
  return array(
    'thumbnail' => $data[0]['thumbnail_medium'],
    'video' => '<iframe src="//player.vimeo.com/video/'. $vid .'" width="640" height="360" frameborder="0"></iframe>'
  );
}

/**
 * group content modal comment form
 */
function sgmap_group_content_modal_comment_form($form, &$form_state, $nid) {
  $form['nid'] = array(
    '#value' => $nid,
    '#type'  => 'hidden',
  );
  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => 'Commentaire',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#value' => 'Poster',
    '#type'  => 'submit',
    '#ajax' => array(
      'callback' => 'sgmap_group_content_ajax_comment_submit',
      'wrapper' => 'modal-comment-form',
      'method' => 'append'
    ),
  );

  return $form;
}

/**
 * ajax submit handler for sgmap_group_content_modal_comment_form
 */
function sgmap_group_content_ajax_comment_submit($form, $form_state) {
  global $user;
  $commands[] = ajax_command_prepend(NULL, theme('status_messages'));
  if ($form_state['values']['comment']) {
    $commands[] = ajax_command_invoke('.form-textarea', 'val', array(''));
    $commands[] = ajax_command_invoke('.alert', 'remove');

    $comment = new stdClass();
    $comment->nid = $form_state['values']['nid'];
    $comment->cid = 0;
    $comment->pid = 0;
    $comment->uid = $user->uid;
    $comment->mail = $user->mail;
    $comment->is_anonymous = 0;
    $comment->homepage = '';
    $comment->status = COMMENT_PUBLISHED; 
    $comment->language = LANGUAGE_NONE;
    $comment->subject = '';
    $comment->comment_body[$comment->language][0]['value'] = $form_state['values']['comment'];
    $comment->comment_body[$comment->language][0]['format'] = 'filtered_html';

    comment_submit($comment);
    comment_save($comment);
    $node = node_load($form_state['values']['nid']);
    $commands[] = array(
      'command' => 'sgmap_group_content_comment_submit', 
      'content' => render(comment_view($comment, $node)),
      'comment_count' => $node->comment_count + 1,
    );
  }
  return array(
    '#type' => 'ajax',
    '#commands' => $commands
  );
}

/**
 * Implements hook_theme().
 */
function sgmap_group_content_theme($existing, $type, $theme, $path) {
  return array(
    'sgmap_group_content_modal_title' => array(
      'template'  => 'sgmap-group-content-modal-title',
      'variables' => array('icon' => NULL ,'name' => NULL, 'score' => NULL),
      'path'      => $path .'/themes',
    ),
  );
}

/**
 * Implements hook_sgmap_base_plus1_alter
 */
function sgmap_group_content_sgmap_base_plus1_alter(&$base_class, $entity_id) {
  $node = node_load($entity_id);
  if (array_key_exists($node->type, sgmap_group_content_get_types())) {
    $base_class[] = 'btn';
    $base_class[] = 'btn-large';
    $base_class[] = 'btn-orange';
  }
}

/**
 * Implements hook_comment_view_alter().
 */
function sgmap_group_content_comment_view_alter(&$build) {
  global $user;
  $node = $build['#node'];
  $comment = $build['#comment'];
  if (og_user_access_entity('delete any '.$node->type.' content', 'node', $node) && !user_access('administer comments') && $user->uid != $comment->uid) {
    $links['comment-delete'] = array(
      'title' => t('delete'),
      'href' => 'comment/'.$comment->cid.'/delete',
      'html' => TRUE,
    );
    $build['links']['comment']['#links'] = $links;

  }
  unset($build['links']['comment']['#links']['comment-reply']);
}

/**
 * Implements hook_comment_delete().
 */
function sgmap_group_content_comment_delete($comment) {
  cache_clear_all($comment->nid, 'cache_entity_node');
}

/**
 * Implements hook_menu_alter().
 */
function sgmap_group_content_menu_alter(&$items) {
  $items['comment/%/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'comment_confirm_delete_page',
    'page arguments' => array(1),
    'access callback' => 'sgmap_group_content_delete_comment_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'comment.admin.inc',
    'file path' => drupal_get_path('module', 'comment'),
    'weight' => 2,
  );
}

/**
 * access callback for group content delete comment
 */
function sgmap_group_content_delete_comment_access($cid) {
  if (user_access('administer comments')) {
    return TRUE;
  }
  $comment = comment_load($cid);
  $node = node_load($comment->nid);
  if (og_user_access_entity('delete any '.$node->type.' content', 'node', $node)) {
    return TRUE;
  }
}
