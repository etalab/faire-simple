<?php
/**
 * @file
 * sgmap_group.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_group_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sgmap_group';
  $context->description = '';
  $context->tag = 'SGMAP Group';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'sgmap_group' => 'sgmap_group',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_group-sgmap_group_animateur_text' => array(
          'module' => 'sgmap_group',
          'delta' => 'sgmap_group_animateur_text',
          'region' => 'out_container_bottom',
          'weight' => '-10',
        ),
        'sgmap_group_content-sgmap_group_content_publisher' => array(
          'module' => 'sgmap_group_content',
          'delta' => 'sgmap_group_content_publisher',
          'region' => 'out_container_bottom',
          'weight' => '-9',
        ),
        'sgmap_group_content-sgmap_group_content_list' => array(
          'module' => 'sgmap_group_content',
          'delta' => 'sgmap_group_content_list',
          'region' => 'out_container_bottom',
          'weight' => '-8',
        ),
        'sgmap_group_annotator-sgmap_group_annotator_work' => array(
          'module' => 'sgmap_group_annotator',
          'delta' => 'sgmap_group_annotator_work',
          'region' => 'out_container_bottom',
          'weight' => '-7',
        ),
        'sgmap_group_firepad-sgmap_group_firepad' => array(
          'module' => 'sgmap_group_firepad',
          'delta' => 'sgmap_group_firepad',
          'region' => 'out_container_bottom',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('SGMAP Group');
  $export['sgmap_group'] = $context;

  return $export;
}
