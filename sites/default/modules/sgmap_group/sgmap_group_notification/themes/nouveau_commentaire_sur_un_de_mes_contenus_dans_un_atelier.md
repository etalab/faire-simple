[Faire Simple - Atelier] Nouveau commentaire sur votre publication
Bonjour @user.firstname @user.lastname,

Un nouveau commentaire vient d'être publié sur votre publication [@node.title](@node.href) dans l'atelier [@group.title](@group.href)
