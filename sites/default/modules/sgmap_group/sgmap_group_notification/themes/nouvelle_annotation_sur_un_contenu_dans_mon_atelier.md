[Faire Simple - Fabrique de solution] Nouveau commentaire sur un contenu
Bonjour @group.recipient,

Un nouveau commentaire vient d'être publié [dans la phase de travail en commun](@group.content_url) dans l'atelier [@group.group_title](@group.group_url) auquel vous participez.