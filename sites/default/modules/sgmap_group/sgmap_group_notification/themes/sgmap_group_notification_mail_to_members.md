[Faire Simple - Fabrique de solution] L'animateur de votre atelier vous a envoyé un message
Bonjour @group.recipient,

@group.sender, l'animateur de votre atelier [@group.group_title](@group.group_url) vous a envoyé le message suivant:

@group.mail_subject

@group.mail_content

