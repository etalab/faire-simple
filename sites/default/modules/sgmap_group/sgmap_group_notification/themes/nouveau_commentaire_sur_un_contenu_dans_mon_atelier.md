[Faire Simple - Fabrique de solution] Nouveau commentaire sur une publication
Bonjour @group.recipient,

Un nouveau commentaire vient d'être publié sur la publication [@group.content_title](@group.content_url) dans l'atelier [@group.group_title](@group.group_url) auquel vous participez.
