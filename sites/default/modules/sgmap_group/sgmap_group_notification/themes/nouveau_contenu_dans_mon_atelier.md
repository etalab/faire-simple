[Faire Simple - Fabrique de solution] Nouvelle publication
Bonjour @group.recipient,

Une nouvelle publication " [@group.content_title](@group.content_url) " a été publiée dans l'atelier [@group.group_title](@group.group_url) auquel vous participez.
