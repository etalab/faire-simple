<?php

/**
 * Implements hook_menu().
 */
function sgmap_group_notification_menu() {
  $items['group/%/%/admin/people/mail'] = array(
    'title' => 'Envoyer un message aux membres',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sgmap_group_notification_mailer_form', 1, 2),
    'access callback' => 'og_ui_user_access_group',
    'access arguments' => array('manage members', 1, 2),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_og_membership_insert().
 */
function sgmap_group_og_membership_insert($og_membership) {
  if ($og_membership->entity_type == 'user') {
    $account = user_load($og_membership->etid);
    sgmap_notification_subscribe($account, 'nouveau_commentaire_sur_un_de_mes_contenus_dans_un_atelier');
    sgmap_notification_subscribe($account, 'nouveau_soutien_sur_un_de_mes_contenus_dans_un_atelier');
    sgmap_notification_subscribe($account, 'nouveau_commentaire_sur_un_contenu_dans_mon_atelier');
    sgmap_notification_subscribe($account, 'nouveau_contenu_dans_mon_atelier');
  }
}

/**
 * Implementation of og_ui_get_group_admin_alter()
 */
function sgmap_group_notification_og_ui_get_group_admin_alter(&$data, $gid) {
  global $user;
  $node = node_load($gid['etid']);
  if (sgmap_group_is_user_admin($user->uid, $node) && $node->type == 'sgmap_group') {
    $data['mail_to_members'] = array(
      'title' => t('Envoyer un message aux membres'),
      'description' => t('Envoyer un message aux membres de l\'atelier'),
      'href' => 'admin/people/mail',
    );
  }
}

/**
 * page callback: send mail to group members
 */
function sgmap_group_notification_mailer_form($form, &$form_state, $type, $nid) {
  $form['subject'] = array(
    '#type'     => 'textfield',
    '#title'    => 'sujet du message',
    '#required' => TRUE
  );

  $form['content'] = array(
    '#type'     => 'textarea',
    '#title'    => 'contenu du message',
    '#required' => TRUE
  );

  $form['gid'] = array(
    '#type'  => 'hidden',
    '#value' => $nid
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Envoyer'
  );

  return $form;
}

/**
 * submit handler for sgmap_group_notification_mailer_form
 */
function sgmap_group_notification_mailer_form_submit($form, &$form_state) {
  $subject = $form_state['values']['subject'];
  $content = $form_state['values']['content'];
  $gid   = $form_state['values']['gid'];

  $group = node_load($gid);
  $users = sgmap_group_get_members($gid);
  foreach ($users as $account) {
    $params = array(
      'group_title'  => $group->title,
      'group_url'    => url('node/' . $gid, array('absolute' => TRUE)),
      'sender'       => $GLOBALS['user']->name,
      'recipient'    => $account->name,
      'mail_subject' => $subject,
      'mail_content' => $content
    );
    drupal_mail('sgmap_group_notification', 'sgmap_group_notification_mail_to_members', $account->mail, LANGUAGE_NONE, $params);
  }
  drupal_set_message('Le message a été envoyé aux membres de l\'atelier');
}

/**
 * Implements hook_node_insert().
 */
function sgmap_group_notification_node_insert($node) {
  if (array_key_exists($node->type, sgmap_group_content_get_types()) || $node->type == 'sgmap_group_proposition') {
    // notificaiton for group members when content is created
    $group_nid = reset($node->og_group_ref[LANGUAGE_NONE]);
    sgmap_group_notification_queue_insert($node->uid, $node->nid, $group_nid['target_id'], 'nouveau_contenu_dans_mon_atelier');
  }
}

/**
 * Implements hook_comment_insert().
 */
function sgmap_group_notification_comment_insert($comment) {
  global $user;
  $node = node_load($comment->nid);

  if (array_key_exists($node->type, sgmap_group_content_get_types())) {
    if ($user->uid != $node->uid) {
      $event = new SgmapNotificationEvent();
      $event->setSubject($node);
      $event->setArguments(array(
        'comment' => $comment
      ));
      // notification for the user who create the content
      sgmap_notification_event_dispatch('group_content.comment.create', $event);
    }      
    // notificaiton for group members
    //sgmap_group_notification_queue_insert($comment->uid, $node->nid, $node->og_group_ref[LANGUAGE_NONE][0]['target_id'], 'nouveau_commentaire_sur_un_contenu_dans_mon_atelier');
  }
}

/**
 * Implements hook_votingapi_insert();
 */
function sgmap_group_notification_votingapi_insert($vote) {

  $vote = (object) $vote[0];

  if($vote->entity_type != 'node') {
    return;
  }

  $node = node_load($vote->entity_id);
  $user = user_load($vote->uid);

  if (array_key_exists($node->type, sgmap_group_content_get_types()) && $user->uid != $node->uid) {

    $event = new SgmapNotificationEvent();
    $event
      ->setSubject($node)
      ->setArguments(array(
        'user' => $user
      ))
    ;

    sgmap_notification_event_dispatch('group_content.vote.create', $event);
  }

}

/**
 * Implements hook_sgmap_notification_mail_mail_keys.
 */
function sgmap_group_notification_sgmap_notification_mail_mail_keys(){
  return array(
    'nouveau_commentaire_sur_un_de_mes_contenus_dans_un_atelier',
    'nouveau_soutien_sur_un_de_mes_contenus_dans_un_atelier',
  );
}

/**
 * Implements hook_sgmap_notification_notification_types_alter.
 */
function sgmap_group_notification_sgmap_notification_notification_types_alter(&$notification_types) {

  $notification_types['nouveau_commentaire_sur_un_de_mes_contenus_dans_un_atelier'] = array(
    'name'       => 'Nouveau commentaire sur une de mes publications dans la Fabrique de solution',
    'event_type' => 'group_content.comment.create',
  );

  $notification_types['nouveau_soutien_sur_un_de_mes_contenus_dans_un_atelier'] = array(
    'name'       => 'Nouveau soutien sur une de mes publications dans la Fabrique de solution',
    'event_type' => 'group_content.vote.create',
  );

  $notification_types['nouveau_contenu_dans_mon_atelier'] = array(
    'name'       => 'Nouvelle publication dans un atelier auquel je participe',
    'event_type' => 'group_content.content.create',
  );

  $notification_types['nouveau_commentaire_sur_un_contenu_dans_mon_atelier'] = array(
    'name'       => 'Nouveau commentaire dans un atelier auquel je participe',
    'event_type' => 'group_content.comment_on_content.create',
  );
}

/**
 * Implements hook_query_TAG_alter().
 * filter subscribtion query
 */
function sgmap_group_notification_query_sgmap_subscription_filter_alter(QueryAlterableInterface $query) {
  
  // retreive notification object
  $event                = $query->getMetaData('sgmap_notification_event');
  $notification_type_id = $query->getMetaData('sgmap_notification_type_id');

  if(in_array($notification_type_id, array(
    'nouveau_commentaire_sur_un_de_mes_contenus_dans_un_atelier',
    'nouveau_soutien_sur_un_de_mes_contenus_dans_un_atelier',
  ))) {
    $node = $event->getSubject();
    $query->condition('s.uid', $node->uid);
  }

}

/**
 * Implements hook_mail().
 */
function sgmap_group_notification_mail($key, &$message, $params) {
  
  // send a mail to group members
  if($key == 'sgmap_group_notification_mail_to_members') {
    $path = drupal_get_path('module', 'sgmap_group_notification') . '/themes';
  
    sgmap_mail_notification_prepare_message($message, $path, $key, array(
      '@group.group_title'  => $params['group_title'],
      '@group.group_url'    => $params['group_url'],
      '@group.mail_subject' => $params['mail_subject'],
      '@group.mail_content' => $params['mail_content'],
      '@group.recipient'    => $params['recipient'],
      '@group.sender'       => $params['sender'],
    ));
  }
  // notification for group members when there is an action(new content, new comment on content...) in group
  else if (in_array($key, array(
    'nouveau_commentaire_sur_un_contenu_dans_mon_atelier',
    'nouveau_contenu_dans_mon_atelier',
    'nouvelle_annotation_sur_un_contenu_dans_mon_atelier'
    ))) 
  {
    $path = drupal_get_path('module', 'sgmap_group_notification') . '/themes';
    $recipient = $params['user'];
    $message['#user'] = $recipient;
    sgmap_mail_notification_prepare_message($message, $path, $key, array(
      '@group.group_title'   => $params['group_title'],
      '@group.group_url'     => $params['group_url'],
      '@group.content_title' => $params['content_title'],
      '@group.content_url'   => $params['content_url'],
      '@group.recipient'     => $recipient->name,
    ),
    'unsubscribe');
  }
  else {
    $supported_notifications = sgmap_group_notification_sgmap_notification_mail_mail_keys();

    if(!in_array($key, array_keys($supported_notifications))) {
      // mail key not supported
      return;
    }

    $notification_types = array();

    sgmap_group_notification_sgmap_notification_notification_types_alter($notification_types);

    if(in_array($key, array_keys($notification_types))) {
      $signature = 'unsubscribe';
    } else {
      $signature = 'no_reply';
    }

    $message['#user'] = $params['user'];

    $event = $params['event'];
    $node = $event->getSubject();

    $message_parameters = array(
      '@node.href'  =>  url('node/'. $node->nid, array('absolute' => TRUE)),
      '@node.title' => $node->title,
    );

    if (in_array($key, array(
        'nouveau_commentaire_sur_un_de_mes_contenus_dans_un_atelier',
        'nouveau_soutien_sur_un_de_mes_contenus_dans_un_atelier'
      ))) 
    {
      $gid = $node->og_group_ref[LANGUAGE_NONE][0]['target_id'];
      $group = node_load($gid);
      $message_parameters['@group.title'] = $group->title;
      $message_parameters['@group.href'] = url('node/'. $group->nid, array('absolute' => TRUE));
      if ($key == 'nouveau_soutien_sur_un_de_mes_contenus_dans_un_atelier') {
        $user = $event->getArgument('user');
        $message_parameters['@user.name'] = $user->name;
      }
    }

    $template_file_messages_path = drupal_get_path('module', 'sgmap_group_notification') . '/themes';

    sgmap_mail_notification_prepare_message($message, $template_file_messages_path, $key, $message_parameters, $signature);
  }
  
}

/**
 * helper: save new notification queue
 */
function sgmap_group_notification_queue_insert($creator_uid, $nid, $gid, $type) {
  db_insert('sgmap_group_notif_queue') 
    ->fields(array(
      'creator_uid' => $creator_uid,
      'nid'         => $nid,
      'gid'         => $gid,
      'type'        => $type,
      'created'     => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * send notifications for group members whene there is an action in a group:
 * new post, new comment on post ....
 */
function sgmap_group_notification_queue_send() {

  // get queue
  $query = db_select('sgmap_group_notif_queue', 'q')
    ->fields('q', array('qid', 'creator_uid', 'nid', 'gid', 'type', 'last_uid', 'url'))
    ->orderBy('qid', 'ASC')
    ->range(0, 1);
  $queue = $query->execute()->fetchAssoc();

  if ($queue) {    
    // get group members and send notification
    $query = db_select('og_membership', 'm');
    $query->fields('m', array('etid'));
    // only for those who want notification
    $query->innerJoin('sgmap_notification_subscription', 's', 's.uid = m.etid');
    $query->condition('s.type', $queue['type'])
      ->condition('m.etid', $queue['creator_uid'], '<>')
      ->condition('m.entity_type', 'user')
      ->condition('m.gid', $queue['gid']);
    $result = $query->execute()->fetchAll();
    if ($result) {
      $group = node_load($queue['gid']);
      // content case
      if ($queue['nid']) {
        $content = node_load($queue['nid']);
        $content_title = $content->title;
        $content_url = url('node/' . $queue['nid'], array('absolute' => TRUE));
        $notif_type = $queue['type'];
      } 
      // annotation case
      else {
        $notif_type = 'nouvelle_annotation_sur_un_contenu_dans_mon_atelier';
        $content_url = $queue['url'];
        $content_title = '';
      }
      
      // send notification for group members
      foreach ($result as $key => $value) {
        $user = user_load($value->etid);
        $params = array(
          'group_title'   => $group->title,
          'group_url'     => url('node/' . $queue['gid'], array('absolute' => TRUE)),
          'content_title' => $content_title,
          'content_url'   => $content_url,
          'user'          => $user,
        );
        drupal_mail('sgmap_group_notification', $notif_type, $user->mail, LANGUAGE_NONE, $params);
      }
      // remove the queue
      db_delete('sgmap_group_notif_queue')
        ->condition('qid', $queue['qid'])
        ->execute();
    }
  }
}

/**
 * remove notification older than 1 week
 */
function sgmap_group_notification_remove_old_queue() {
  $a_week_ago = strtotime('-1 week');
  db_delete('sgmap_group_notif_queue')
    ->condition('created', $a_week_ago, '<')
    ->execute();
}

/**
 * Implements hook_cron().
 */
function sgmap_group_notification_cron() {
  sgmap_group_notification_remove_old_queue();
  sgmap_group_notification_queue_send();
}