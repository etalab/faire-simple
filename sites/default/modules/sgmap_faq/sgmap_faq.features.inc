<?php
/**
 * @file
 * sgmap_faq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_faq_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_faq_image_default_styles() {
  $styles = array();

  // Exported image style: faq_slider.
  $styles['faq_slider'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 500,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'faq_slider',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sgmap_faq_node_info() {
  $items = array(
    'question_faq' => array(
      'name' => t('Question FAQ'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'question_reponse' => array(
      'name' => t('Question Réponse'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
