<?php
/**
 * @file
 * sgmap_faq.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_faq_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'faq';
  $context->description = '';
  $context->tag = 'faq';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'question_reponse' => 'question_reponse',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_faq-create_faq_question' => array(
          'module' => 'sgmap_faq',
          'delta' => 'create_faq_question',
          'region' => 'out_container_bottom',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('faq');
  $export['faq'] = $context;

  return $export;
}
