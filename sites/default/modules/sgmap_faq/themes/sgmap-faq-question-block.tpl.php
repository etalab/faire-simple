<div id="faq-box">
	<div class="container">
	  <?php if(!$user_logged): ?>

	    <div class="unlogged-faq" id="unlogged-faq">
	      <p>Merci de vous connecter pour poser une question</p>
	      <a class="btn btn-large" href="<?php print url('user/register') ?>"><?php print t("Je m'inscris maintenant") ?></a>
	      <a class="btn btn-large" href="<?php print url('user/login') ?>"><?php print t("J'ai déjà un compte") ?></a>
	    </div>
	    <?php else: ?>
				<?php print $form ?>
      <?php endif; ?>
  </div>
</div>