<?php

/**
 * menu callback
 * France connect cofiguration setting forms
 */
function sgmap_fc_config_settings() {
  $form['fc_api_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL de l\'API France connect',
    '#default_value' => variable_get('fc_api_url', NULL)
  );
 return system_settings_form($form);
}
