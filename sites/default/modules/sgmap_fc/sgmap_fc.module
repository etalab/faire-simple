<?php

/**
 * Implements hook_menu().
 */
function sgmap_fc_menu() {

  $items['admin/config/sgmap/france_connect'] = array(
    'title' => 'France connect config',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sgmap_fc_config_settings'),
    'file' => 'sgmap_fc.admin.inc',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_user_logout().
 */
function sgmap_fc_user_logout($account) {
  $user = user_load($account->uid);
  if ($user->field_sgmap_fc[LANGUAGE_NONE][0]['value']) {
    $destination = &drupal_static(__FUNCTION__);
    $destination = variable_get('fc_api_url', NULL) . '/logout';
  }
}

/**
 * Implements hook_drupal_goto_alter().
 */
function sgmap_fc_drupal_goto_alter(&$path, &$options, &$http_response_code) {
  $destination = &drupal_static('sgmap_fc_user_logout');
  if (!$path && $destination) {
    drupal_goto($destination);
  }
}

/**
 * Implements hook_menu_alter().
 */
function sgmap_fc_menu_alter(&$items) {
  $items['openid-connect/%']['page callback'] = 'sgmap_fc_redirect_page';
}

/**
 * custom redirect page
 */
function sgmap_fc_redirect_page($client_name) {
  // Delete the state token, since it's already been confirmed.
  unset($_SESSION['openid_connect_state']);

  // Get parameters from the session, and then clean up.
  $parameters = array(
    'destination' => 'user',
    'op' => 'login',
    'connect_uid' => NULL,
  );
  $_SESSION['sgmap_fc_redirect'] = $_SESSION['openid_connect_destination'];
  foreach ($parameters as $key => $default) {
    if (isset($_SESSION['openid_connect_' . $key])) {
      $parameters[$key] = $_SESSION['openid_connect_' . $key];
      unset($_SESSION['openid_connect_' . $key]);
    }
  }
  $destination = $parameters['destination'];

  $client = openid_connect_get_client($client_name);
  if (!isset($_GET['error']) && (!$client || !isset($_GET['code']))) {
    // In case we don't have an error, but the client could not be loaded or
    // there is no state token specified, the URI is probably being visited
    // outside of the login flow.
    return MENU_NOT_FOUND;
  }

  $provider_param = array('@provider' => $client->getLabel());

  if (isset($_GET['error'])) {
    if ($_GET['error'] == 'access_denied') {
      // If we have an "access denied" error, that means the user hasn't granted
      // the authorization for the claims.
      drupal_set_message(t('Logging in with @provider has been canceled.', $provider_param), 'warning');
    }
    else {
      // Any other error should be logged. E.g. invalid scope.
      $variables = array(
        '@error' => $_GET['error'],
        '@details' => $_GET['error_description'],
      );
      watchdog('openid_connect_' . $client_name, 'Authorization failed: @error. Details: @details', $variables, WATCHDOG_ERROR);
    }
  }
  else {
    // Process the login or connect operations.
    $tokens = $client->retrieveTokens($_GET['code']);
    if ($tokens) {
      if ($parameters['op'] === 'login') {
        $success = sgmap_fc_complete_authorization($client, $tokens, $destination);
        if (!$success) {
          drupal_set_message(t('Logging in with @provider could not be completed due to an error.', $provider_param), 'error');
        }
      }
      elseif ($parameters['op'] === 'connect' && $parameters['connect_uid'] === $GLOBALS['user']->uid) {
        $success = openid_connect_connect_current_user($client, $tokens);
        if ($success) {
          drupal_set_message(t('Account successfully connected with @provider.', $provider_param));
        }
        else {
          drupal_set_message(t('Connecting with @provider could not be completed due to an error.', $provider_param), 'error');
        }
      }
    }
  }
}

/**
 * Complete the authorization after tokens have been retrieved.
 *
 * @param OpenIDConnectClientInterface $client
 *   The client.
 * @param array $tokens
 *   The tokens as returned from OpenIDConnectClientInterface::retrieveTokens().
 * @param string|array &$destination
 *   The path to redirect to after authorization.
 *
 * @return bool
 *   TRUE on success, FALSE on failure.
 */
function sgmap_fc_complete_authorization($client, $tokens, &$destination) {
  if (user_is_logged_in()) {
    throw new \RuntimeException('User already logged in');
  }

  $user_data = $client->decodeIdToken($tokens['id_token']);
  $userinfo = $client->retrieveUserInfo($tokens['access_token']);

  // if ($userinfo && empty($userinfo['email'])) {
  //   watchdog('openid_connect', 'No e-mail address provided by @provider', array('@provider' => $client->getLabel()), WATCHDOG_ERROR);

  //   return FALSE;
  // }

  $sub = openid_connect_extract_sub($user_data, $userinfo);
  if (empty($sub)) {
    watchdog('openid_connect', 'No "sub" found from @provider', array('@provider' => $client->getLabel()), WATCHDOG_ERROR);

    return FALSE;
  }

  $account = openid_connect_user_load_by_sub($sub, $client->getName());
  if ($account) {
    // An existing account was found. Save user claims.
    if (variable_get('openid_connect_always_save_userinfo', TRUE)) {
      openid_connect_save_userinfo($account, $userinfo);
    }
  }
  else {
    // Check whether there is an e-mail address conflict.
    if (user_load_by_mail($userinfo['email'])) {
      drupal_set_message(t('The e-mail address is already taken: @email', array('@email' => $userinfo['email'])), 'error');

      return FALSE;
    }

    // Create a new account.
    $account = sgmap_fc_create_user($sub, $userinfo, $client->getName());
    openid_connect_save_userinfo($account, $userinfo);
  }

  openid_connect_login_user($account, $destination);

  module_invoke_all('openid_connect_post_authorize', $tokens, $account, $userinfo, $client->getName());

  return TRUE;
}

/**
 * Creates a user indicating sub-id and login provider.
 *
 * @param string $sub
 *   The subject identifier.
 * @param array $userinfo
 *   The user claims, containing at least 'email'.
 * @param string $client_name
 *   The machine name of the client.
 *
 * @return object|FALSE
 *   The user object or FALSE on failure.
 */
function sgmap_fc_create_user($sub, $userinfo, $client_name) {
  $edit = array(
    'name' => sgmap_fc_generate_username($sub, $userinfo, $client_name),
    'pass' => user_password(),
    'status' => 1,
    'field_user_civilite' => array(
      LANGUAGE_NONE => array(
        array(
          'value' => $userinfo['gender'] == 'male' ? 'h' : 'f'
        )
      )
    ),
    'field_sgmap_fc' => array(
      LANGUAGE_NONE => array(
        array(
          'value' => 1
        )
      )
    ),
    'openid_connect_client' => $client_name,
    'openid_connect_sub' => $sub,
  );

  return user_save(NULL, $edit);
}

/**
 * Generate a username for a new account.
 *
 * @param array $userinfo
 *   The user claims.
 *
 * @return string
 *   A unique username.
 */
function sgmap_fc_generate_username($sub, $userinfo, $client_name) {
  $name = $userinfo['family_name'];

  // Ensure there are no duplicates.
  for ($original = $name, $i = 1; openid_connect_username_exists($name); $i++) {
    $name = $original . '_' . $i;
  }

  return $name;
}

/**
 * Implements hook_openid_connect_post_authorize
 */
function sgmap_fc_openid_connect_post_authorize($tokens, $account, $userinfo, $client_name) {
  $destination = $_SESSION['sgmap_fc_redirect'];
  // if user never saved his type
  if (count($account->field_sgmap_public) == 0) {
    drupal_set_message('Merci d’avoir utilisé France Connect. Afin de finaliser votre inscription et de participer au site, il vous faut remplir les informations obligatoires du formulaire ci-dessous .', 'warning');
    drupal_goto('user/' . $account->uid . '/edit', array('query' => array('destination' => $destination[0])));
  }
  // It's possible to set 'options' in the redirect destination.
  if (is_array($destination)) {
    drupal_goto($destination[0], $destination[1]);
  }
  else {
    drupal_goto($destination);
  }
}

/**
 * Implements hook_form_alter
 */
function sgmap_fc_form_user_profile_form_alter(&$form, &$form_state) {

  if($form['field_sgmap_fc'][LANGUAGE_NONE]['#default_value'] == 1) {
    $user_validate_current_pass_key = array_search('user_validate_current_pass', $form['#validate']);
    unset($form['#validate'][$user_validate_current_pass_key]);
    unset($form['account']['pass'], $form['account']['current_pass_required_values'], $form['account']['current_pass']);
  }

  unset($form['field_sgmap_fc']);
}

/**
 * custom login france connect button
 * @see openid_connect_login_form in openid_connect.forms.inc
 */
function sgmap_fc_login_form($form, &$form_state) {

  $form['openid_connect_client_generic_login'] = array(
    '#type' => 'submit',
    '#value' => 'Se connecter avec France connect',
    '#name' => 'generic',
  );

  return $form;
}

/**
 * Form submit handler: Log in with an OpenID Connect login provider.
 */
function sgmap_fc_login_form_submit(&$form, &$form_state) {
  openid_connect_save_destination();

  $client_name = $form_state['triggering_element']['#name'];
  $client = openid_connect_get_client($client_name);
  $scopes = openid_connect_get_scopes();
  $_SESSION['openid_connect_op'] = 'login';
  $client->authorize($scopes);
}