<?php
/**
 * @file
 * sgmap_user.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_user_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_user_image_default_styles() {
  $styles = array();

  // Exported image style: medium-profile_picture.
  $styles['medium-profile_picture'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 55,
          'height' => 55,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'medium-profile_picture',
  );

  // Exported image style: mini-profile_picture.
  $styles['mini-profile_picture'] = array(
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 40,
          'height' => 40,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'mini-profile_picture',
  );

  // Exported image style: profile_picture.
  $styles['profile_picture'] = array(
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 165,
          'height' => 165,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'profile_picture',
  );

  return $styles;
}
