<?php

/**
 * Implements hook_user_insert().
 */
function sgmap_user_user_insert(&$edit, $account, $category) {
  // for agent, check if mail domain is goog
  if ($account->field_sgmap_public[LANGUAGE_NONE][0]['value'] == 'a') {
    $user_mail_domaine = sgmap_user_get_mail_domain($account->mail);
    $domain = sgmap_base_get_vocabulary_terms_choices('nom_domaine');
    if(!in_array($user_mail_domaine, $domain)) {
      $event = new SgmapNotificationEvent();
      $event->setSubject($account);
      // notifier webmaster
      sgmap_notification_event_dispatch('user_register.agent_bad_domain', $event);
    }
  }

  // save user last destination url
  $query_parameters = drupal_get_query_parameters();
  if (isset($query_parameters['destination'])) {
    db_insert('sgmap_user_register_redirect')
    ->fields(array(
      'uid' => $account->uid,
      'url' => $query_parameters['destination']
    ))
    ->execute();
  }
}

/**
 * Implements hook_sgmap_notification_event_process.
 */
function sgmap_user_sgmap_notification_event_process($event_type, $event) {
  if($event_type == 'user_register.agent_bad_domain') {
    $webmasters = sgmap_user_get_webmasters();
    foreach($webmasters as $webmaster) {
      $notification = new SgmapNotification($webmaster, 'inscription_agent_bad_domain', $event);
      sgmap_notification_send($notification);
    }
  }
}

/**
 * Implements hook_sgmap_notification_mail_mail_keys.
 */
function sgmap_user_sgmap_notification_mail_mail_keys(){
  return array(
    'inscription_agent_bad_domain',
  );
}

/**
 * Implements hook_mail().
 */
function sgmap_user_mail($key, &$message, $params) {
  global $base_url;
  $supported_notifications = sgmap_user_sgmap_notification_mail_mail_keys();

  if(!in_array($key, array_keys($supported_notifications))) {
    // mail key not supported
    return;
  }

  $message['#user'] = $params['user'];

  $event = $params['event'];

  $template_file_messages_path = drupal_get_path('module', 'sgmap_user') . '/themes/mail';

  if ($key == 'inscription_agent_bad_domain') {
    $account = $event->getSubject();
    $message_parameters = array(
      '@agent_name'        => $account->name,
      '@agent_profile_url' => $base_url . '/user/' . $account->uid . '/edit',
      '@domain_admin_url'  => $base_url . '/admin/structure/taxonomy/nom_domaine'
    );
    sgmap_mail_notification_prepare_message($message, $template_file_messages_path, $key, $message_parameters);
  }
}