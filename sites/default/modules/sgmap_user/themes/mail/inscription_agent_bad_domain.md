Inscripion d'un agent: mauvaise extenstion de courriel
Bonjour,

@agent_name a sollicité une inscription en tant qu'agent sur Faire Simple. L'extension du courriel renseigné ne correspondant pas à une de celles listées dans la base de données de agents, vous pouvez accepter ou refuser cette inscription via l'URL ci-après : @agent_profile_url

N'oubliez pas d'ajouter l'extension dans la liste des extensions autorisées pour les agents: @domain_admin_url)

Bonne journée.