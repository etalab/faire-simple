<div class="row">
    <div class="span12">
        <div class="proposition-list">
            <div class="propositions">
                <?php if($content) : ?>
                    <?php print render($content) ?>
                <?php else: ?>
                    <h2>Mes idées soumises</h2>
                    <ul>
                        <li class="no-propositions">
                            <article class="node-proposition node-teaser">
                                <div class="main-proposition empty">
                                    <a href="/les-sujets-du-moment" title="Liste des propositions">
                                       <i class="icon-sgmap-ampoule"></i>
                                       <div><?php print t("Pas encore d'idée soumise? Vite, proposez-en une !"); ?></div>
                                   </a>
                                </div>
                            </article>
                        </li>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
