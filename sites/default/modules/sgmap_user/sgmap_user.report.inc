<?php
function _sgmap_user_get_admin_query($query_parameters) {
  $query = db_select('users', 'u');
  $query->condition('u.uid', array(0,1), 'NOT IN');
  $query->leftJoin('users_roles', 'ur', 'ur.uid = u.uid');
  $query->leftJoin('role', 'r', 'r.rid = ur.rid');
  $query->innerJoin('sgmap_user_participation_data', 'd', 'd.uid = u.uid');
  $query->innerJoin('field_revision_field_sgmap_public', 'public', 'public.entity_id = u.uid');
  $query->condition('public.entity_type', 'user');
  $query
    ->fields('u', array('uid', 'name', 'status', 'created', 'access', 'mail'))
    ->fields('r', array('name'))
    ->fields('d', array('propositions_published'));
  
  if (isset($query_parameters['role'])) {
    $query->condition('r.rid', $query_parameters['role']);
  }

  if (isset($query_parameters['status'])) {
    $query->condition('u.status', $query_parameters['status']);
  }

  if (isset($query_parameters['public'])) {
    $query->condition('public.field_sgmap_public_value', $query_parameters['public']);
  }

  if (isset($query_parameters['date_connexion_debut'])) {
    $query->condition('u.access', strtotime($query_parameters['date_connexion_debut']), '>=');
  } 

  if (isset($query_parameters['date_connexion_fin'])) {
    $query->condition('u.access', strtotime($query_parameters['date_connexion_fin']), '<='); 
  }

  if (isset($query_parameters['date_created_debut'])) {
    $query->condition('u.created', strtotime($query_parameters['date_created_debut']), '>=');
  } 

  if (isset($query_parameters['date_created_fin'])) {
    $query->condition('u.created', strtotime($query_parameters['date_created_fin']), '<='); 
  }

  return $query;
}

/** 
 * page callaback sgmap_user_report_page
 */
function sgmap_user_report_page() {
  global $user;

  $query_parameters = drupal_get_query_parameters();

  $header = array(
    'username' => array('data' => t('Email'), 'field' => 'u.mail'),
    'status' => array('data' => t('Status'), 'field' => 'u.status'),
    'roles' => array('data' => t('Roles'), 'field' => 'r.name'),
    'propositions' => array('data' => 'propositions publiées', 'field' => 'd.propositions_published'),
    'member_for' => array('data' => t('Member for'), 'field' => 'u.created', 'sort' => 'desc'),
    'access' => array('data' => t('Last access'), 'field' => 'u.access'),
    'operations' => array('data' => t('Operations')),
  );

  $query = _sgmap_user_get_admin_query($query_parameters);

  $count_result = $query->countQuery()->execute()->fetchField();

  $results = $query
    ->extend('TableSort')
    ->orderByHeader($header)
    ->extend('PagerDefault')
    ->limit(50)
    ->execute()
  ;
  $status = array(t('blocked'), t('active'));
  $rows = array();
  foreach ($results as $account) {
    $rows[$account->uid] = array(
      'username' => l($account->mail, 'user/' . $account->uid),
      'status' =>  $status[$account->status],
      'roles' => $account->r_name,
      'propositions' => $account->propositions_published,
      'member_for' =>  format_date($account->created, 'custom', 'd/m/Y'),
      'access' =>  $account->access ? format_date($account->access, 'custom', 'd/m/Y') : t('never'),
      'operations' => array('data' => array('#type' => 'link', '#title' => t('edit'), '#href' => "user/$account->uid/edit", '#options' => array('query' => $destination))),
    );
  }
  
  return array(
    'filter_form' => drupal_get_form('sgmap_user_report_filter_form'),
    'count' => array('#markup' => $count_result . ' résultat(s)'),
    'table'       => array(
      '#theme'   => 'table',
      '#header'  => $header,
      '#rows'    => $rows,
      '#sticky'  => TRUE,
      '#empty'   => 'Aucun utilisateur',
      '#attributes' => array('id' => 'results-table'),
    ),
    'pager' => array(
      '#theme' => 'pager',
      '#tags'  => array(),
    ),
    'export' => array(
      '#theme'   => 'link',
      '#text'    => 'Exporter au format csv',
      '#path'    => 'admin/sgmap_reports/users/export',
      '#options' => array(
        'query'      => $query_parameters,
        'attributes' => array(),
      ),
    ),
    'update' => $user->uid == 1 ? drupal_get_form('sgmap_user_participation_count_form') : ''
  );
}

function sgmap_user_report_filter_form() {
  $query_parameters = drupal_get_query_parameters();

  $user_roles = user_roles();
  unset($user_roles[1], $user_roles[2], $user_roles[3]);
  $form['filters'] = array(
    '#type'  => 'fieldset',
    '#title' => 'Filtres',
    '#collapsible' => TRUE,
    '#collapsed' => $query_parameters['filter'] ? FALSE : TRUE,
  );
  $form['filters']['role'] = array(
    '#title'         => 'rôle',
    '#type'          => 'select',
    '#empty_option' => 'Tous',
    '#empty_value'   => 0,
    '#options'       => $user_roles,
    '#default_value' => $query_parameters['role'] ? $query_parameters['role'] : ''
  );
  $form['filters']['status'] = array(
    '#title'   => 'statut',
    '#type'    => 'select',
    '#empty_option' => 'Tous',
    '#empty_value'   => '',
    '#options' => array(
      1  => 'actif',
      0  => 'bloqué',
    ),
    '#default_value' => $query_parameters['status']
  );

  $form['filters']['public'] = array(
    '#title'         => 'public',
    '#type'          => 'select',
    '#empty_option' => 'Tous',
    '#empty_value'   => 0,
    '#options'       => sgmap_base_get_public_choices(),
    '#default_value' => $query_parameters['public'] ? $query_parameters['public'] : ''
  );

  $form['filters']['connexion'] = array(
    '#type'  => 'fieldset',
    '#title' => 'Date de dernière connexion'
  );
  $form['filters']['connexion']['date_connexion_debut'] = array(
    '#title' => 'du',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
    '#default_value' => $query_parameters['date_connexion_debut'] ? $query_parameters['date_connexion_debut'] : ''
  );
  $form['filters']['connexion']['date_connexion_fin'] = array(
    '#title' => 'au',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
    '#default_value' => $query_parameters['date_connexion_fin'] ? $query_parameters['date_connexion_fin'] : ''
  );

  $form['filters']['created'] = array(
    '#type'  => 'fieldset',
    '#title' => 'Date de la création du compte'
  );
  $form['filters']['created']['date_created_debut'] = array(
    '#title' => 'du',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
    '#default_value' => $query_parameters['date_created_debut'] ? $query_parameters['date_created_debut'] : ''
  );
  $form['filters']['created']['date_created_fin'] = array(
    '#title' => 'au',
    '#type'  => 'date_popup',
    '#date_format' => 'd/m/Y',
    '#default_value' => $query_parameters['date_created_fin'] ? $query_parameters['date_created_fin'] : ''

  );
  $form['filters']['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Filtrer',
    '#submit' => array('sgmap_user_report_filter_submit')
    );

  $form['filters']['reinitialiser'] = array(
    '#type'  => 'submit',
    '#value' => 'Réinitialiser',
    '#submit' => array('sgmap_user_report_clean_filters')
    );
  return $form;
}

/**
 * filter form submit handler
 */
function sgmap_user_report_filter_submit($form, &$form_state) {
  $query = array('filter' => 1);

  if (isset($form_state['values']['role']) && $form_state['values']['role']) {
    $query['role'] = $form_state['values']['role'];
  }

  if (isset($form_state['values']['status']) && $form_state['values']['status'] != '') {
    $query['status'] = $form_state['values']['status'];
  }

  if (isset($form_state['values']['public']) && $form_state['values']['public']) {
    $query['public'] = $form_state['values']['public'];
  }

  if ($form_state['values']['date_connexion_debut']) {
    $query['date_connexion_debut'] = $form_state['values']['date_connexion_debut'];
  } 

  if ($form_state['values']['date_connexion_fin']) {
    $query['date_connexion_fin'] = $form_state['values']['date_connexion_fin'];
  }

  if ($form_state['values']['date_created_debut']) {
    $query['date_created_debut'] = $form_state['values']['date_created_debut'];
  }

  if ($form_state['values']['date_created_fin']) {
    $query['date_created_fin'] = $form_state['values']['date_created_fin'];
  }

  $form_state['redirect'] = array(
    'admin/sgmap_reports/users',
    array(
      'query'    => $query,
      'fragment' => 'results-table'
    )
  );

}

function sgmap_user_report_export() {
  global $user;
  $cols = array(
    'pseudo',
    'nom',
    'prénom',
    'mail',
    'URL',
    'type',
    'ville',
    'code postal',
    'Ministère de rattachement',
    'role',
    'peut être contacté',
    'date de création du compte',
    'date de la dernière connextion',
    'nombre propositions publiées',
    'nombre propositions refusées',
    'nombre de poprositions passées en mesure engagée',
    'nombre de commentaires',    
    'nombre de recommandations',
  );

  array_walk($cols, 'sgmap_export_format_cel');

  $export_file = sgmap_export_get_file_object('utilisateurs', $user, $cols);
  $query_parameters = drupal_get_query_parameters();
  
  $batch = array(
    'operations' => array(
      array('sgmap_user_report_export_process', array($export_file, $query_parameters)),
    ),
    'finished'         => 'sgmap_user_report_export_finished',
    'title'            => t('Processing export batch'),
    'init_message'     => t('Export batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Export batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'sgmap_user') . '/sgmap_user.report.inc',
  );

  batch_set($batch);
  batch_process('admin/sgmap_reports/users');
}

function sgmap_user_report_export_process($export_file, $query_parameters, &$context) {
  $query = _sgmap_user_get_admin_query($query_parameters);
  $public_names = sgmap_base_get_public_choices();

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_user'] = 0;
    $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results'] = array('file' => $export_file);
  }

  $query->condition('u.uid', $context['sandbox']['current_user'], '>');

  $query->addField('public', 'field_sgmap_public_value');
  
  $query->leftJoin('field_data_field_user_lastname', 'ln', 'ln.entity_id = u.uid');
  $query->addField('ln', 'field_user_lastname_value');

  $query->leftJoin('field_data_field_user_firstname', 'fn', 'fn.entity_id = u.uid');
  $query->addField('fn', 'field_user_firstname_value');

  $query->leftJoin('field_data_field_user_admin_contact', 'ac', 'ac.entity_id = u.uid');
  $query->addField('ac', 'field_user_admin_contact_value');

  $query->leftJoin('field_revision_field_sgmap_user_location', 'ville', 'ville.entity_id = u.uid');
  $query->addField('ville', 'field_sgmap_user_location_value');

  $query->leftJoin('field_data_field_user_cp', 'cp', 'cp.entity_id = u.uid');
  $query->addField('cp', 'field_user_cp_value');

  $query->leftJoin('field_revision_field_user_ministere', 'fm', 'fm.entity_id = u.uid');
  $query->leftJoin('taxonomy_term_data', 'ministere', 'ministere.tid = fm.field_user_ministere_tid');
  $query->addField('ministere', 'name');

  $query->innerJoin('sgmap_user_participation_data', 'data', 'data.uid = u.uid');
  $query->fields('data', array('propositions_published', 'propositions_refused', 'propositions_engaged', 'comments','recommandations'));

  $query->range(0, 100);
  $results = $query->execute();
  
  $fp = fopen(drupal_realpath($export_file->uri), 'a');
  

  foreach ($results as $user) {
    $row = array(
      $user->name,
      $user->field_user_lastname_value,
      $user->field_user_firstname_value,
      $user->mail,
      url('user/' . $user->uid, array('absolute' => TRUE)),
      $public_names[$user->field_sgmap_public_value],
      $user->field_sgmap_user_location_value,
      $user->field_user_cp_value,
      $user->ministere_name,
      $user->r_name,
      $user->field_user_admin_contact_value ? 'oui' : 'non',
      format_date($user->created, 'custom', 'd/m/Y'),
      $user->access ? format_date($user->access, 'custom', 'd/m/Y') : t('never'),
      $user->propositions_published,
      $user->propositions_refused,
      $user->propositions_engaged,
      $user->comments,
      $user->recommandations
    );
    
    array_walk($row, 'sgmap_export_format_cel');

    fputcsv($fp, $row);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_user'] = $user->uid;
  }

  fclose($fp);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function sgmap_user_report_export_finished($success, $results, $operations) {
  if ($success) {
    global $user;
    $file = $results['file'];
    $file = file_save($file);

    sgmap_export_send_export($file);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}

/**
 * clean filter submit handler
 */
function sgmap_user_report_clean_filters($form, &$form_state) {
  $form_state['redirect'] = array('admin/sgmap_reports/users');
}

/**
 * form to update user participation data
 */
function sgmap_user_participation_count_form() {
  $form['description'] = array(
    '#type'  => 'item',
    '#title' => t('Appuyez sur le boutton ci-dessous pour mettre à jour les données des utilisateurs'),
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Mettre à jour'),
  );
  $form['#redirect'] = FALSE;
  return $form;
}

/**
 * submit handler for sgmap_user_participation_count_form
 */
function sgmap_user_participation_count_form_submit($form, &$form_state) {
  $path = drupal_get_path('module', 'sgmap_user');
  $batch = array(
    'title' => t('Updating'),
    'operations' => array(
      array('sgmap_user_participation_count_update_batch', array()),
    ),
    'file' => $path . '/sgmap_user.report.inc',
  );
  batch_set($batch);
}

/**
 * batch process update user data
 */
function sgmap_user_participation_count_update_batch(&$context) {
  sgmap_user_participation_count_update();
}
