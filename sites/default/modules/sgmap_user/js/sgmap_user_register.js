(function ($) {

  Drupal.behaviors.sgmapUserRegister = {
    attach: function (context, settings) {
      $( ".form-item-field-sgmap-public-und input[type=radio]" ).on( "click", function() {
        $("input:checked").val() == 'a' ? $('#mail-desc').show() : $('#mail-desc').hide();
      });
    }
  };

})(jQuery);