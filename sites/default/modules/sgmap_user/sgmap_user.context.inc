<?php
/**
 * @file
 * sgmap_user.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sgmap_user_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_show';
  $context->description = '';
  $context->tag = 'user';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'sgmap_user-user_proposals' => array(
          'module' => 'sgmap_user',
          'delta' => 'user_proposals',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
        'sgmap_bookmark-user_bookmarked_contents' => array(
          'module' => 'sgmap_bookmark',
          'delta' => 'user_bookmarked_contents',
          'region' => 'content_bottom',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('user');
  $export['user_show'] = $context;

  return $export;
}
