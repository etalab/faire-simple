<?php

/**
 * Implements hook_form_alter().
 */
function sgmap_search_form_alter(&$form, &$form_state, $form_id) {

  if($form_id != 'apachesolr_search_custom_page_search_form') {
    return false;
  }
  unset($form['required']);
  unset($form['basic']['retain-filters']);
  unset($form['basic']['get']);
  $form['basic']['keys']['#title'] = 'Rechercher par mots clés';
  $form['basic']['keys']['#attributes'] = array('placeholder' => 'Mots-clés');
  $form['basic']['keys']['#theme_wrappers'][] = 'bootstrap_search_form_wrapper';

  if(arg(0) == 'search') {
    $form['basic']['submit']['#attributes'] = array('class' => array('element-invisible'));
  }
}

function sgmap_search_apachesolr_search_page_alter(array &$build, array $search_page) {

  if($search_page['page_id'] == 'core_search') {
    $build['search_infos'] = sgmap_search_get_search_infos_build();
    $build['search_form']['#weight']    = 1;
    $build['search_infos']['#weight']   = 2;
    $build['search_results']['#weight'] = 3;
  }
}

/**
 * sgmap_search_get_search_infos_build
 *
 */
function sgmap_search_get_search_infos_build() {
  $build = array();
  $search_infos = _sgmap_search_get_search_infos();

  $build['results count'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'search-result-count',
        'scrollhack',
      ),
      'id' => 'search-result-count',
    ),
    'content' => array(
      '#markup' => format_plural($search_infos['num_found'],
        '<p><strong>@num_found</strong> résultat</p>',
        '<p><strong>@num_found</strong> résultats</p>',
        array('@num_found' => $search_infos['num_found'] ? $search_infos['num_found'] : 0)
      )
    )
  );

  return $build;
}

/**
 * _sgmap_search_get_search_infos
 *
 */
function _sgmap_search_get_search_infos() {
  $infos = null;

  $search_page = apachesolr_search_page_load('core_search');
  $env_id = $search_page['env_id'];

  if (apachesolr_has_searched($env_id)) {
    $query = apachesolr_current_query($env_id);
    $response = apachesolr_static_response_cache($query->getSearcher());
    $params = $query->getParams();
    $infos = array();
    $infos['keywords'] = $params['q'];
    $infos['num_found'] = $response->response->numFound;
  }

  return $infos;
}
