<?php

function sgmap_api_get_access_account() {
  $form['name'] = array(
    '#title' => t('Compte pour l\'API'),
    '#type' => 'fieldset',
    );

  $header = array(
    'name',
    );

  $query = db_select('sgmap_api_account', 'a');
  $query->fields('a', array('account','id_account'));
  $accounts = $query->execute()->fetchAll();

  $rows = array();
  foreach ($accounts as $account) {
    $rows[$account->id_account] = array(
      $account->account,
      );
  }

  $form['name']['table'] = array(
    '#type'    => 'tableselect',
    '#header'  => $header,
    '#options' => $rows,
    '#sticky'  => TRUE,
    '#empty'   => 'Aucune compte pour l\'API',
    '#attributes' => array('id' => 'results-table'),
    );

  $form['name']['suppression'] = array(
    '#type' => 'submit',
    '#value' => t('Supprimer les comptes cochés'),
    );

  $form['name']['account_name'] = array(
    '#type'          => 'textfield',
    '#title'         => 'Nom :',
    '#default_value' => '',
    );

  $form['name']['account_passwd'] = array(
    '#type'          => 'textfield',
    '#title'         => 'Mot de passe :',
    '#default_value' => '',
    );

  $form['name']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Ajouter'),
    );

  return $form;
}

function sgmap_api_get_access_account_validate($form, &$form_state) {
  if($form_state['clicked_button']['#parents'][0] == 'submit'){
    if (empty($form_state['values']['account_name'])) {
      form_set_error('account_name',t('Merci de remplire le champ nom.'));
    }
    if (empty($form_state['values']['account_passwd'])){
      form_set_error('account_passwd',t('Merci de remplire le champ mot de passe.'));
    }
  }
}

/**
 * Fonction qui crypt le mot de passe pour qu'il soit compatible avec l'appliquation Silex 
 * 
 */
function _password_crypt($pass){
  $algo = 'sha512';
  $nbiteration = 5000;
  $digest = hash($algo, $pass, true);
  for ($i = 1; $i < $nbiteration; $i++) {
    $digest = hash($algo, $digest.$pass, true);
  }
  return base64_encode($digest);
}

function sgmap_api_get_access_account_submit($form, &$form_state) {
  if($form_state['clicked_button']['#parents'][0] == 'submit'){
    $table = 'sgmap_api_account';
    $record = new stdClass();
    $record->account = check_plain($form_state['values']['account_name']);
    $record->psswd = _password_crypt($form_state['values']['account_passwd']);
    drupal_write_record($table, $record);
    drupal_set_message(t('Le compte a bien été ajouté.'));
  } else {
    $selected_uids = array_filter($form_state['values']['table']);
    foreach ($selected_uids as $selected_uid) {
      db_delete('sgmap_api_account')
        ->condition('id_account', $selected_uid)
        ->execute();
    }
  }
}

function sgmap_api_get_access_key() {
  $form['name'] = array(
    '#title' => t('Clés pour l\'API'),
    '#type' => 'fieldset',
    );

  $header = array(
    'cles',
    'nom',
    );

  $query = db_select('sgmap_api_API_key', 'k');
  $query->fields('k', array('cles','name', 'id_cles'));
  $cles = $query->execute()->fetchAll();

  $rows = array();
  foreach ($cles as $cle) {
    $rows[$cle->id_cles] = array(
      $cle->cles,
      $cle->name,
      );
  }

  $form['name']['table'] = array(
    '#type'   => 'tableselect',
    '#header'  => $header,
    '#options'    => $rows,
    '#sticky'  => TRUE,
    '#empty'   => 'Aucune Clés pour l\'API',
    '#attributes' => array('id' => 'results-table'),
    );
  
  $form['name']['suppression'] = array(
    '#type' => 'submit',
    '#value' => t('Supprimer les clés cochées'),
    );

  $form['name']['key_name'] = array(
    '#type'          => 'textfield',
    '#title'         => 'Nom :',
    '#default_value' => '',
    );

  $form['name']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Générer une nouvelle clé'),
    );

  return $form;
}

function sgmap_api_get_access_key_validate($form, &$form_state) {
  if($form_state['clicked_button']['#parents'][0] == 'submit'){
    if (empty($form_state['values']['key_name'])) {
      form_set_error('key_name',t('Merci de remplire le champ nom.'));
    }
  }
}

function sgmap_api_get_access_key_submit($form, &$form_state){
  if($form_state['clicked_button']['#parents'][0] == 'submit'){
    $num = 1;
    $length = 32;
    $allowCaractere = array ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $keys = array();
    for ($j=0; $j < $num ; $j++) {
      $keys[$j] = '';
      for ($i=0; $i < $length ; $i++) {
        $rand_keys = array_rand($allowCaractere);
        $keys[$j] .= $allowCaractere[$rand_keys];
      }
    }
    foreach ($keys as $key) {
      $table = 'sgmap_api_API_key';
      $record = new stdClass();
      $record->cles = $key;
      $record->name = check_plain($form_state['values']['key_name']);
      drupal_write_record($table, $record);
    }
    drupal_set_message(t('La clé a bien été ajouté.'));
  } else {
    $selected_uids = array_filter($form_state['values']['table']);
    foreach ($selected_uids as $selected_uid) {
      db_delete('sgmap_api_API_key')
      ->condition('id_cles', $selected_uid)
      ->execute();
    }
  }
}