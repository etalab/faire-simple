<?php
require_once __DIR__.'/../../../../includes/password.inc';

function sgmap_panel_partenaire_import($delete_limit = 0) {
  $path = drupal_get_path('module', 'sgmap_panel');
  if($delete_limit) {
    $batch = array(
      'operations' => array(
        array('sgmap_panel_csv_parser', array($delete_limit)),
      ),
      'title'            => t('Processing import batch'),
      'error_message'    => t('Import batch has encountered an error.'),
      'file' => $path . '/sgmap_panel.import.inc',
    );
  }
  else {
    $batch = array(
      'operations' => array(
        array('sgmap_panel_csv_parser', array(1)),
        array('sgmap_panel_csv_parser', array(2)),
        array('sgmap_panel_csv_parser', array(3)),
        array('sgmap_panel_csv_parser', array(4)),
        array('sgmap_panel_csv_parser', array(5)),
        array('sgmap_panel_csv_parser', array(6)),
      ),
      'title'            => t('Processing import batch'),
      'error_message'    => t('Import batch has encountered an error.'),
      'progress_message' => t('Import batch in process.'),
      'file' => $path . '/sgmap_panel.import.inc',
    );
  }
  

  batch_set($batch);
  batch_process('admin/people');
}

/**
 * one shoot user partenaire import
 */
function sgmap_panel_csv_parser($sequence, &$context) {
  switch ($sequence) {
    case 1:
      $debut = 1;
      $fin = 200;
      break;
    case 2:
      $debut = 201;
      $fin = 400;
      break;
    case 3:
      $debut = 401;
      $fin = 600;
      break;
    case 4:
      $debut = 601;
      $fin = 800;
      break;
    case 5:
      $debut = 801;
      $fin = 1000;
      break;
    case 6:
      $debut = 1001;
      $fin = 1100;
      break;
  }

  if ($debut && $fin) {
    $path = drupal_get_path('module', 'sgmap_panel');
    $file = $path . '/associes.csv';
    
    $row = 0;
    if (($handle = fopen($file, "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1500, ";")) !== FALSE) {
        $num = count($data);
        for ($c = 0; $c < $num; $c++) {
          $data[$c];
        }
        $row++;
        if($row > $debut && $row<=$fin) {
          $name = $data[1];
          $civilite  = $data[2];
          $lastname = $data[4];
          $firstname = $data[5];
          $mail      = $data[6];
          $password  = $data[7];
          $ville     = $data[8];

          // if user_name already exists in database
          $query = db_select('users', 'u')
            ->fields('u', array('uid', 'mail'))
            ->condition('name', utf8_encode($name));
          $result = $query->execute()->fetchField();
          if ($result) {
            $name = $name . '-1';
          }

          // if user is already subscribed
          // make him partenaire
          $query = db_select('users', 'u')
            ->fields('u', array('uid', 'mail'))
            ->condition('mail', $mail);
          $result = $query->execute()->fetchAssoc();
          if ($result) {
            $user = user_load($result['uid']);
            $user->field_sgmap_est_partenaire[LANGUAGE_NONE][0]['value'] = 1;
            user_save($user);
            // notifier user
            $params = array(
              'name' => $user->name,
            );
          }
          else {
            $account = new StdClass();
            $account->is_new = TRUE;
            $account->status = TRUE;
            $account->name = $name ? utf8_encode($name) : utf8_encode($firstname . ' ' . $lastname);
            $account->pass = user_hash_password($password);
            $account->mail = $mail;
            $account->init = $mail;
            $account->field_user_civilite[LANGUAGE_NONE][0]['value'] = $civilite == 1 ? 'h' : 'f';
            $account->field_user_firstname[LANGUAGE_NONE][0]['value'] = utf8_encode($firstname);
            $account->field_user_lastname[LANGUAGE_NONE][0]['value'] = utf8_encode($lastname);
            $account->field_sgmap_public[LANGUAGE_NONE][0]['value'] = 'p';
            $account->field_sgmap_est_partenaire[LANGUAGE_NONE][0]['value'] = 1;
            $account->field_sgmap_user_location[LANGUAGE_NONE][0]['value'] = utf8_encode($ville);
            $user = user_save($account);
          }
        }
      }
      fclose($handle);
    }
  }
}

