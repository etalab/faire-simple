[Faire simple] Votre compte Associé sur Faire Simple vient d'être créé
Bonjour @panel.user_name,

Bienvenue sur faire-simple.gouv.fr !

Vous avez reçu cette semaine de la part de BVA un email concernant votre nouveau statut d'Associé de la modernisation de l'action publique. Votre lieu de rassemblement se situe désormais sur faire-simple.gouv.fr, le site dédié à la simplification et à la qualité du service rendu par les services publics. Sur ce site se retrouvent les usagers particuliers et les entreprises d’une part et les agents publics d’autre part. Vous figurerez parmi tous les internautes avec un statut particulier qui vous distinguera des autres usagers car vous êtes les témoins et les garants de l’expérience usager.

Pour vous connecter: @panel.user_login avec votre adresse email: @panel.user_mail et votre mot de passe



