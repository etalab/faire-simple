<?php print($codePanel); ?>
<?php if($userLogin) : ?>
  <?php if($userPartenaire) : ?>
    <a class="btn btn-bleu btn-large" href="/panel/redirect/<?php print $uid ?>">Voir mon profil associé</a>
  <?php else : ?>
    <?php if($userParticulier): ?>
      <a class="btn btn-bleu btn-large" href="/panel/subscribe/<?php print $uid ?>">Devenir associé</a>
    <?php else : ?>
      <p class="alert alert-info">Vous ne disposez pas du statut de particulier, vous ne pouvez donc pas prétendre à devenir associé.</p>
    <?php endif ; ?>
  <?php endif; ?>
<?php else : ?>
  <a class="btn btn-bleu btn-large" href="/user/login?destination=les-associes-de-la-modernisation">Je me connecte</a>
  <a class="btn btn-large" href="/user/register">Créer votre compte</a>
<?php endif; ?>