<?php

function sgmap_panel_config_panel() {

  $form['sgmap_panel_panel_value'] = array(
    '#title' => 'Corps du texte de la page panel',
    '#type'  => 'text_format',
    '#value' => variable_get('sgmap_panel_panel_value', NULL),
  );

  $form['sgmap_panel_connexion_url'] = array(
     '#type'           => 'textfield',
     '#title'          => t("URL de redirection vers la page de connexion au panel"),
     '#default_value'  => variable_get('sgmap_panel_connexion_url', NULL),
  );

  $form['sgmap_panel_subscription_url'] = array(
     '#type'           => 'textfield',
     '#title'          => t("URL de redirection vers la page d'inscription au panel"),
     '#default_value'  => variable_get('sgmap_panel_subscription_url', NULL),
  );

  $form['panel']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Enregistrer'),
  );

  return $form;
}

function sgmap_panel_config_panel_submit($form, &$form_state) {
  variable_set('sgmap_panel_panel_value', $form_state['input']['sgmap_panel_panel_value']['value']);
  variable_set('sgmap_panel_connexion_url', $form_state['input']['sgmap_panel_connexion_url']);
  variable_set('sgmap_panel_subscription_url', $form_state['input']['sgmap_panel_subscription_url']);
}
