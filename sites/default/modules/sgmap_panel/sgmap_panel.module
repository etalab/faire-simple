<?php

/**
 * Implements hook_permission().
 */
function sgmap_panel_permission() {
  return array(
    'access panel page admin' =>  array(
      'title' => t('Access panel page admin'),
      'description' => t('Allow user to modify the page panel'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function sgmap_panel_menu() {
  $items['les-associes-de-la-modernisation'] = array(
    'title' => 'Les associés de la modernisation',
    'page callback' => 'sgmap_panel_page_panel',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  $items['panel/redirect/%user'] = array(
    'page callback' => 'sgmap_panel_redirect_action',
    'page arguments' => array(2),
    'access callback' => 'sgmap_panel_redirect_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );

  $items['panel/subscribe/%user'] = array(
    'page callback' => 'sgmap_panel_subscribe_action',
    'page arguments' => array(2),
    'access callback' => 'sgmap_panel_redirect_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/sgmap/panel'] = array(
    'title' => 'Gestion PANEL',
    'description' => 'Edition de la page panel',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sgmap_panel_config_panel'),
    'file' => 'sgmap_panel.admin.inc',
    'access callback' => 'user_access',
    'access arguments' => array('access panel page admin'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * access callback for sgmap_panel_redirect_action
 */
function sgmap_panel_redirect_access($account) {
  global $user;
  if ($user->uid == $account->uid) {
    return TRUE;
  }
}

/**
 * Implements hook_user_view().
 */
function sgmap_panel_user_view($account, $view_mode, $langcode) {
  if (sgmap_panel_est_partenaire($account)) {
    $account->content['panel_link'] = array(
      '#theme' => 'link',
      '#text' => 'Mon profil associé',
      '#path' => 'panel/redirect/' . $account->uid,
      '#options' => array(
        'attributes' => array('class' => array('btn','panel')),
          'html' => TRUE,
      ),
    );
  }
}

/**
 * user key generator
 * @return [string] key
 */
function sgmap_panel_key_gen() {
  $key = drupal_hash_base64(drupal_random_bytes(55));
  
  return $key;
}

/**
 * get key by user uid
 * @param  [integer] $uid
 * @return [string] key
 */
function sgmap_panel_get_user_key($uid) {
  $query = db_select('sgmap_panel_user_key', 'k')
    ->fields('k', array('user_key', 'updated'))
    ->condition('k.uid', $uid);
  $result = $query->execute()->fetchAssoc();
  return $result;
}

/**
 * update user key
 * @param  [integer] $uid 
 * @return [string] new key
 */
function sgmap_panel_update_user_key($uid) {
  db_merge('sgmap_panel_user_key')
    ->key(array('uid' => $uid))
    ->fields(array(
      'user_key' => sgmap_panel_key_gen(),
      'updated'  => time()
    ))
    ->execute()
    ;
  $key_data = sgmap_panel_get_user_key($uid);
  return $key_data;
}

/**
 * page callback
 * generate key and redirect to solirem panel page
 */
function sgmap_panel_redirect_action($user) {
  $key_data = sgmap_panel_update_user_key($user->uid);
  drupal_goto(variable_get('sgmap_panel_connexion_url', NULL) . '?uid=' . $user->uid . '&token=' . $key_data['user_key']);
}

/**
 * page callback
 * generate key and redirect to subscribe solirem page
 */
function sgmap_panel_subscribe_action($user) {
  $key_data = sgmap_panel_update_user_key($user->uid);
  drupal_goto(variable_get('sgmap_panel_subscription_url', NULL) . '?uid=' . $user->uid . '&token=' . $key_data['user_key']);
}

/**
 * check if the user is associé
 * @param $user variable user
 * @return boolean
 */

function sgmap_panel_est_partenaire($user) {
  if($user->field_sgmap_est_partenaire['und'][0]['value'] == 1) {
    return true;
  } else {
    return false;
  }
}

/**
 * check if the user is particulier
 * @param $user variable user
 * @return boolean
 */

function sgmap_panel_est_particulier($user) {
  if($user->field_sgmap_public['und'][0]['value'] == 'p') {
    return true;
  } else {
    return false;
  }
}

/**
 * Implements hook_theme().
 */
function sgmap_panel_theme($existing, $type, $theme, $path) {
  return array(
    'sgmap_panel_page' => array(
      'template'  => 'sgmap_panel-page',
      'variables' => array(
        'codePanel' => NULL, 
        'userLogin' => NULL,
        'userParticulier' => NULL,
        'userPartenaire'  => NULL,
        'uid' => NULL
        ),
      'path' => $path .'/themes',
    ),
  );
}

/**
 * page callback
 * Page Panel
 */

function sgmap_panel_page_panel() {
  global $user;
  $userLogin = false;
  $userPartenaire = false;
  $userParticulier = false;

  if(user_is_logged_in()) {
    $userLogin = true;
    $currentUser = user_load($user->uid);
    if(sgmap_panel_est_partenaire($currentUser)) {
      $userPartenaire = true;
    } else {
      if(sgmap_panel_est_particulier($currentUser)){
        $userParticulier = true; 
      }
    }    
  }

  return array(
    '#theme' => 'sgmap_panel_page',
    '#codePanel' => variable_get('sgmap_panel_panel_value', ''),
    '#userLogin' => $userLogin,
    '#userParticulier' => $userParticulier,
    '#userPartenaire' => $userPartenaire,
    '#uid' => $currentUser->uid,
  );
}