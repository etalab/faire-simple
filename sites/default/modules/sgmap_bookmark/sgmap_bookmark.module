<?php
/**
 * @file
 * Code for the SGMAP Bookmark feature.
 */

include_once 'sgmap_bookmark.features.inc';

/**
 * Implements hook_node_view().
 */
function sgmap_bookmark_node_view($node, $view_mode, $langcode) {
  if(
    user_is_anonymous() ||
    $view_mode != 'full' ||
    !in_array($node->type, _sgmap_bookmark_node_types())
  ) {
    return NULL;
  }

  $node->content['bookmark_btn'] = array(
    '#weight' => -10,
    '#markup' => flag_create_link('bookmark', $node->nid)
  );

}

/**
 * Implements hook_block_info().
 */
function sgmap_bookmark_block_info() {
  $blocks['user_bookmarked_contents'] = array(
    'info' => t('SGMAP user bookmarked contents'),
    'cache' => DRUPAL_NO_CACHE
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function sgmap_bookmark_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'user_bookmarked_contents':
      $block['content'] = sgmap_bookmark_user_bookmarked_contents_content();
    break;
  }

  return $block;
}

function sgmap_bookmark_user_bookmarked_contents_content() {

  global $user;
  global $pager_total;

  // load bookmarks
  $bookmarked_contents_nids = array_keys(sgmap_bookmark_get_user_bookmarked_contents($user->uid));

  $bookmarked_contents_views = array();

  $bookmarked_contents = node_load_multiple($bookmarked_contents_nids);

  foreach($bookmarked_contents as $bookmarked_content) {
    $bookmarked_contents_views[] = render(node_view($bookmarked_content, 'teaser'));
  }

  $pagerElement = 1;

  if(!count($bookmarked_contents_views)) {
    return array();
  }

  return array(
    '#prefix' => '<section id="user-bookmarked-propositions">',
    '#suffix' => '</section>',
    'title'   => array(
      '#markup' => '<h2>Mes idées suivies</h2>'
    ),
    'content' => array(
      '#theme_wrappers' => array('container'),
      '#attributes'     => array(
        'data-next-selector' => 'div.pagination .next a',
        'data-nav-selector'  => 'div.pagination',
        'data-item-selector' => 'ul > li',
        'data-pager-element' => $pagerElement,
        'data-max-page'      => ($pager_total[$pagerElement] - 1),
        'data-btn-text'      => "Voir plus de propositions",
        'class'              => array('proposition-list', 'row-fluid', 'infinite-scroll'),
        'id'                 => 'user-bookmarked-propositions-list'
      ),
      'content' => array(
        '#theme'  => 'item_list',
        '#items'  => $bookmarked_contents_views
      ),
      'pager' => array(
        '#theme'   => 'pager',
        '#element' => $pagerElement
      )
    )
  );
}

function sgmap_bookmark_get_user_bookmarked_contents($uid) {

  // load bookmark flag record
  $bookmark_flag = flag_load('bookmark');

  return db_select('flagging', 'f')
      ->extend('PagerDefault')
      ->fields('f', array('entity_id'))
      ->condition('fid', $bookmark_flag->fid)
      ->condition('uid', $uid)
      ->limit(8)
      ->element(1)
      ->execute()
      ->fetchAllAssoc('entity_id')
  ;
}


function _sgmap_bookmark_node_types() {
  return array(
    'proposition',
  );
}
