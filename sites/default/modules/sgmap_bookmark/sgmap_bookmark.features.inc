<?php
/**
 * @file
 * sgmap_bookmark.features.inc
 */

/**
 * Implements hook_flag_default_flags().
 */
function sgmap_bookmark_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favoris".
  $flags['bookmark'] = array(
    'entity_type' => 'node',
    'title' => 'Favoris',
    'global' => 0,
    'types' => array(
      0 => 'proposition',
    ),
    'flag_short' => 'Suivre <i class="icon-sgmap-etoile icon-white"></i>',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Ne plus suivre <i class="icon-sgmap-etoile icon-white"></i>',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'sgmap_bookmark',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
