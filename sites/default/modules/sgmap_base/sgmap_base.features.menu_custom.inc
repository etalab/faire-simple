<?php
/**
 * @file
 * sgmap_base.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function sgmap_base_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-les-campagnes.
  $menus['menu-les-campagnes'] = array(
    'menu_name' => 'menu-les-campagnes',
    'title' => 'Les campagnes',
    'description' => '',
  );
  // Exported menu: menu-liens-bas-de-page.
  $menus['menu-liens-bas-de-page'] = array(
    'menu_name' => 'menu-liens-bas-de-page',
    'title' => 'Liens bas de page',
    'description' => '',
  );
  // Exported menu: menu-liens-pied-de-page.
  $menus['menu-liens-pied-de-page'] = array(
    'menu_name' => 'menu-liens-pied-de-page',
    'title' => 'Liens pied de page',
    'description' => '',
  );
  // Exported menu: menu-notre-demarche.
  $menus['menu-notre-demarche'] = array(
    'menu_name' => 'menu-notre-demarche',
    'title' => 'Notre démarche',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Les campagnes');
  t('Liens bas de page');
  t('Liens pied de page');
  t('Notre démarche');

  return $menus;
}
