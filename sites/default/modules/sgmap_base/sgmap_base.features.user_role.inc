<?php
/**
 * @file
 * sgmap_base.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function sgmap_base_user_default_roles() {
  $roles = array();

  // Exported role: rédacteur.
  $roles['rédacteur'] = array(
    'name' => 'rédacteur',
    'weight' => 4,
  );

  // Exported role: webmaster.
  $roles['webmaster'] = array(
    'name' => 'webmaster',
    'weight' => 3,
  );

  // Exported role: webmaster de campagne.
  $roles['webmaster de campagne'] = array(
    'name' => 'webmaster de campagne',
    'weight' => 5,
  );

  return $roles;
}
