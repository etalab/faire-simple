<?php
/**
 * @file
 * sgmap_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgmap_base_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sgmap_base_image_default_styles() {
  $styles = array();

  // Exported image style: large_statique.
  $styles['large_statique'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 940,
          'height' => 355,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'large_statique',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sgmap_base_node_info() {
  $items = array(
    'page_statique' => array(
      'name' => t('Page statique'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
