(function($) {
    $(document).ready(function() {
        if($(".line-img-box").length > 0) {
            var $window = $(window);
            $window.scroll(function(){
                if($window.scrollTop() > 300) {
                    $(".line-1 .line-img-box").fadeIn(300);
                }
                if($window.scrollTop() > 500) {
                    $(".line-2 .line-img-box").fadeIn(300);
                }
                if($window.scrollTop() > 750) {
                    $(".line-3 .line-img-box").fadeIn(300);
                }
            });
        }
    });
})(jQuery);

