<?php
/**
 * @file
 * Code for the SGMAP Base feature.
 */

include_once 'sgmap_base.features.inc';

/**
 * Implements hook_menu().
 */
function sgmap_base_menu() {
  $items['page-404'] = array(
    'title'           => t('Erreur 404'),
    'page callback'   => 'sgmap_base_404_page',
    'access callback' => TRUE,
  );
  $items['page-403'] = array(
    'title'            => t('Erreur 403'),
    'page callback'    => 'sgmap_base_403_page',
    'access callback'  => TRUE,
  );

  $items['admin/config/sgmap'] = array(
    'title'            => 'SGMAP Configuration',
    'description'      => '',
    'position'         => 'left',
    'weight'           => -100,
    'page callback'    => 'system_admin_menu_block_page',
    'access arguments' => array('administer site configuration'),
    'file'             => 'system.admin.inc',
    'file path'        => drupal_get_path('module', 'system'),
  );

  $items['admin/sgmap_reports'] = array(
    'title'            => 'SGMAP Rapports',
    'description'      => '',
    'page callback'    => 'system_admin_menu_block_page',
    'access arguments' => array('administer proposition'),
    'file'             => 'system.admin.inc',
    'file path'        => drupal_get_path('module', 'system'),
  );

  $items['comment-ca-marche'] = array(
    'title'           => t('Comment ça marche'),
    'page callback'   => 'sgmap_base_comment_ca_marche',
    'access callback' => TRUE,
    'type'            => MENU_CALLBACK
  );

  $items['fabrique-de-solutions/comment-ca-marche'] = array(
    'title'           => t('La fabrique de solution'),
    'page callback'   => 'sgmap_base_fds',
    'access callback' => TRUE,
    'type'            => MENU_CALLBACK
  );

  return $items;
}

function sgmap_base_404_page() {
  return theme('sgmap_404');
}

function sgmap_base_403_page() {
  return theme('sgmap_403');
}

/**
 * page callback sgmap_base_comment_ca_marche
 */
function sgmap_base_comment_ca_marche() {
  $path = drupal_get_path('module', 'sgmap_base');
  drupal_add_js($path . '/js/sgmap.comment_ca_marche.js');
  return array('#theme' => 'sgmap_base_comment_ca_marche_theme');
}

/**
 * page callback sgmap_base_fds
 */
function sgmap_base_fds() {
  return array('#theme' => 'sgmap_base_fds_theme');
}

/**
 * Implements hook_preprocess_page().
 */
function sgmap_base_preprocess_page(&$vars) {
  $notitle = array('comment-ca-marche', 'fabrique-solution');
  // hide title on front_page
  if ($vars['is_front'] || in_array(arg(0), $notitle)) {
    $vars['title'] = '';
  }
}

/**
 * Implements hook_block_info().
 */
function sgmap_base_block_info() {
  $blocks['sgmap_base_footer_nav'] = array(
    'info' => t('SGMAP - Navigation pied de page'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks['sgmap_base_header_top_navigation'] = array(
    'info' => t('Sgmap - Navigation header'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks['sgmap_base_top_fixed_navigation'] = array(
    'info' => t('Sgmap - Top fixed navigation'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks['sgmap_base_comment_ca_marche'] = array(
    'info' => t('Sgmap - comment ca marche'),
    'cache' => DRUPAL_NO_CACHE
  );
  $blocks['sgmap_base_eregie'] = array(
    'info' => t('SGMAP e-regie block'),
    'cache' => DRUPAL_CACHE_GLOBAL,
    'region' => 'footer_top',
    'weight' => 10
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function sgmap_base_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'sgmap_base_footer_nav':
      $block['content'] = sgmap_base_footer_nav_content();
      break;
    case 'sgmap_base_header_top_navigation':
      $block['subject'] = '';
      $block['content'] = _sgmap_base_header_top_navigation_content();
      break;
    case 'sgmap_base_top_fixed_navigation':
      $block['subject'] = '';
      $block['content'] = _sgmap_base_top_fixed_navigation_content();
      break;
    case 'sgmap_base_comment_ca_marche':
      $block['subject'] = '';
      $block['content'] = theme('sgmap_base_comment_ca_marche_footer');
      break;
    case 'sgmap_base_eregie':
      $block['subject'] = '';
      $block['content'] = theme('sgmap_base_eregie');
      break;
  }
  return $block;
}
/**
 * Implements hook_menu_alter().
 */
function sgmap_base_menu_alter(&$items) {
  $items['nodequeue/autocomplete']['page callback'] = 'sgmap_base_nodequeue_autocomplete';

  // disable rss
  $items['rss.xml']['page callback'] = 'drupal_not_found';
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function sgmap_base_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if ($router_item['map'][0] == 'comment') {
    if (arg(2) == 'edit') {
      $comment = $router_item['page_arguments'][0];
      $nid = $comment->nid;
      $comment_id = $comment->cid;
    }
    if (arg(2) == 'delete') {
      $comment_id = $router_item['map'][1];
      $comment = comment_load($comment_id);
      $nid = $comment->nid;
    }
    $data['tabs'][0]['output'][0] = array(
      '#theme' => 'menu_local_task',
      '#link' => array(
        'title' => 'Voir le commentaire',
        'href' => 'node/' . $nid,
        'localized_options' => array(
          'fragment' => 'comment-' . $comment_id,
        ),
      ),
    );
  }
}

/**
 * Page callback for autocomplete.
 * hacking nodequeue autocomplete
 */
function sgmap_base_nodequeue_autocomplete() {
  $args = func_get_args();
  $sqid = array_shift($args);
  $string = implode('/', $args);
  $matches = _sgmap_base_nodequeue_autocomplete($sqid, $string);
  drupal_json_output(drupal_map_assoc($matches));
}

function _sgmap_base_nodequeue_autocomplete($sqid, $string) {
  $output = array();

  if (!is_numeric($sqid) || !$string) {
    return $output;
  }

  $subqueue = nodequeue_load_subqueue($sqid);
  if (!$subqueue) {
    return $output;
  }

  $queue = nodequeue_load($subqueue->qid);
  if (!$queue) {
    return $output;
  }

  $nodes = sgmap_base_nodequeue_api_autocomplete($queue, $subqueue, $string);
  return $nodes;
}

function sgmap_base_nodequeue_api_autocomplete($queue, $subqueue, $string) {
  $matches = array();
  if (empty($string)) {
    return $matches;
  }

  $query = db_select('node', 'n')
    ->addTag('node_access')
    ->fields('n', array('nid', 'tnid', 'title', 'type'))
    ->condition('n.status', 1)
    ->range(0, variable_get('nodequeue_autocomplete_limit', 10));

  if (!empty($queue->types)) {
    $query->condition('n.type', $queue->types, 'IN');
  }

  $where_args = array();
  global $user;

  // Run a match to see if they're specifying by nid.
  $preg_matches = array();
  $match = preg_match('/\[nid: (\d+)\]/', $string, $preg_matches);
  if (!$match) {
    $match = preg_match('/^nid: (\d+)/', $string, $preg_matches);
  }

  if ($match) {
    // If it found a nid via specification, reduce our resultset to just that nid.
    $query->condition('n.nid', $preg_matches[1]);
  }
  else {
    // Build the constant parts of the query.
    $query->where('LOWER(n.title) LIKE LOWER(:string)', array(':string' => '%' . db_like($string) . '%'));
  }

  // Call to the API.
  $function = $queue->owner . "_nodequeue_autocomplete";
  if (function_exists($function)) {
    return $function($queue, $subqueue, $string, $where, $where_args);
  }
  else {
    $query->addTag('i18n_select');
    $result = $query->execute();

    foreach ($result as $node) {
      $id = nodequeue_get_content_id($queue, $node);
      $node_loaded = node_load($node->nid);
      if (!$node_loaded->field_proposition_thematique[LANGUAGE_NONE][0]['tid']) {
        $matches[$node->nid] = check_plain($node->title) . " [nid: $id]";
      }
    }
  }

  return $matches;
}

/**
 * Implements hook_theme_registry_alter().
 */
function sgmap_base_theme_registry_alter(&$theme_registry) {
  $theme_registry['file_link']['function'] = '_sgmap_file_link';

  // unset default user_picture preprocess function
  foreach($theme_registry['user_picture']['preprocess functions'] as $key => $function) {
    if($function == "template_preprocess_user_picture") {
      unset($theme_registry['user_picture']['preprocess functions'][$key]);
    }
  };

  if (isset($theme_registry['field_multiple_value_form'])) {
    $theme_registry['field_multiple_value_form']['type'] = 'module';
    $theme_registry['field_multiple_value_form']['theme path'] = drupal_get_path('module', 'sgmap_base');
    $theme_registry['field_multiple_value_form']['function'] = 'sgmap_base_theme_field_multiple_value_form';
  }
}

function _sgmap_file_link($variables) {
  global $theme;

  $file = $variables['file'];

  $url = file_create_url($file->uri);

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // Open in new window
  $options['attributes']['target'] = '_blank';

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
  }

  $file_format = strtoupper(pathinfo($file->filename, PATHINFO_EXTENSION));
  $link_text .= ' (<abbr>' . $file_format . '</abbr>, ' . format_size($file->filesize, 'fr') . ')';

  $options['html'] = TRUE;

  return l('<i class="icon-sgmap-download"></i> ' . $link_text, $url, $options);
}

function sgmap_base_preprocess_user_picture(&$variables) {
  $variables['user_picture'] = '';
  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if(!$account->picture) {
      $account = user_load($account->uid);
    }
    if (!empty($account->picture)) {
      if (is_numeric($account->picture)) {
        $account->picture = file_load($account->picture);
      }
      if (!empty($account->picture->uri)) {
        $filepath = $account->picture->uri;
      }
    }
    elseif (variable_get('user_picture_default', '')) {
      $filepath = variable_get('user_picture_default', '');
    }
    if (isset($filepath)) {
      $alt = t("@user's picture", array('@user' => format_username($account)));
      if (module_exists('image') && file_valid_uri($filepath)) {
        $style = (isset($variables['style_name'])) ? $variables['style_name'] : variable_get('user_picture_style', '');
        $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      else {
        $variables['user_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      if (!empty($account->uid) && user_access('access user profiles')) {
        $attributes = array(
          'attributes' => array('title' => t('View user profile.')),
          'html' => TRUE,
        );
        $variables['user_picture'] = l($variables['user_picture'], "user/$account->uid", $attributes);
      }
    }
  }
}

/**
 * Theme function override for multiple-value form widgets.
 * add possibility to avoid drag an drop on multiple-value form
 *
 * @see theme_field_multiple_value_form()
 */
function sgmap_base_theme_field_multiple_value_form($variables) {
  $element = $variables['element'];
  $output = '';

  // The first condition is the override.
  if (($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED) && isset($element[0]['#nodrag'])) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';
    $header = array(
      array(
        'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
        'class' => array('field-label'),
      ),
    );
    $rows = array();
    // Sort items according to weight
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
      }
      else {
        $items[] = &$element[$key];
      }
    }
    usort($items, '_field_sort_items_value_helper');
    // Add the items as table rows.
    foreach ($items as $key => $item) {
      // We don't want the weight to render.
      unset($item['_weight']);
      $cells = array(
        drupal_render($item),
      );
      $rows[] = array(
        'data' => $cells,
      );
    }

    $output = '<div class="form-item">';
    $output .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => $table_id,
        'class' => array('field-multiple-table'),
      ),
    ));
    $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
    $output .= '<div class="clearfix">' . drupal_render($add_more_button) . '</div>';
    $output .= '</div>';
  }
  elseif ($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $order_class = $element['#field_name'] . '-delta-order';
    $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';

    $header = array(
      array(
        'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
        'colspan' => 2,
        'class' => array('field-label'),
      ),
      t('Order'),
    );
    $rows = array();

    // Sort items according to '_weight' (needed when the form comes back after
    // preview or failed validation)
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
      }
      else {
        $items[] = &$element[$key];
      }
    }
    usort($items, '_field_sort_items_value_helper');

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      $item['_weight']['#attributes']['class'] = array($order_class);
      $delta_element = drupal_render($item['_weight']);
      $cells = array(
        array(
          'data' => '',
          'class' => array('field-multiple-drag'),
        ),
        drupal_render($item),
        array(
          'data' => $delta_element,
          'class' => array('delta-order'),
        ),
      );
      $rows[] = array(
        'data' => $cells,
        'class' => array('draggable'),
      );
    }

    $output = '<div class="form-item">';
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id, 'class' => array('field-multiple-table'))));
    $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
    $output .= '<div class="clearfix">' . drupal_render($add_more_button) . '</div>';
    $output .= '</div>';

    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }

  return $output;
}

function sgmap_base_footer_nav_content() {

  $main_menu_tree              = menu_tree('main-menu');
  // remove the first element of the main menu
  array_splice($main_menu_tree, 0, 1);
  $notre_demarche_menu_tree    = menu_tree('menu-notre-demarche');
  $les_campagnes_menu_tree     = menu_tree('menu-les-campagnes');
  $liens_bas_de_page_menu_tree = menu_tree('menu-liens-bas-de-page');

  _sgmap_base_update_menu_items_theme($main_menu_tree, 'menu_link__footer_menu');
  _sgmap_base_update_menu_items_theme($notre_demarche_menu_tree, 'menu_link__footer_menu');
  _sgmap_base_update_menu_items_theme($les_campagnes_menu_tree, 'menu_link__footer_menu');
  _sgmap_base_update_menu_items_theme($liens_bas_de_page_menu_tree, 'menu_link__footer_menu');

  $render_array = array(
    '#prefix'           => '<div id="footer-nav">',
    '#suffix'           => '</div>',
    'main_menu'         => $main_menu_tree,
    'notre_demarche'    => $notre_demarche_menu_tree,
    'les_campagnes'     => $les_campagnes_menu_tree,
    'liens_bas_de_page' => $liens_bas_de_page_menu_tree,
  );


  return render($render_array);
}

function _sgmap_base_update_menu_items_theme(&$menu_tree, $theme) {
  foreach ($menu_tree as $key => $menu_tree_item) {
    if(isset($menu_tree_item['#theme'])) {
      $menu_tree_item['#theme'] = 'menu_link__footer_menu';
      $menu_tree[$key] = $menu_tree_item;
    }
  }
}

/**
 * Return complaint reasons.
 */
function sgmap_base_complaint_reason_alter(&$reasons) {

  $reasons = array(
    'Hors sujet',
    'Contraire à la charte d’engagement',
    'Raciste ou xénophobe',
    'Spam',
    'Autre'
  );
}

/**
 * Implements hook_node_view().
 */
function sgmap_base_node_view($node, $view_mode, $langcode) {
  if ($view_mode != 'full') {
    unset($node->content['links']['node']['#links']['node-readmore']);
    unset($node->content['links']['comment']);
  }
}

/*
 * Implements hook_preprocess_node()
 */
function sgmap_base_preprocess_node(&$vars) {
  $node = $vars['node'];

  $vars['theme_hook_suggestions'][] = 'node__'.$node->type.'__'.$vars['view_mode'];

  if (variable_get('node_submitted_' . $node->type, TRUE)) {
    if($vars['view_mode'] == 'full') {
      $vars['submitted'] = t('<strong>Écrit par !username</strong><br/><em>publié le !datetime</em>', array('!username' => $vars['name'], '!datetime' => format_date($node->created, 'short')));
    }
    if($vars['view_mode'] == 'teaser') {
      $vars['submitted'] = t('par !username', array('!username' => $vars['name'] ));
    }
  }

  $vars['user_picture'] = theme('user_picture', array(
    'account'    => $node,
    'style_name' => ($vars['view_mode'] == 'teaser') ? 'mini-profile_picture' : 'medium-profile_picture'
  ));

}

function sgmap_base_preprocess_comment(&$variables) {

  $comment = $variables['elements']['#comment'];
  $variables['picture'] = theme('user_picture', array(
    'account'    => $comment,
    'style_name' => 'mini-profile_picture'
  ));

  $variables['submitted'] = t('<strong>Écrit par !username</strong><br><em>Le !datetime</em>', array('!username' => $variables['author'], '!datetime' => format_date($comment->created, 'short')));
}

/**
 * Implements hook_preprocess_comment_wrapper
 */
function sgmap_base_preprocess_comment_wrapper(&$variables) {
  $user = $variables['user'];
  $user_loaded = user_load($user->uid);
  if (count($user_loaded->field_sgmap_public) == 0) {
    $variables['fc_user'] = TRUE;
  }
}

/**
 * header top navigation
 */
function _sgmap_base_header_top_navigation_content_build() {

  $render_array = array(
    'navigation'     => array(
      '#theme_wrappers' => array('menu_tree__top_navigation')
    )
  );

  $notre_demarche_menu_tree = _sgmap_base_clean_menu_tree(menu_tree('menu-notre-demarche'));

  $render_array['navigation']['notre-demarche'] = $notre_demarche_menu_tree;

  global $user;

  if(user_is_anonymous()) {
    $render_array['navigation']['user-login'] = array(
      '#theme' => 'menu_link',
      '#title' => '<i class="icon-sgmap-particulier"></i> <span class="log_txt" >Connexion</span> <span class="caret"></span>',
      '#href' => 'user/login',
      '#attributes' => array(
      ),
      '#localized_options' => array(
        'html'       => TRUE,
        'attributes' => array(
          'title'         => '',
          'aria-expanded' => "false",
          'aria-controls' => "sgmap-user-login-block",
          'data-target'   => '#',
          'role'          => "button",
          'id'            => array('btn-login-form-toggle'),
          'class'         => array('btn-login-form-toggle'),
        )
      )
    );
    $render_array['user_login_box'] = array(
      '#prefix' => '<div id="sgmap-user-login-block"><div class="container"><div class="content">',
      '#suffix' => '</div></div></div>',
      'login'    => array(
        '#prefix' => '<div class="col col-login"><i class="icon-sgmap-particulier"></i><i class="icon-sgmap-entreprise"></i><i class="icon-sgmap-agent"></i><p>'. t('Vous êtes un particulier,<br /> une entreprise, un agent ?') .'</p>',
        '#markup' => l(t('je crée mon compte'), 'user/register', array('attributes' => array('class' => array('btn', 'btn-large', 'btn-register'))))
      ),
      'france_connect' => array(
        'form'    => drupal_get_form('sgmap_fc_login_form'),
        '#suffix' => '</div>',
      ),
      'register' => array(
        '#prefix' => '<div class="col col-register"><p>'. t("J'ai déjà un compte") .'</p>',
        'form'    => drupal_get_form('user_login_block'),
        '#suffix' => '</div>',
      ),
    );
  } else {
    $render_array['navigation']['user-status'] = array(
      'menu' => array(
        '#theme' => 'menu_link',
        '#title' => '<i class="icon-sgmap-particulier"></i> <span class="log_txt" >'.$user->name.'</span>',
        '#href' => 'user/',
        '#attributes' => array(
        ),
        '#localized_options' => array(
          'html'       => TRUE,
          'attributes' => array(
            'title' => '',
            'class' => array('user-profile-link')
          )
        ),
        '#below' => array(
          'dashboard' => array(
            '#theme' => 'menu_link',
            '#title' => t('Mon tableau de bord'),
            '#href' => 'user/',
            '#attributes' => array(
            ),
            '#localized_options' => array(
              'attributes' => array()
            )
          ),
          'settings' => array(
            '#theme' => 'menu_link',
            '#title' => t('Editer mon profil'),
            '#href' => 'user/'.$user->uid.'/edit',
            '#attributes' => array(
            ),
            '#localized_options' => array(
              'attributes' => array()
            )
          ),
          'logout' => array(
            '#theme' => 'menu_link',
            '#title' => t('Se déconnecter'),
            '#href' => 'user/logout',
            '#attributes' => array(
            ),
            '#localized_options' => array(
              'attributes' => array()
            )
          ),
        )
      )
    );
  }

  return $render_array;
}

function _sgmap_base_header_top_navigation_content() {
  $render_array = _sgmap_base_header_top_navigation_content_build();

  return render($render_array);
}

function _sgmap_base_top_fixed_navigation_content() {
  $header_top_navigation_content = _sgmap_base_header_top_navigation_content_build();
  if(user_is_anonymous()) {
    $header_top_navigation_content['navigation']['user-login']['#localized_options']['attributes']['id'] = 'btn-login-form-toggle-fixed';
    $header_top_navigation_content['navigation']['user-login']['#href'] = '';
    unset($header_top_navigation_content['navigation']['user-login']['#localized_options']['attributes']['aria-expanded']);
    unset($header_top_navigation_content['navigation']['user-login']['#localized_options']['attributes']['aria-controls']);
    unset($header_top_navigation_content['navigation']['user-login']['#localized_options']['attributes']['data-target']);
  }

  $navigation = $header_top_navigation_content['navigation'];
  $navigation['#theme_wrappers'] = array('menu_tree__mavigation_top_fixed');

  $main_menu_tree = menu_tree('main-menu');
  $main_menu_tree['#theme_wrappers'] = array('menu_tree__main_menu_top_fixed');
  foreach($main_menu_tree as $key => &$element) {
    if(is_numeric($key)) {
      $element['#theme'] = 'menu_link__main_menu_top_fixed';
    }
  }

  $top_fixed_navigation_content = array(
    '#theme_wrappers' => array('container'),
    '#attributes'     => array(
      'id'    => 'float-header-menu',
      'class' => array(
        'clearfix',
        'hide'
      )
    ),
    'content' => array(
      '#theme_wrappers' => array('container'),
      '#attributes'     => array('class' => 'container'),
      'logo'            => array(
        '#theme_wrappers' => array('container'),
        '#attributes'     => array('class' => array('logo', 'pull-left')),
        'link'            => array(
          '#theme' => 'link',
          '#path'  => '<front>',
          '#text'  => theme_image(array(
            'path'   => 'public://fiaresimple_logo-21.png',
            'alt'    => '',
            'width'  => '',
            'height' => '',
          )),
          '#options' => array(
            'attributes' => array('class' => array('brand')),
            'html'       => TRUE,
          )
        )
      ),
      'main_menu'          => $main_menu_tree,
      'navigation'         => $navigation,
      'search_block_form'  => drupal_get_form('search_block_form'),
      'mobile_search_link' => array(
        '#theme' => 'link',
        '#text' => '<i class="icon-search"></i><span class="element-invisible">Recherche</span>',
        '#path' => 'search/site',
        '#options' => array(
          'attributes' => array('class' => array('link-search', 'visible-phone', 'pull-left')),
          'html'       => TRUE
        )
      )
    )
  );

  return $top_fixed_navigation_content;
}

function _sgmap_base_clean_menu_tree($menu_tree) {
  unset($menu_tree['#theme_wrappers']);
  unset($menu_tree['#sorted']);
  foreach($menu_tree as $element_key => $element) {
    foreach ($element['#attributes']['class'] as $class_key => $class) {
      if (in_array($class, array('last', 'first'))) {
        unset($menu_tree[$element_key]['#attributes']['class'][$class_key]);
      }
    }
  }

  return $menu_tree;
}

function sgmap_base_form_search_form_alter(&$form, &$form_state) {
  unset($form['#suffix']);
}

function sgmap_base_form_alter(&$form, &$form_state, $form_id) {
  if (path_is_admin(current_path()) && $form_id != 'user_profile_form') {
    return;
  }

  $form['required'] = array(
    '#prefix' => '<div class="required">',
    '#suffix' => '</div>',
    '#markup' => '*champ obligatoire',
    '#weight' => 9,
  );
  $form_not_required = array(
    'search_block_form',
    'sgmap_campagne_proposition_search_form',
    'sgmap_proposition_filter_form',
    'comment_abuse_complaint_form',
    'user_register_form',
    'search_form',
    'user_pass_reset',
    'user_pass',
    'sgmap_notification_user_notification_settings_form',
    'og_ui_confirm_subscribe',
    'og_ui_confirm_unsubscribe',
    'sgmap_group_content_filter_form',
    'comment_confirm_delete',
    'poll_view_voting',
    'sgmap_group_swot_radar_choices_form',
    'sgmap_campagne_list_filter_form',
    'sgmap_fc_login_form'
  );

  if(in_array($form_id, $form_not_required)) {
    unset($form['required']);
  }

  if($form_id == 'sgmap_proposition_save_proposal_form') {
    $form['step1']['required'] = $form['required'];
    unset($form['required']);
  }
}

/**
 * Implements hook_block_view_alter().
 */
function sgmap_base_block_view_alter(&$data, $block) {
	global $user;

	if($block->delta == 'user-menu' && $user->uid) {
		$data['subject'] = $user->name;
	}
}

/**
 * get_vocabulary_terms_choices
 */
function sgmap_base_get_vocabulary_terms_choices($vocabulary_name, $empty_choice = false, $parent = 0, $max_depth = 1) {
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);
  $values = array();

  // empty choice can be false string or boolean
  if($empty_choice) {
    if(is_array($empty_choice)) {
      $values[key($empty_choice)] = current($empty_choice);
    } else {
      $values[] = is_string($empty_choice) ? $empty_choice : '';
    }
  }

  if (!$vocabulary) {
    return $values;
  }

  $terms = taxonomy_get_tree($vocabulary->vid, $parent, $max_depth);


  foreach($terms as $term) {
    $values[$term->tid] = $term->name;
  }

  if($empty_choice && count($values) <= 1) {
    $values = array();
  }

  return $values;
}

/**
 * Implements hook_theme().
 */
function sgmap_base_theme($existing, $type, $theme, $path) {
  return array(
    'sgmap_404' => array(
      'template'  => 'sgmap-404',
      'path'      => $path .'/themes',
    ),
    'sgmap_403' => array(
      'template'  => 'sgmap-403',
      'path'      => $path .'/themes',
    ),
    'sgmap_base_comment_ca_marche_theme' => array(
      'template'  => 'sgmap-comment-ca-marche-theme',
      'path'      => $path .'/themes',
    ),
    'sgmap_base_comment_ca_marche_footer' => array(
      'template'  => 'sgmap-comment-ca-marche-footer',
      'path'      => $path .'/themes',
    ),
    'sgmap_base_fds_theme' => array(
      'template'  => 'sgmap-base-fds-theme',
      'path'      => $path .'/themes',
    ),
    'sgmap_base_eregie' => array(
      'template'  => 'sgmap-base-eregie',
      'path'      => $path .'/themes',
    ),
  );
}


/**
 * Implements hook_preprocess_block()
 */
function sgmap_base_preprocess_block(&$vars) {

  /* Set shortcut variables. Hooray for less typing! */
  $block_id = $vars['block']->module . '-' . $vars['block']->delta;

  $classes = &$vars['classes_array'];
  // $title_classes = &$vars['title_attributes_array']['class'];
  // $content_classes = &$vars['content_attributes_array']['class'];

  /* Add classes based on the block delta. */
  switch ($block_id) {
    case 'sgmap_base-sgmap_base_eregie':
        $classes[] = 'span12';
      break;
    case 'user-login':
      $block = &$vars['block'];
      $block->subject = null;
      break;
  }
}

/**
 * Implements hook_form_alter().
 */
function sgmap_base_form_user_login_block_alter(&$form, &$form_state) {
  unset($form['links']);

  $form['name']['#title'] = 'Email';
  $form['name']['#attributes']['placeholder'] = 'Entrez votre email';
  $form['pass']['#attributes']['placeholder'] = 'Entrez votre mot de passe';
  $form['actions']['submit']['#value'] = 'Je me connecte';
  $form['actions']['submit']['#weight'] = 100;
  $form['actions']['submit']['#weight'] = 100;
  $form['actions']['submit']['#attributes']['class'][] = 'btn-block';
  $form['actions']['submit']['#attributes']['class'][] = 'btn-large';
  $form['actions']['submit']['#attributes']['class'][] = 'btn-bleu';

  $form['actions']['reset_password'] = array(
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => l(
      'Mot de passe oublié ?',
      'user/password',
      array(
        'attributes' => array(
          'class' => array('reset-password-link')
        )
      )
    )
  );
}

function sgmap_base_get_public_choices() {
    return array(
        'a' => 'agents',
        'p' => 'particuliers',
        'e' => 'entreprises'
    );
}

function sgmap_base_load_infinite_scroll() {
  drupal_add_js('libraries/infinite-scroll/jquery.infinitescroll.min.js');

  $path = drupal_get_path('module', 'sgmap_base');
  drupal_add_js($path . '/js/' . 'sgmap.infinitescroll.js');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sgmap_base_form_contact_site_form_alter(&$form, &$form_state) {
//var_dump($form['cid']);
  $form['cid']['#default_value'] = 1;
  $form['cid']['#prefix'] = '<div class="hide">';
  $form['cid']['#suffix'] = '</div>';

  $form['prenom'] = array(
    '#title'    => t('Votre prénom'),
    '#type'     => 'textfield',
    '#required' => TRUE,
    '#weight'   => 0,
  );
  $form['annuler'] = array(
    '#value'      => t('Annuler'),
    '#type'       => 'button',
    '#weight'     => 10,
    '#attributes' => array('class' => array('reset-form')),
  );
}


/**
 * Implements hook_comment_abuse().
 * BECAUSE DRUPAL DEV SUXX!!
 */
function sgmap_base_comment_abuse($abuse) {
  $abuse = get_object_vars($abuse);
  // Send email about complaint on comment.
  if (variable_get('comment_abuse_email_notification', FALSE)) {
    global $language;
    $emails = explode(',', variable_get('comment_abuse_email_list', ''));
    foreach ($emails as $email) {
      drupal_mail('sgmap_base', 'abuse', $email, $language->language, $abuse);
    }
  }
}

/**
 * Implements hook_mail().
 */
function sgmap_base_mail($key, &$message, $abuse) {
  if ($key == 'abuse') {
    $langcode = $message['language']['language'];
    $reasons = comment_abuse_get_complaint_reasons();
    $message['subject'] = t('Comment abuse notification.', array('langcode' => $langcode));
    $message['body'][] = t("On your site came a new complaint on a comment.\n
      Reason : !reason \n
      Message: !message \n
      Page url: !node_url \n
      Moderation page: !moderation_url",
      array(
        '!reason' => $reasons[$abuse['reason_id']],
        '!message' => $abuse['message'],
        '!node_url' => url("node/$abuse[nid]", array('absolute' => TRUE)),
        '!moderation_url' => url('admin/content/comment-abuse/list', array('absolute' => TRUE)),
      ),
      array('langcode' => $langcode));
  }
}

/**
 * Preprocess function for widget template file. Provides variables for default widget.
 * @param  $variables
 * @return void
 */
function sgmap_base_preprocess_plus1_widget(&$variables) {
  $entity_type = $variables['entity_type'];
  $entity_id = $variables['entity_id'];
  $tag = $variables['tag'];
  $voted = $variables['voted'];
  $logged_in = $variables['logged_in'];
  $score = $variables['score'];
  $vote_link = $variables['vote_link'];
  $undo_vote_link = $variables['undo_vote_link'];
  $link_query = $variables['link_query'];
  $can_vote = $variables['can_vote'];
  $can_undo_vote = $variables['can_undo_vote'];
  $vote_text = '<i class="icon-heart-empty"></i> '.html_entity_decode($variables['vote_text']);
  $voted_text = '<i class="icon-heart"></i> '.html_entity_decode($variables['voted_text']);


  $base_class = array('plus1-link');
  drupal_alter('sgmap_base_plus1', $base_class, $entity_id);

  if (!$logged_in && !$can_vote) {
    //$variables['widget_message'] =  l(t('Log in<br />to vote'), 'user', array('html' => TRUE));
    $variables['widget_message'] = l($vote_text, $vote_link,
      array(
        'query' => $link_query,
        'attributes' => array(
          'class' => $base_class
        ),
        'html' => TRUE
      )
    );
  }
  elseif ($variables['voted']) { // User already voted.
    // is the user can undo his vote then provide him with link
    if ($can_undo_vote) {
        // if we don't have text for undo action, add "arrow-down" link after score.
        $base_class[] = 'active';
        $variables['use_arrow_down'] = TRUE;
        $variables['widget_message'] = l($voted_text, $undo_vote_link, array('query' => $link_query, 'attributes' => array('class' => $base_class),'html' => TRUE));
    }
    else {
      $variables['widget_message'] = $voted_text;
    }
  }
  elseif ($can_vote) {
    // User is eligible to vote.
    $variables['widget_message'] = l($vote_text, $vote_link,
      array(
        'query' => $link_query,
        'attributes' => array(
          'class' => $base_class
        ),
        'html' => TRUE
      )
    );
  }
}
