<div id="fds-box" class="container">
	<div class="row">
		<div class="span12">
			<!-- <h1 class="page-header"><span class="fds-label">Prochainement</span></h1> -->
			<div class="fds-header">                          
				<div class="row"> 
					<div class="span7 fds-txt">
						Agents du service public et usagers se rencontrent et 
						créent ensemble des solutions pour simplifier la vie et les 
						démarches de tous.
					</div>
					<!-- <div class="span5">
						<div class="info-date-box pull-right">
							<i class="icon-calendar"></i>Ouverture de la fabrique de solution :
							<span>janvier 2014.</span>
						</div>
						
					</div> -->
				</div>
			</div>
			<div class="fds-content">                          
				<div class="fds-top">
					<h3>Comment ça marche ?</h3>
					<p>Un outil collaboratif en ligne pour construire des solutions de simplification 
					à partir de vos propositions, en tenant compte de toutes les parties prenantes.</p>
				</div>

				<div class="fds-line fds-line-1">
					<div class="fds-number">1</div>
					<div class="fds-line-txt">
						Proposez vos idées pour simplifier vos démarches
						en ligne sur <a class="btn btn-red btn-mini" href="<?php print url('vos-idees') ?>">Toutes vos idées</a>
					</div>
				</div>
				
				<div class="fds-line fds-line-2">
					<div class="fds-number">2</div>
					<div class="fds-line-txt">
						Participez aux groupes de réflexion en ligne entre usagers, 
						experts et agents du service public.
					</div>
				</div>

				<div class="fds-line fds-line-3">
					<div class="fds-number">3</div>
					<div class="fds-line-txt">
						Échangez, argumentez et construisez ensemble des solutions grâce à 
						des outils simples : questionnaires, forums, commentaires, recommandations...
					</div>
				</div>

				<div class="fds-line fds-line-4">
					<div class="fds-number">4</div>
					<div class="fds-line-txt">
						Partagez vos conclusions et faItes part de vos 
						recommandations !
					</div>
				</div>
			</div>
			<div class="fds-footer">                          
				<div class="row"> 
					<div class="span7 fds-txt">
						Les contributeurs les plus actifs et leurs idées seront mis en avant sur la 
						plateforme et dans les démarches de co-design engagées par le SGMAP.
					</div>
					<div class="span5">
						<div class="btn-box">
							<p>Construisons ensemble l'espace<br />
							" Fabrique de solutions " !</p>
							<a class="btn btn-red btn-large" href="<?php print url('node/780') ?>"><i class="icon-sgmap-ampoule"></i> Je propose</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>