<div class="error404">
	<div id="msg404" >
		<div class="errorNumber"><?php print t('40<span>4</span>'); ?></div>
		<i class="icon-search"></i>
		<h3><?php print t('Hum...'); ?></h3>
		<div class="header-404">
			<?php print t("La page que tentez d'afficher n'existe pas ou a été déplacée."); ?>
		</div>
		<div class="body-404">
			<?php print t("Nous vous invitons à retourner sur la page d'accueil ou à effectuer 
			une recherche sur le site afin de trouver le contenu que vous souhaitez consulter."); ?>
			<a href="/"><?php print t("Retour à l'accueil"); ?></a>
		</div>

	</div>
</div>
