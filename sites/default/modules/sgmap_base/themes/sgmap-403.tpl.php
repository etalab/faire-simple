<div class="error403">
    <div id="msg403" >
        <div class="errorNumber"><?php print t('403'); ?></div>
        <i class="icon-minus-sign "></i>
        <h3><?php print t('Accès refusé !'); ?></h3>
        <div class="header-404">
            <?php print t("Vous n'êtes pas autorisé(e) à accéder à cette page."); ?>
        </div>
    </div>
    <div id="action-403">
        <strong>Plusieurs raisons possibles :</strong>
        <p><i class="icon-unlock-alt"></i> Vous n'êtes pas connecté(e) :</p>
        <div class="btn-box">
            <a href="<?php print url('user/login')?>" class="btn btn-bleu btn-large"><?php print t("Je me connecte"); ?></a>
            <a href="<?php print url('user/register')?>" class="btn btn-large"><?php print t("Créer votre compte"); ?></a>
        </div>
        <p><i class="icon-sgmap-agent"></i> Cette page est réservé(e) aux personnes ayant le statut Agent Public.</p>
        <div class="btn-box">
            <a href="<?php print($base_url); ?>"><?php print t("Retour à l'accueil"); ?></a>
        </div>
    </div>
</div>