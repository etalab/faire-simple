<div id="htw-box" class="container">
	<div class="row">
		<div class="span12">
			<h1 class="page-header">Comment ça marche ?</h1>
			<div class="htw-header">                          
				<div class="row"> 
					<div class="span7 htw-txt">
						<h2>faire simple</h2>
						est une plateforme collaborative à l'initiative du secrétariat général pour la modernisation 
						de l'action publique (SGMAP).
						L'objectif de ce site est de simplifier les démarches et moderniser l'action publique.<br /><br />
						Cette plateforme est composée de 3 espaces :

					</div>
					<div class="span5">
						<div class="graph-users">
							<div class="picto-box">
								<i class="icon-sgmap-particulier"></i>
								<i class="icon-sgmap-entreprise"></i>
								<i class="icon-sgmap-agent"></i>
							</div>
							Lieu de réflexion, d’échange et de dialogue, 
							Faire simple est une initiative ouverte à tous : 
							particuliers, entreprises, agents des services publics.
						</div>
					</div>
				</div>
			</div>
			<div class="htw-content">                          
				<div class="line line-1">
					<div class="line-number">1</div>
					<h3>Les sujets<br />du moment</h3>
					<div class="line-img-box">

					</div>
					<div class="graph-box graph-idee">
						Un espace où chacun peut <span class="red">proposer ses idées</span> pour simplifier les démarches et les services publics en général.
						Chacun peut également <span class="red">commenter</span> ou <span class="red">soutenir une idée</span> directement en ligne. 
						<div class="btn-box">
							<a class="btn btn-large btn-red" href="<?php echo url('les-sujets-du-moment') ?>"><i class="icon-sgmap-ampoule"></i>voir</a>
						</div>
					</div>
				</div>

				<div class="line line-2">
					<div class="line-number">2</div>
					<h3>La fabrique<br />de solution</h3>
					<div class="line-img-box">
						
					</div>
					<div class="graph-box graph-fabrique pull-right">
						Un espace où <span class="orange">agents du service public et usagers se rencontrent</span> et 
						créent ensemble de nouvelles possibilités, de nouvelles solutions pour simplifier la vie et les démarches. 
						<div class="btn-box">
							<a class="btn btn-large btn-orange" href="<?php echo url('fabrique-de-solutions') ?>"><i class="icon-sgmap-particulier"></i>voir</a>
						</div>
					</div>
				</div>

				<div class="line line-3">
					<div class="line-number">3</div>
					<h3>Mesures<br />engagées</h3>
					<div class="line-img-box">
						
					</div>
					<div class="graph-box graph-mesures">
						Un espace de <span class="purple">promotion</span> et de <span class="purple">suivi</span> pour permettre à tous d’apprécier 
						les <span class="purple">résultats</span> des échanges et du dialogue. 
						<div class="btn-box">
							<a class="btn btn-large btn-purple" href="http://simplification.modernisation.gouv.fr/"><i class="icon-sgmap-ok"></i>voir</a>
						</div>
					</div>
					<div class="log-box pull-right">
						<p>Créez un compte et partagez dès à présent vos  propositions de simplification</p>
						<a class="btn btn-large" href="<?php echo url('user/login') ?>">Créez votre compte</a>
						<p>Votre participation s’effectue dans le cadre de la charte de bonne conduite.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>