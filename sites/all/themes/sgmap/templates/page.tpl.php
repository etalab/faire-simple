<div id="wrapper">
  <div class="container" id="skip-nav-container">
      <ul id="skip-nav" class="nav nav-pills">
          <li><a href="#main-content"><?php print t('Skip to main content'); ?></a></li>
          <li><a href="#nav"><?php print 'Aller à la navigation principale'; ?></a></li>
          <li><a href="#sgmap-top-navigation"><?php print 'Aller à la navigation secondaire'; ?></a></li>
          <li><a href="#search-block-form"><?php print 'Aller à la recherche'; ?></a></li>
          <li><a href="#block-sgmap-base-sgmap-base-footer-nav"><?php print 'Liens complémentaires'; ?></a></li>
      </ul>
  </div>
  <header id="header" class="header-container" >
      <div class="top-box header-top">
        <div class="container">
          <span>Un site proposé par <a href="http://www.modernisation.gouv.fr/">modernisation.gouv.fr</a></span>
        </div>
      </div>
      <div id="header-top-nav">
        <?php print render($page['header_top']); ?>
      </div>
      <div class="container">
        <div id="site-name-and-slogan">
            <?php if(drupal_is_front_page()) : ?>
            <h1 id="site-name">
              <div class="logo">
                <img src="<?php print $logo; ?>" alt="" />
                <span><?php print $site_name; ?></span>
              </div>
            </h1>
            <?php else : ?>
            <div id="site-name">
              <div class="logo">
                <a href="<?php print $front_page; ?>" class="brand">
                  <img src="<?php print $logo; ?>" alt="" />
                  <span><?php print $site_name; ?></span>
                </a>
              </div>
            </div>
          <?php endif; ?>

          <div id="slogan">
            <?php print $site_slogan; ?>
          </div>
        </div>
      </div>
      
      <?php print render($page['header']); ?>

      <div class="container">
        <a id="nav"></a>
        <?php if (!empty($primary_nav)): ?>
          <a href="#" class="visible-phone link-menu-mobile">Menu <i class="icon-reorder"></i></a>
          <div id="nav-box" class="">
            <nav role="navigation">
              <?php if (!empty($primary_nav)): ?>
                <?php print render($primary_nav); ?>
              <?php endif; ?>
            </nav>
          </div>
        <?php endif; ?>
    </div>

    <?php if(!empty($page['header_bottom'])): ?>
      <div class="header-bottom">
        <?php print render($page['header_bottom']); ?>
      </div>
    <?php endif; ?>
  </header>


  <div class="main-container">

    <a id="main-content"></a>
    <div id="content">

      <div class="container">
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <?php print $messages; ?>
      </div>

      <?php if(!empty($page['content_top'])): ?>
        <?php print render($page['content_top']); ?>
      <?php endif; ?>

      <?php if(!$page['no_container']): ?>
      <div class="container">
        <div class="row">
          <div class="<?php print _bootstrap_content_span($columns); ?>">
      <?php endif; ?>

            <?php if($page['no_container']): ?>
            <div class="container">
            <?php endif; ?>
            <?php if (!empty($page['highlighted'])): ?>
              <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
            <?php endif; ?>
            <?php print render($title_prefix); ?>
            <?php if (!empty($title)): ?>
              <h1 class="page-header"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>
            <?php if (!empty($tabs)): ?>
              <?php print render($tabs); ?>
            <?php endif; ?>
            <?php if (!empty($page['help'])): ?>
              <div class="well"><?php print render($page['help']); ?></div>
            <?php endif; ?>
            <?php if (!empty($action_links)): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
            <?php if($page['no_container']): ?>
            </div>
            <?php endif; ?>

            <?php print render($page['content']); ?>
            <?php if(!empty($page['content_bottom'])): ?>
              <?php print render($page['content_bottom']); ?>
            <?php endif; ?>

      <?php if(!$page['no_container']): ?>
        </div>
        <?php if (!empty($page['sidebar_second'])): ?>
          <aside class="span3" role="complementary">
            <?php print render($page['sidebar_second']); ?>
          </aside>  <!-- /#sidebar-second -->
        <?php endif; ?>
        </div>
      </div>
      <?php endif; ?>

      <?php if(!empty($page['out_container_bottom'])): ?>
        <?php print render($page['out_container_bottom']); ?>
      <?php endif; ?>

      <?php if(!empty($page['out_container_bottom_second'])): ?>
        <?php print render($page['out_container_bottom_second']); ?>
      <?php endif; ?>

      <?php if(!empty($page['out_container_bottom_third'])): ?>
        <?php print render($page['out_container_bottom_third']); ?>
      <?php endif; ?>
    </div>
  </div>
  <footer class="footer footer-container">
      <?php if(!empty($page['footer_top'])): ?>
      <div class="footer-top">
        <div class="container">
          <div class="row">
            <?php if(!empty($page['footer_top'])): ?>
              <?php print render($page['footer_top']); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php endif; ?>

      <?php if (drupal_is_front_page()) : ?>
        <div class="footer-aftertop">
          <div class="container">
            <?php if(!empty($page['footer_aftertop'])): ?>
              <?php print render($page['footer_aftertop']); ?>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="footer-middle">
        <div class="container">
            <?php if(!empty($page['footer'])): ?>
              <?php print render($page['footer']); ?>
            <?php endif; ?>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <?php if(!empty($page['footer_bottom'])): ?>
            <?php print render($page['footer_bottom']); ?>
          <?php endif; ?>
        </div>
      </div>
  </footer>
  <?php if(!empty($page['out_footer'])): ?>
    <?php print render($page['out_footer']); ?>
  <?php endif; ?>
</div>
