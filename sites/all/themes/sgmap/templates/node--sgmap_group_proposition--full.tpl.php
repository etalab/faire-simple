<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header>
    <?php print render($title_prefix); ?>
      <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
    <?php print render($title_suffix); ?>
  </header>
  
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['swot_comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['plus1_widget']);
    hide($content['pcount_text']);
    if ($content['radar_sliders']) {
      hide($content['radar_sliders']);
      hide($content['radar_form']);
    }
    print render($content);
  ?>

  <span class="submitted">
    <?php print $user_picture; ?>
    <?php print $submitted; ?>
  </span>

  <!-- Participation count -->
  <?php if ($content['pcount_text']): ?>
    <div class="count-partipation">
      <span class="p-count"><?php print render($content['pcount_text']) ?></span>
    </div>
  <?php endif; ?>
  <!--End Participation count -->

  <!-- Nav tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#group-radar" data-toggle="tab" id="group-radar-nav" class="tab-link">&Eacute;valuation des membres de l'atelier</a></li>
    <li><a href="#personal-radar" data-toggle="tab" id="personal-radar-nav" class="tab-link">Ajouter mon évaluation</a></li>
  </ul>
  <!--End Nav tabs -->
  <!-- Tab panes -->
  <div class="tab-pane group-radar">
    <div class="clearfix">
      <div id="radar" class="svg-radar-box pull-left"></div>
      <div class="radar-info">
        <i class="icon-sgmap-faq"></i><?php print t("Ce graphique vous permet d'avoir un aperçu schématique des forces et faiblesses de la proposition. Vous êtes invité à donner votre estimation et celle-ci sera comptabilisée dans la moyenne générale.") ?>
      </div>
    </div>
    <h3>Tous les commentaires sur :</h3>
    <?php print render($content['swot_comments']); ?>
  </div>

  <div class="tab-pane personal-radar">
    <div id="edit-radar-block" class="clearfix">
      <p>Participez en évaluant la proposition selon les critères proposés. Après la validation votre estimation sera prise en compte dans la moyenne de l'atelier.</p>
      <div id="edit-radar" class="svg-radar-box"></div>
      <?php print render($content['radar_form']); ?>
    </div>
  </div>
  <!-- End Tab panes -->


</article> <!-- /.node -->
