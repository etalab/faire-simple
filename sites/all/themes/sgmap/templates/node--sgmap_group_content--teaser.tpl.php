<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header>
    <div class="typepost"><i class="<?php print $type_icon ?>"></i> <?php print $type_name ?></div>
    <h2<?php print $title_attributes; ?>><?php print render($content['node-tile-link']) ?></h2>
  </header>
  <?php
    hide($content['plus1_widget']);
    print render($content);
    if(!isset($comment_count)){
      $comment_count = 0;
    }
  ?>
  <footer>
    <div class="footerBox">
      <span class="user-picture"><?php print $user_picture; ?></span>
      <span class="comment_count_wrapper"><i class="icon-sgmap-comment "></i> <span class="comment_count"><?php print $comment_count;?></span></span>
      <span class="plus_count_wrapper"><i class="icon-heart"></i> <span class="plus_count"><?php print $content['plus1_widget']['#score']; ?></span></span>
    </div>
  </footer>
  <?php if ($node->is_admin_content): ?>
    <span class="picto_admin"></span>
  <?php endif?>
</article> <!-- /.node -->
