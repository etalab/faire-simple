<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row">  
    <div class="span8">
      <h1><?php print $title ?></h1>
      <span class="members"><i class="icon-sgmap-particulier"></i> <?php print render($content['member_count']) ?></span>
      <?php if ($content['duration']): ?>
        <span class="duration"><i class="icon-sgmap-clock"></i> <?php print render($content['duration']) ?></span>
      <?php endif; ?>
    </div>
    <div class="action-group-box span4 pull-right">
      <?php if ($content['private_info']): ?>
        <?php print render($content['private_info']) ?>
      <?php endif; ?>

      <?php if ($content['unsubscribe']): ?>
        <a href='#' class='btn btn-orange btn-large' data-toggle="modal" data-target="#modal-body">
          <i class="icon-target"></i> Les objectifs de l'atelier
        </a>
      <?php endif ?>

      <?php if ($content['subscribe']): ?>
        <?php print render($content['subscribe']) ?>
      <?php endif ?>

      <?php if ($content['pending_subscription']): ?>
        <div class="pending-subscription alert alert-info">
          <?php print render($content['pending_subscription']) ?>
        </div>
      <?php endif ?>

      <?php if ($content['unsubscribe']): ?>
        <?php print render($content['unsubscribe']) ?>
      <?php endif ?>

    </div>
  </div>
  <!-- user is group member -->
  <?php if ($content['unsubscribe']): ?>
    <div id='modal-body' class="modal hide">
      <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
        <h3><i class="icon-target"></i> Les objectifs de l'atelier</h3>
      </div>
      <div class="modal-body">
        <?php print render($content['body']) ?>
      </div>
    </div>
    <ul class="steps">
      <li class="<?php $node->active_phase == '1' ? print('active') : print('not-active') ?>">
        <a href='<?php print $node_url ?>?step=doc' class="step-tooltip"  data-html="true" data-title="<div class='tooltip-title'><i class='icon-sgmap-faq'></i><?php print t('titre tooltip doc')?></div><?php print t('contenu tooltip doc')?>">
          Documentation
          <?php if ($content['duration']): ?>
            <span class="date">
              <?php print render($content['doc_date']) ?>
            </span>
          <?php endif; ?>
        </a>
      </li>
      <li class="<?php $node->active_phase == '2' ? print('active') : print('not-active') ?>">
        <a href="<?php print $node_url ?>?step=work" class="step-tooltip" data-html="true" data-title="<div class='tooltip-title'><i class='icon-sgmap-faq'></i><?php print t('titre tooltip travail en commun')?></div><?php print t('contenu tooltip travail en commun')?>">
          Travail en commun
          <?php if ($content['duration']): ?>
            <span class="date">
              <?php print render($content['work_date']) ?>
            </span>
          <?php endif; ?>          
        </a>
      <li class="<?php $node->active_phase == '3' ? print('active') : print('not-active') ?>">    
        <a href="<?php print $node_url ?>?step=synthese" class="step-tooltip" data-html="true" data-title="<div class='tooltip-title'><i class='icon-sgmap-faq'></i><?php print t('titre tooltip recommandation')?></div><?php print t('contenu tooltip recommandation')?>">
          Recommandations
          <?php if ($content['duration']): ?>
            <span class="date">
              <?php print render($content['synth_date']) ?>
            </span>
          <?php endif; ?>          
        </a>
      </li>
    </ul>
  <?php else :?>
    <div class="objectif-box">
      <h2 class="obj-title">Les Objectifs de l'atelier</h2>
        <?php print render($content['body']) ?>
      </div>
  <?php endif; ?>
</article> <!-- /.node -->
