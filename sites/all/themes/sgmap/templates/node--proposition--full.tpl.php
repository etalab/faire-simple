<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix node-full"<?php print $attributes; ?>>
  <?php
    // if($content['field_sgmap_p_statut']['und'][0]['value'] == 1) {
    //   echo 'Mesure engagée';
    // }
  ?>
  <div class="actionbox-container">
    <?php print render($content['bookmark_btn']) ?>
    <?php if($content['field_sgmap_p_statut']['und'][0]['value'] == 1) : ?>
      <span class="btn btn-red note-engaged"><i class="icon-sgmap-ok"></i> <?php print t("Mesure engagée"); ?></span>
    <?php endif; ?>
    <span class="btn btn-red followbox">
      <span class="plus1_count"><?php print render($content['support_widget']['score'])?></span> 
      <i class="icon-heart icon-white"></i>
    </span>    
  </div>
  <header>
    <div class="public">
      <?php print render($content['public']); ?>
    </div>
    <?php print render($title_prefix); ?>
      <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
    <?php print render($title_suffix); ?>
  </header>
  <div class="thematique">
    <?php print render($content['thematique']); ?>
  </div>
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_thematique_entreprise']);
    hide($content['field_thematique_agent']);
    hide($content['field_thematique_particulier']);
    hide($content['field_sgmap_public']);
    hide($content['print_mail']);
    hide($content['social']);
    hide($content['support_widget']);
    print render($content);
  ?>
  <?php if ($display_submitted): ?>
  <span class="submitted">
    <?php print $user_picture; ?>
    <?php print $submitted; ?>
  </span>

  <?php
    show($content['support_widget']['content']['button']);
    print render($content['support_widget']);
  ?>

  <div class="proposal-links">
    <?php
      print render($content['print_mail']);
      print render($content['social']);
    ?>
  </div>

  <?php endif; ?>

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php //print render($content['links']); ?>
    </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>

</article> <!-- /.node -->
