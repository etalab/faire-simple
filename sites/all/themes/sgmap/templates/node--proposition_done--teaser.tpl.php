<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix "<?php print $attributes; ?>>
  <header>
    <div class="done-title">
      Mesure engagée
    </div>
  </header>
  <div class="main-proposition">
    <div class="central-proposition">
      <h2 class="proposition-title"><a href="<?php print $node_url; ?>"><?php print truncate_utf8(decode_entities($title), 50, true, true, 1); ?></a></h2>
      <div class="central-content"><?php print truncate_utf8(strip_tags($content['body']['0']['#markup']), 93, true, true, 1) ?></div>
    </div>
  </div>
</article> <!-- /.node -->
