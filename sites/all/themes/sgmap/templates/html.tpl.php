<!DOCTYPE html>
<html lang="<?php print $language->language; ?>">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <meta property="og:image" content="<?php echo $GLOBALS['base_url'].'/'.drupal_get_path('theme', 'sgmap')?>/img/logo_sgmap.png">
  <?php print $styles; ?>
  <style type="text/css" media="print">@import url("<?php print $GLOBALS['base_url'] .'/'. path_to_theme() ?>/css/print.css");</style>
  <?php print $scripts; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
    <html class="lt-ie9"> 
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body class="<?php print $classes; ?> nojs" <?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
