<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix "<?php print $attributes; ?>>
  <div class="main-proposition">
    <header>
      <div class="public"><?php print render($content['public']); ?></div>
    </header>
    <div class="central-proposition">
      <h2 class="proposition-title"><a href="<?php print $node_url; ?>"><?php print truncate_utf8(decode_entities($title), 90, $wordsafe = true, $add_ellipsis = true, $min_wordsafe_length = 1); ?></a></h2>
      <div class="central-content">
        Dans "<?php print truncate_utf8($content['thematique']['#text'], 40, $wordsafe = true, $add_ellipsis = true, $min_wordsafe_length = 1); ?>"
      </div>
    </div>
    <footer>
      <span class="comment_count_wrapper"><i class="icon-sgmap-comment "></i> <span class="comment_count"><?php print $comment_count;?></span></span>
      <span class="plus_count_wrapper"><i class="icon-heart"></i> <span class="plus_count"><?php print $content['plus1_widget']['#score'] ;?></span></span>
    </footer>
  </div>
  <div class="more-proposition">
    <header>
      <div class="proposition-date"><?php print format_date($node->created, 'medium', 'd/m/Y'); ?></div>
    </header>
    <div class="central-proposition">
     <h2 class="proposition-title"><a href="<?php print $node_url; ?>"><?php print truncate_utf8(decode_entities($title), 90, $wordsafe = true, $add_ellipsis = true, $min_wordsafe_length = 1); ?></a></h2>
      <div class="central-content">
        <?php print truncate_utf8(strip_tags($content['body']['0']['#markup']), 75, $wordsafe = true, $add_ellipsis = true, $min_wordsafe_length = 1) ?>
      </div>
    </div>
    <footer>
      <?php if ($display_submitted): ?>
      <span class="submitted">
        <?php print $user_picture; ?>
        <?php print $submitted; ?>
      </span>
    <?php endif; ?>
  </footer>
  </div>
</article> <!-- /.node -->
