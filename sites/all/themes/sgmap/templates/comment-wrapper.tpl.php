<?php $node = menu_get_object() ?>
<section id="comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($content['comments'] && $node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
    <h2 class="title"><i class="icon-comment"></i> <span class="comments-count"><?php print $node->comment_count;?></span> <?php
    $node->comment_count <= 1 ? print t('Comment') : print t('Comments'); ?></h2>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
  <?php if ($content['comment_form']): ?>
    <h2 class="title"><?php print t('Add new comment'); ?></h2>
    <?php if ($fc_user == TRUE) :?>
      <div id="participation-box">
        <a href="/user/<?php echo $user->uid?>/edit?destination=node/<?php echo $node->vid ?>" class="btn btn-large" >Je finalise mon inscription pour pouvoir commenter</a>
      </div>
    <?php else: ?>
      <section id="comment-form-wrapper" class="">
        <?php print render($content['comment_form']); ?>
      </section> <!-- /#comment-form-wrapper -->
    <?php endif; ?>
  <?php endif; ?>

  <?php if(user_is_anonymous()) : ?>
    <div id="participation-box">
      <h2><?php print t('participer à la conversation'); ?></h2>
      <a href="/user/register?destination=node/<?php echo $node->vid ?>" class="btn btn-large" >Je créé mon compte</a>
      <a href="/user?destination=node/<?php echo $node->vid ?>%23comment-form-wrapper" class="btn btn-large" >Je me connecte</a>
    </div>
  <?php endif; ?>
  
</section> <!-- /#comments -->
