<?php
/**
 * @file
 * Template implementation to display the node widget.
 */
?>
<?php
$class = '';
if (!isset($use_arrow_down)) {
  if($can_vote && !$voted) {
    $class = ' plus1-vote';
  }
} elseif(isset($use_arrow_down)){
  $class = ' plus1-undo-vote';
}
?>
<?php if($can_vote) : ?>
	<div class="plus1-widget<?php echo $class?>"><?php print $widget_message;?></div>
<?php else: ?>
	<div class="plus1-widget"><a href="/user/login" class="btn btn-red"><i class="icon-heart-empty"></i> Connectez-vous pour voter</a></div>
<?php endif; ?>
