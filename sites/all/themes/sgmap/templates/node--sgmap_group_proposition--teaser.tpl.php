<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php print render($title_suffix); ?>
  </header>

   <div class="svg-radar-box pull-right" id="radar-teaser-<?php print $node->nid ?>"> </div>

  <?php
    hide($content['pcount_text']);
    print render($content);
    if(!isset($comment_count)){
      $comment_count = 0;
    }
  ?>
 
  <footer>
    <div class="footerBox">
      <span class="user-picture"><?php print $user_picture; ?></span>
      <span class="content-submitted"><?php print $submitted; ?></span>
      
        <!-- Participation count -->
        <?php if ($content['pcount_text']): ?>
          <div class="count-partipation">
            <span class="p-count"><?php print render($content['pcount_text']) ?></span>
            <span class="comment_count_wrapper"><i class="icon-sgmap-comment "></i> <span class="comment_count"><?php print $comment_count;?></span></span>
          </div>
        <?php endif; ?>
        <!--End Participation count -->
        
    </div>
  </footer>

</article> <!-- /.node -->
