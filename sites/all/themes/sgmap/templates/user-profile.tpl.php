<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>
<?php if($user_profile['panel_link']): ?>
<?php print render($user_profile['panel_link'])?>
<?php endif ?>
<div class="row">
    <div class="span6">
        <div class="userblock blockw clearfix"<?php print $attributes; ?>>
            <div class="pull-left avatar">
                <?php print render($user_profile['user_picture'])?>
            </div>
            <div class="infos">
                <h3><?php print render($elements['#account']->name); ?></h3>
                <?php print render($user_profile['field_sgmap_public'])?>
                <ul>
                    <?php if(isset($user_profile['age'])): ?>
                        <li>J'ai <?php print render($user_profile['age'])?> ans</li>
                    <?php endif ?>
                    <?php if(isset($user_profile['field_sgmap_user_location'])): ?>
                        <li>Je vis à <?php print $user_profile['field_sgmap_user_location'][0]['#markup']?></li>
                    <?php endif ?>
                    <li>Membre depuis le <?php print render($user_profile['member_since'])?></li>
                </ul>
                <?php print render($user_profile['field_sgmap_user_loisirs']); ?>
                <?php
                    hide($user_profile['summary']);
                    hide($user_profile['proposals_count']);
                    hide($user_profile['workshop_count']);
                    hide($user_profile['proposals_accepted_count']);
                    hide($user_profile['field_sgmap_user_location']);
                    print render($user_profile);
                ?>
                <?php if(arg(1) == $user->uid): ?>
                    <a href="/user/<?php echo $user->uid; ?>/edit" class="btn btn-large edit-profil btn-bleu"><i class="icon-pencil"></i> <?php print t('editer mon profil');  ?></a>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="userblock-right blockw">
            <h3>Ma participation</h3>
            <ul class="clearfix">
                <li class="vos-idees">
                    <i class="icon-sgmap-ampoule"></i>
                    <div class="participation-box">
                        <span class="count-proposal"><?php print render($user_profile['proposals_count'])?></span> idées soumises
                    </div>
                </li>
                <li class="les-mesures-engagees">
                    <i class="icon-sgmap-ok"></i>
                    <div class="participation-box">
                        <span class="count-proposal"><?php print render($user_profile['proposals_accepted_count'])?></span> idées adoptées
                    </div>
                </li>
                <li class="la-fabrique-de-solutions">
                    <i class="icon-sgmap-personnes"></i>
                    <div class="participation-box">
                        <span class="count-proposal"><?php print render($user_profile['workshop_count'])?></span> solutions proposées
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
