<article class="<?php print $classes . ' ' . $zebra; ?>"<?php print $attributes; ?>>
  <div class="submitted">
    <?php print $picture ?>
    <?php print $submitted ?>
  </div>

  <div class="comment-content">
    <?php
      // We hide the comments and links now so that we can render them later.
      print render($content);
    ?>
    <?php print render($content['links']['comment']['#links']['comment-complaint']['title']) ?>
  </div>
</article> <!-- /.comment -->
