<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($content['thematique']); ?>
  <?php if ($node->modal_title): ?>
    <h1><?php print $node->title ?></h1>
  <?php endif; ?>
  <?php
    hide($content['comments']);
    hide($content['links']);
    hide($content['modal_comment_form']);
    hide($content['modal_comment_list']);
    hide($content['plus1_widget']);
    hide($content['edit_link']);
    print render($content);
  ?>
  <span class="submitted">
    <?php print $user_picture; ?>
    <?php print $submitted; ?>
  </span>

  <?php if($content['edit_link'] && !$content['blocked']): ?>
    <div class="edit-link-container">
      <?php print render($content['edit_link']) ?>
    </div>
  <?php endif; ?>

  <?php if (!$content['blocked']): ?>
    <?php print render($content['plus1_widget']); ?>
  <?php endif; ?>
  
  <?php print render($content['comments']); ?>

  <?php if ($node->modal_title): ?>
    <div class="modal-comments" id ="modal-comments">
      <h2 class="comments-header"><span class="tot-com" id="comment-count"><?php print $node->comment_count ?></span> Commentaire(s)</h2>
        <?php if ($content['modal_comment_list']): ?>
          <?php print render($content['modal_comment_list']) ?>
        </div>
      <?php endif; ?>
    </div>
    <?php if (!$content['blocked']): ?>
      <div id="modal-comment-form">
        <?php print render($content['modal_comment_form']) ?>
      </div>
    <?php endif; ?>
  <?php endif; ?>
</article> <!-- /.node -->
