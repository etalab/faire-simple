<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div>
    <div class="pull-left clearfix">
      <a class="link-img-group" href="<?php print $node_url; ?>"><?php print(render($content['field_group_img_crop'])) ?></a>
      <div class="info-group">
        <span><i class="icon-sgmap-particulier"></i> <?php print(render($content['member_count'])) ?></span>
        <?php if ($content['duration']): ?>
          <span><i class="icon-sgmap-clock"></i> <?php print(render($content['duration'])) ?></span>
        <?php endif; ?>
      </div>
    </div>
    <div class="desc-group clearfix">
      <div class="central-content">
      <?php if ($content['atelier-policy']['#markup']): ?>
        <span class="padlock-close"><i class="icon-lock"></i> Atelier privé</span>
      <?php else: ?>
        <span class="padlock-open"><i class="icon-unlock-alt"></i> Atelier ouvert</span>
      <?php endif; ?>
      <h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php print(render($content['field_group_resume'])); ?>
      </div>
      <footer class="clearfix">
        <?php if ($content['status']): ?>
          <div class="pull-left">
            phase : <br />
            <span class="status-group"><?php print(render($content['status'])) ?></span>
            <?php if(in_array(render($content['phase']), array('1','2','3'))): ?>
            <div class="progress-box clearfix">
              <i class="icon-busy"></i>
              <?php print(render($content['time_left'])) ?>
              <div class="progress">
                <div class="bar" role="progressbar" aria-valuenow="<?php print(render($content['percent'])) ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print(render($content['percent'])) ?>%;">
                </div>
              </div>
            </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if ($content['atelier-policy']['#markup'] == 0 || $content['user_is_group_member']): ?>
          <div class="pull-right">
            <a class="btn btn-large btn-orange" href="<?php print $node_url; ?>">Je souhaite Participer</a>
          </div>
        <?php endif; ?>
      </footer>
    </div>
  </div>
  
</article> <!-- /.node -->