(function($) {
    
    $(document).ready(function(){
        // initAriaAccordion: avoid conflict with ajax modal
        $('a[aria-expanded]').each(function () {
            var $this = $(this);
            if ($this.attr('aria-expanded') == 'false') {
                $('#' + $this.attr('aria-controls')).hide();
            } else {
                $this.addClass("open");
            }
        }).click(function(event){
            var target = $('#' + $(this).attr('aria-controls'));
            var $this = $(this);
            if ($this.attr('aria-expanded') == 'false') {
                $this.addClass("open").attr('aria-expanded', true);
                target.slideDown();
            } else {
                $this.removeClass("open").attr('aria-expanded', false);
                target.slideUp();
            }

            event.preventDefault();
        });

        // if login form has error open login form
        if($('#user-login-form').data('form-has-error') === 1) {
            openLoginForm();
        }

        // on top fixed header login button click scroll to top and open login block
        $('#btn-login-form-toggle-fixed').click(function(event) {
            openLoginForm();
            event.preventDefault();
        });
    });
    


    Drupal.behaviors.sgmap = {
        attach: function(context, settings) {
            initJs();
            initBootstrapAccordion();
            //initAriaAccordion();
            initICheck('.search-form');
            initResetForm();
            initSgmapHelpBootstrapPopover();
            initFixedHeaderMenu();
            initMobileNavigation();
            initScrollAfterCloseLightbox();

            

            // go to top link
            $(".link-top").click(function(event) {
                goToTop()
                event.preventDefault();
            });

            // then wizard form wrapper re-init popover
            $('#wizard-form-wrapper').ajaxComplete(function(event, xhr, settings) {
                initSgmapHelpBootstrapPopover();
            });

            // click on proposition
            $('.proposition-list li:not(".no-propositions")').click(function(){
                window.location.href = $(this).find('.central-proposition a').attr('href');
            });
        }
    };

    /**
     * Provide the HTML to create the modal dialog.
     */
    Drupal.theme.prototype.CommentAbuse = function () {

        var html = '';

        html += '<div id="ctools-modal" class="popups-box">';
        html += '  <div class="ctools-modal-content ctools-comment-abuse">';
        html += '          <div class="popups-container">';
        html += '            <div class="modal-header popups-title">';
        html += '              <span id="modal-title" class="modal-title"></span>';
        html += '              <span class="popups-close"><a class="close" href="#"><i class="icon-remove"></i></a></span>';
        html += '              <div class="clear-block"></div>';
        html += '            </div>';
        html += '            <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>';
        html += '            <div class="popups-buttons"></div>';
        html += '            <div class="popups-footer"></div>';
        html += '          </div>';
        html += '  </div>';
        html += '</div>';

        return html;

    }


    // bootstrap accordion with chevron
    var initBootstrapAccordion = function() {
        var accordion = $('.accordion');

        accordion
            .find('.accordion-heading i')
            .addClass('icon-chevron')
        ;

        accordion
            .find('.accordion-body.collapse.in')
            .siblings('.accordion-heading')
            .find('.accordion-toggle')
            .addClass('actived')
        ;

        accordion.on('show hide', function (event) {
            var targetlink = $(event.target)
            .siblings('.accordion-heading')
            .find('.accordion-toggle')
        ;

        var chevronClass = event.type == 'show' ? 'actived' : '';
        targetlink.removeClass('actived').addClass(chevronClass);
        });
    }

    // init aria accordion
    var initAriaAccordion = function () {
        $('a[aria-expanded]').each(function () {
            var $this = $(this);
            if ($this.attr('aria-expanded') == 'false') {
                $('#' + $this.attr('aria-controls')).hide();
            } else {
                $this.addClass("open");
            }
        }).click(function(event){
            var target = $('#' + $(this).attr('aria-controls'));
            var $this = $(this);
            if ($this.attr('aria-expanded') == 'false') {
                $this.addClass("open").attr('aria-expanded', true);
                target.slideDown();
            } else {
                $this.removeClass("open").attr('aria-expanded', false);
                target.slideUp();
            }

            event.preventDefault();
        });
    }

    // Icheck initialisation
    var initICheck = function(selector) {

        var iCheckOptions = {
            handle: 'checkbox',
            labelHoverClass: 'hover',
            checkboxClass: 'icheckbox'
        };

        var $checkboxes = $(selector)
            .find('input.form-checkbox')
            .iCheck(iCheckOptions)
            .on('ifChanged', function(event) {
                $(this).parent().parent().toggleClass('checked')
            })
        ;

        $checkboxes.filter(':checked').parent().parent().addClass('checked');

    }

    window.initICheck = initICheck;

    // open login form
    var openLoginForm = function() {
        goToTop();
        $('#btn-login-form-toggle').click();
    }

    var goToTop = function () {
        $('body, html').animate({
            scrollTop: 0
        }, 700, "linear");
    }

    var initSgmapHelpBootstrapPopover = function() {
        $('.detailsbox a').popover({ html : true }).parent().delegate('button#close', 'click', function() {
            $('.detailsbox a').popover('hide');
        });
    }

    var initJs = function() {
        $('body')
            .removeClass('nojs')
            .addClass('js');
    }

    var initResetForm = function() {
        $('.reset-form').click(function(){
            $(this).parents('form')[0].reset();
        });
    }

    var initFixedHeaderMenu = function() {
        

        // float header menu display
        //
        var fixedHeader = $('#float-header-menu');

        var header = $('#header');
        if (header.length) {
            header
                .data('position', header.position())
                .data('triggerValue', header.position().top + header.height() - header.find('.header-bottom').height())
            ;
        }
        

        var scroller = function() {
            var scroll = getScroll();
            if (header.data('triggerValue') < scroll.top){
                fixedHeader.removeClass('hide');
            }
            else {
                fixedHeader.addClass('hide');
            }
        };

        var hashHandler = function(event) {
            // scroll pour eviter le menu flottant
            var theHash = window.location.hash;
            if(theHash!="" && $(theHash).length > 0) {
                setTimeout(function() {
                    var scroll = getScroll();
                    if (!fixedHeader.hasClass('hide')) {
                        var scrollEleValue = $(theHash).offset().top - 60;
                        $('body, html').scrollTop(scrollEleValue);
                    }
                }, 100);
            }
        };

        if (window.addEventListener) {
            window.addEventListener('scroll', scroller, false);
            window.addEventListener('hashchange', hashHandler, false);
        } else if (window.attachEvent) {
            window.attachEvent('onscroll', scroller);
            window.attachEvent('onhashchange', hashHandler, false);
        }

        hashHandler();

    }

    var initMobileNavigation = function() {
        // link menu mobile
        $(".link-menu-mobile").click(function(event){
            var menuId = $("#nav-box");
            if(menuId.hasClass("open")) {
                menuId.removeClass("open").slideUp();
            }
            else {
                menuId.addClass("open").slideDown();
            }
        event.preventDefault();
        });
    }

    var getScroll = function() {
        var b = document.body;
        var e = document.documentElement;
        return {
            left: parseFloat( window.pageXOffset || b.scrollLeft || e.scrollLeft ),
            top: parseFloat( window.pageYOffset || b.scrollTop || e.scrollTop )
        };
    }

    var initScrollAfterCloseLightbox = function() {
        $("#group-content-list .use-ajax").click(function(event){
            var scroll = getScroll();
            scrollLvl = scroll.top;
            event.preventDefault();
        });
        $('#lns_modal_container').on('show', function () {
            $('#lns_modal_container').on('hidden', function () {
                $('body, html').scrollTop(scrollLvl);
            });
        });
            
    }

})(jQuery);

