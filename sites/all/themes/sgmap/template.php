<?php

/**
 * Preprocess variables for page.tpl.php
 *
 * @see page.tpl.php
 */
function sgmap_preprocess_page(&$variables) {
  if (isset($variables['no-title'])) {
    $variables['title'] = '';
  }
  if (isset($variables['no_container'])) {
    $variables['no_container'] = false;
  }
}

function sgmap_menu_tree__primary(&$variables) {
  return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
}

function sgmap_menu_tree__main_menu_top_fixed(&$variables) {
  return '<ul class="inline pull-left">' . $variables['tree'] . '</ul>';
}

function sgmap_menu_tree__mavigation_top_fixed(&$variables) {
  return '<ul class="list-drop inline pull-left">' . $variables['tree'] . '</ul>';
}

function sgmap_menu_tree__top_navigation(&$variables) {

  $mobile_search_link = theme('link', array(
    'text' => '<i class="icon-search"></i><span class="element-invisible">Recherche</span>',
    'path' => 'search/site',
    'options' => array(
      'attributes' => array('class' => array('link-search', 'visible-phone', 'pull-left')),
      'html'       => TRUE
    )
  ));

  return '<div class="container"><nav id="sgmap-top-navigation"><ul class="nav inline pull-left">' . $variables['tree'] . '</ul>' . $mobile_search_link . render(drupal_get_form('search_block_form')) . '</nav></div>';
}

function sgmap_html_class($string) {
  return transliteration_get(drupal_html_class($string));
}

function sgmap_campagne_content_bottom($elements) {

  $markup = '<div class="row">';
  $markup .= '<div class="span9">' . $elements['propositions_box'] . '</div>';
  $markup .= '<div class="span3">' . $elements['plus_d_infos_box'] . '</div>';
  $markup .= '</div>';

  return $markup;
}

function sgmap_field($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<div '. $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . $variables['content_attributes'] . '>' . $output . '</div>';


  return $output;
}

function sgmap_menu_link__footer_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="submenu">' . drupal_render($element['#below']) . '</ul>';
    $element['#localized_options']['attributes']['class'][] = 'title';

      // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
    $element['#localized_options']['attributes']['data-target'] = '#';
  }
 // Issue #1896674 - On primary navigation menu, class 'active' is not set on active menu item.
 // @see http://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
   $element['#attributes']['class'][] = 'active';
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function sgmap_menu_link__main_menu_top_fixed(array $variables) {
  $element = $variables['element'];

  $link_class = sgmap_html_class($element['#title']);

  $link_options = array(
    'html' => true,
    'attributes' => array(
      'class' => array($link_class)
    )
  );

  $link_options += $element['#localized_options'];
  unset($link_options['attributes']['title']);

  $description = $element['#original_link']['options']['attributes']['title'];

  $icon_class_mapping = _sgmap_get_main_menu_icon_class_mapping();

  $output = l($element['#title'], $element['#href'], $link_options);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output ."</li>\n";
}

function sgmap_menu_link__main_menu(array $variables) {

  $element = $variables['element'];

  switch ($variables['element']['#attributes']['class'][0]) {
    case 'first':
      $link_class = 'toutes-vos-idees';
      break;
    case 'leaf':
      $link_class = 'la-fabrique-de-solutions';
      break;
    case 'last':
      $link_class = 'les-mesures-engagees';
      break;
  }

  $link_options = array(
    'html' => true,
    'attributes' => array(
      'class' => array($link_class)
    )
  );


  $link_options += $element['#localized_options'];
  unset($link_options['attributes']['title']);

  $description = $element['#original_link']['options']['attributes']['title'];

  $icon_class_mapping = _sgmap_get_main_menu_icon_class_mapping();
  $element['#title'] = '<i class="'. $icon_class_mapping[$link_class] .'"></i> <strong>' . $element['#title'] . '</strong>';

  if(drupal_is_front_page()) {
    $element['#title'] .= '<br>' . $description;
  }

  $output = l($element['#title'], $element['#href'], $link_options);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output ."</li>\n";
}

function _sgmap_get_main_menu_icon_class_mapping() {
  return array(
    'toutes-vos-idees'          => 'icon-sgmap-ampoule',
    'la-fabrique-de-solutions'  => 'icon-sgmap-personnes',
    'les-mesures-engagees'      => 'icon-sgmap-ok'
  );
}

/**
 * Returns the correct span class for a region
 */
function _sgmap_content_span($columns = 1) {
  $class = FALSE;

  if(drupal_is_front_page()) {
    $class = 'span8';
  }
  else {
    $class = _bootstrap_content_span($columns);
  }
  return $class;
}

function sgmap_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {

    // Prevent dropdown functions from being added to management menu as to not affect navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }

    else {
      // Add our own wrapper
      unset($element['#below']['#theme_wrappers']);
      $dropdown_toggle_id = sgmap_html_class(strip_tags($element['#title']));
      $sub_menu = '<ul class="dropdown-menu" role="menu" aria-labelledby="'.$dropdown_toggle_id.'">' . drupal_render($element['#below']) . '</ul>';
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
      $element['#localized_options']['attributes']['id'] = $dropdown_toggle_id;
      $element['#localized_options']['attributes']['role'] = 'button';

      // Check if this element is nested within another
      if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
        // Generate as dropdown submenu
        $element['#attributes']['class'][] = 'dropdown-submenu';
      }
      else {
        // Generate as standard dropdown
        $element['#attributes']['class'][] = 'dropdown';
        $element['#localized_options']['html'] = TRUE;
        $element['#title'] .= ' <span class="caret"></span>';
      }

      // Set dropdown trigger element to # to prevent inadvertant page loading with submenu click
      $element['#localized_options']['attributes']['data-target'] = '#';
    }
  }
 // Issue #1896674 - On primary navigation menu, class 'active' is not set on active menu item.
 // @see http://drupal.org/node/1896674
 if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
   $element['#attributes']['class'][] = 'active';
 }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements hook_css_alter().
 */
function sgmap_css_alter(&$css) {
  $theme_path = drupal_get_path('theme', 'bootstrap');
  // Add Bootstrap CDN file and overrides.
  $override = $theme_path . '/css/overrides.css';

  if(!isset($css[$override])) {
    $css[$override] = array(
      'data' => $override,
      'type' => 'file',
      'every_page' => TRUE,
      'media' => 'all',
      'preprocess' => TRUE,
      'group' => CSS_THEME,
      'browsers' => array('IE' => TRUE, '!IE' => TRUE),
      'weight' => -1,
    );
  }
}

/**
 * Implements hook_js_alter;
 */
function sgmap_js_alter(&$javascript) {
  // replace bootstrap theme js file by bower managed js file
  $old_path = 'sites/all/themes/sgmap/js/bootstrap.min.js';
  $copy = $javascript[$old_path];

  $new_path = 'libraries/sass-bootstrap/docs/assets/js/bootstrap.min.js';
  unset($javascript[$old_path]);

  $copy['data'] = $new_path;
  $javascript[$new_path] = $copy;
}

function sgmap_date_combo($variables) {
  return theme('form_element', $variables);
}

/**
 * Implement hook_form_alter
 */
function sgmap_form_search_block_form_alter(&$form) {
  $form['#attributes']['class'][] = 'pull-left';
}

/**
 * Returns HTML for Bootstrap's Collapse/Accordion.
 */
function sgmap_bootstrap_accordion($variables) {
  $elements = $variables['elements'];

  if (empty($variables['id'])) {
    $accordion_id = 'accordion-'. md5($elements);
  }
  else {
    $accordion_id = check_plain($variables['id']);
  }

  $output .= '<div class="accordion" id="'.$accordion_id.'">';

  foreach ($elements as $id => $item) {
    $output .= '<div class="accordion-group"><div class="accordion-heading">';
    $output .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#'. $accordion_id .'" href="#'. $id .'"><i></i> '. check_plain($item['header']) .'</a></div>';
    $output .= '<div id="'. $id .'" class="accordion-body collapse"><div class="accordion-inner">';
    $output .= render($item['content']);
    $output .= '</div></div></div>';
  }

  $output .= '</div>';

  return $output;
}

/**
 * breadcrum: add title at the last step
 */
function sgmap_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!drupal_is_front_page()) {
    $breadcrumb[] = '<span>'.drupal_get_title().'</span>';
  }
  $custom['breadcrumb'] = $breadcrumb;
  return bootstrap_breadcrumb($custom);
}
