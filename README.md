# Faire simple

_Faire-simple_ is a collaborative web platform created with Drupal 7 for the [Secrétariat général à la modernisation de l'action publique (SGMAP)](http://modernisation.gouv.fr/le-sgmap).

The objective is to simplify administrative steps and modernize the public action.

_Faire-simple_ is made of 3 components:

- "les sujets du moment", where individuals and companies can bring new ideas to simplify administrative steps.

- "La fabrique de solutions", where public officials and users interact together in many workshops

- "Les mesures engagées", that contains the results of interactions.


[Presentation of _Faire simple_ in french](https://framagit.org/etalab/faire-simple/wikis/plaquette-presentation-faire-simple-fr.pdf)

[Wiki](https://git.framasoft.org/etalab/faire-simple/wikis/home)

## Admin

Login : admin

password : administrator

## License

Faire-simple has the same licence as drupal 7 : GNU GENERAL PUBLIC LICENCE Version 2